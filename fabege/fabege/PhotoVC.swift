//
//  PhotoVC.swift
//  fabege
//
//  Created by Patrik Billgert on 26/03/15.
//  Copyright (c) 2015 Frontcell AB. All rights reserved.
//

import UIKit
import AVFoundation
import MobileCoreServices
import Photos


protocol PhotoVCDelegate {
    func didTakePhoto(_ image : UIImage);
}

class PhotoVC: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    @IBOutlet weak var cameraButton: UIButton!
    @IBOutlet weak var photosButton: UIButton!
    
    let captureSession = AVCaptureSession()
    var captureDevice : AVCaptureDevice?
    var previewLayer : AVCaptureVideoPreviewLayer?
    var stillImageOutput = AVCaptureStillImageOutput()
    var delegate : PhotoVCDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationItem.rightBarButtonItem?.tintColor = UIColor.white
        navigationItem.rightBarButtonItem?.setTitleTextAttributes(convertToOptionalNSAttributedStringKeyDictionary([convertFromNSAttributedStringKey(NSAttributedString.Key.font):UIFont(name: "HelveticaNeue", size: 14)!]),for: UIControl.State())
        
        navigationItem.title = "Ta foto"
        
        photosButton.layer.cornerRadius = 5
        photosButton.imageView?.contentMode = .scaleAspectFill
        
        setupCamera()
        loadLatestPhoto()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    func beginSession(device : AVCaptureDevice) {
        do {
            let input = try AVCaptureDeviceInput(device: device)
            captureSession.addInput(input)
        } catch {
            print("Erorr starting camera")
            return
        }
        
        previewLayer = AVCaptureVideoPreviewLayer(session: captureSession)
        //self.view.layer.addSublayer(previewLayer!)
        let rootLayer = self.view.layer
        rootLayer.masksToBounds = true
        previewLayer?.frame = self.view.frame
        previewLayer?.videoGravity = AVLayerVideoGravity.resizeAspectFill
        
        rootLayer.insertSublayer(previewLayer!, at: 0)
        
        stillImageOutput.outputSettings = [AVVideoCodecKey: AVVideoCodecJPEG]
        if captureSession.canAddOutput(stillImageOutput) {
            captureSession.addOutput(stillImageOutput)
        }
        
        captureSession.startRunning()
    }
    
    
    
    // MARK: - IBActions
    
    @IBAction func openArchive(_ sender: AnyObject) {
        let imagePicker = UIImagePickerController()
        imagePicker.delegate = self
        imagePicker.allowsEditing = true
        imagePicker.sourceType = .photoLibrary
        
        present(imagePicker, animated: true, completion: nil)
    }
    
    @IBAction func close(_ sender: AnyObject) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func takePicture(_ sender: AnyObject) {
        if let videoConnection = stillImageOutput.connection(with: AVMediaType(rawValue: convertFromAVMediaType(AVMediaType.video))) {
            stillImageOutput.captureStillImageAsynchronously(from: videoConnection) { (imageDataSampleBuffer, error) -> Void in
                if let data = imageDataSampleBuffer, let imageData = AVCaptureStillImageOutput.jpegStillImageNSDataRepresentation(data) {
                    let image = UIImage(data:imageData)!
                    self.setImageOnRegisterVC(image)
                    self.presentingViewController?.dismiss(animated: true, completion: nil)
                }
            }}
    }
    
    
    
    // MARK: - UIImagepicker Delegate
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        // Local variable inserted by Swift 4.2 migrator.
        let info = convertFromUIImagePickerControllerInfoKeyDictionary(info)
        
        let image = info[convertFromUIImagePickerControllerInfoKey(UIImagePickerController.InfoKey.editedImage)] as! UIImage
        self.setImageOnRegisterVC(image)
        presentingViewController?.dismiss(animated: true, completion: nil)
    }
    
    
    
    // MARK: - UINavigationControllerDelegate Delegate
    
    func navigationController(_ navigationController: UINavigationController, willShow viewController: UIViewController, animated: Bool) {
        UIApplication.shared.setStatusBarStyle(UIStatusBarStyle.lightContent, animated: false)
        navigationController.navigationBar.isTranslucent = false
    }
    
    
    
    // MARK: - Other functions
    
    func setImageOnRegisterVC(_ image: UIImage) {
        let maxSide = max(image.size.width, image.size.height)
        let scale = min(2000 / maxSide, 1)
        if let resizedImage = image.resized(withPercentage: scale) {
            delegate?.didTakePhoto(resizedImage)
        }
        
    }
    
    func setupCamera() {
        let captureSession = AVCaptureSession()
        
        // If we find a device we'll store it here for later use
        //var captureDevice : AVCaptureDevice?
        
        // Do any additional setup after loading the view, typically from a nib.
        captureSession.sessionPreset = AVCaptureSession.Preset(rawValue: convertFromAVCaptureSessionPreset(AVCaptureSession.Preset.low))
        
        let devices = AVCaptureDevice.devices()
        
        // Loop through all the capture devices on this phone
        for device in devices {
            // Make sure this particular device supports video
            if device.hasMediaType(.video) {
                // Finally check the position and confirm we've got the back camera
                if device.position == AVCaptureDevice.Position.back {
                    captureDevice = device
                }
            }
        }
        
        if let device = captureDevice {
            beginSession(device: device)
        }
    }
    
    // Repeatedly call the following method while incrementing
    // the index until all the photos are fetched
    func loadLatestPhoto() {
        
        let imgManager = PHImageManager.default()
        
        // Note that if the request is not set to synchronous
        // the requestImageForAsset will return both the image
        // and thumbnail; by setting synchronous to true it
        // will return just the thumbnail
        let requestOptions = PHImageRequestOptions()
        requestOptions.isSynchronous = true
        
        // Sort the images by creation date
        let fetchOptions = PHFetchOptions()
        fetchOptions.sortDescriptors = [NSSortDescriptor(key:"creationDate", ascending: true)]
        
        let fetchResult: PHFetchResult = PHAsset.fetchAssets(with: PHAssetMediaType.image, options: fetchOptions)
        
        // If the fetch result isn't empty,
        // proceed with the image request
        if fetchResult.count > 0 {
            // Perform the image request
            imgManager.requestImage(for: fetchResult.object(at: fetchResult.count - 1) as PHAsset, targetSize: view.frame.size, contentMode: PHImageContentMode.aspectFill, options: requestOptions, resultHandler: { (image, _) in
                
                self.photosButton.setImage(image, for: UIControl.State())
            })
        }
    }
    
    
    
    
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromUIImagePickerControllerInfoKeyDictionary(_ input: [UIImagePickerController.InfoKey: Any]) -> [String: Any] {
    return Dictionary(uniqueKeysWithValues: input.map {key, value in (key.rawValue, value)})
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertToOptionalNSAttributedStringKeyDictionary(_ input: [String: Any]?) -> [NSAttributedString.Key: Any]? {
    guard let input = input else { return nil }
    return Dictionary(uniqueKeysWithValues: input.map { key, value in (NSAttributedString.Key(rawValue: key), value)})
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromNSAttributedStringKey(_ input: NSAttributedString.Key) -> String {
    return input.rawValue
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromAVMediaType(_ input: AVMediaType) -> String {
    return input.rawValue
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromUIImagePickerControllerInfoKey(_ input: UIImagePickerController.InfoKey) -> String {
    return input.rawValue
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromAVCaptureSessionPreset(_ input: AVCaptureSession.Preset) -> String {
    return input.rawValue
}
