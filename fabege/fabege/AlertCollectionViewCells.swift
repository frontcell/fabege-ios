//
//  LargeAlertCollectionViewCell.swift
//  fabege
//
//  Created by Oscar Kockum on 2019-06-11.
//  Copyright © 2019 Frontcell AB. All rights reserved.
//

import UIKit


class SmallAlertCollectionViewCell: UICollectionViewCell {
     @IBOutlet weak var title: UILabel!
     func setAlert(alert : WAWAlert) {
          title.text = alert.title
     }
}


class LargeAlertCollectionViewCell: UICollectionViewCell {
     
     var onExitClicked: (() -> Void)?
     
     @IBOutlet weak var title: UILabel!
     @IBOutlet weak var message: UITextView!
     @IBOutlet weak var date: UILabel!
     
     static let df : DateFormatter = {
          let df = DateFormatter()
          // 10 juni 2019, 08:03
          df.dateFormat = "dd MMMM yyyy, HH:mm"
          df.locale = Locale(identifier: "sv_SE")
          return df
     }()
     
     override func awakeFromNib() {
          message.textContainerInset = UIEdgeInsets.zero
          message.textContainer.lineFragmentPadding = 0;
     }
     
     func setAlert(alert : WAWAlert) {
          title.text = alert.title
          message.formatHTML(text: alert.message)
          if alert.publishAt > 0 {
               date.text = LargeAlertCollectionViewCell.df.string(from: Date(timeIntervalSince1970: alert.publishAt))
          } else {
               date.text = ""
          }
          
     }
     
     @IBAction func exitAction(_ sender: Any) {
          onExitClicked?()
     }
}
