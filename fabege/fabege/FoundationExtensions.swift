//
//  FoundationExtensions.swift
//  fabege
//
//  Created by Oscar Kockum on 2018-12-17.
//  Copyright © 2018 Frontcell AB. All rights reserved.
//

import Foundation

extension String {
     func isValidEmail() -> Bool {
          let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
          let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
          return emailTest.evaluate(with: self)
     }
     
     public func toPhoneNumber() -> String {
          var numberToReturn = self
          if self.starts(with: "46") && self.count >= 10 {
               numberToReturn = "+" + numberToReturn
               numberToReturn = numberToReturn.replacingOccurrences(of: "(\\d{2})(\\d{3})(\\d{3})(\\d+)", with: "$1 $2 $3 $4", options: .regularExpression, range: nil)
          } else {
               numberToReturn = numberToReturn.replacingOccurrences(of: "(\\d{4})(\\d+)", with: "$1 $2", options: .regularExpression, range: nil)
          }
          return numberToReturn
     }

    func insertNewLines() -> String {
        return replacingOccurrences(of: "\\n", with: "\n")
    }
}


