//
//  WAWIntiateVC.swift
//  fabege
//
//  Created by Oscar Kockum on 2019-06-11.
//  Copyright © 2019 Frontcell AB. All rights reserved.
//

import UIKit
import StoreKit

protocol WAWInitiator : SKStoreProductViewControllerDelegate  {
    
}

extension WAWInitiator where Self: UIViewController {
    
    func validateApp() {
        AppConfigService.shared.validateApp { (isWAWAllowed) in
            if !isWAWAllowed {
                let vc = DialogController.display(from: self, title: "Hoppsan", highlightedText: "Din app behöver uppdateras innan du kan använda appen", defaultText: nil, lightMode: true, dialogType: .appStore)
                 vc.onAppstoreClicked = { [unowned self] in
                      //self.dismiss(animated: true, completion: {
                           self.goToAppStore()
                      //})
                 }
            }
        }
    }
    
    func initiateWAW() {
        AppConfigService.shared.validateApp { (isWAWAllowed) in
            if isWAWAllowed {
                WAWRouter(navigationController: self.navigationController!).initialize()
            } else {
                let vc = DialogController.display(from: self, title: "Hoppsan", highlightedText: "Din app behöver uppdateras innan du kan använda appen", defaultText: nil, lightMode: true, dialogType: .appStore)
                vc.onAppstoreClicked = { [unowned self] in
                     //self.dismiss(animated: true, completion: {
                          self.goToAppStore()
                     //})
                }
            }
        }
    }
    
    func goToAppStore() {
        
        let urlStr = "itms-apps://itunes.apple.com/app/apple-store/id994180572?mt=8"
        if #available(iOS 10.0, *) {
            UIApplication.shared.open(URL(string: urlStr)!, options: [:], completionHandler: nil)
        } else {
            UIApplication.shared.openURL(URL(string: urlStr)!)
        }
        /*
         let vc = SKStoreProductViewController()
         let params = [SKStoreProductParameterITunesItemIdentifier:994180572]
         vc.modalPresentationStyle = .fullScreen //or .overFullScreen for transparency
         vc.delegate = self
         vc.loadProduct(withParameters: params) { (finished, error) in
              
         }
         self.present(vc, animated: true, completion: nil)*/
    }
    
}


