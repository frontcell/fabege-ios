//
//  WAWInfoController.swift
//  fabege
//
//  Created by Oscar Kockum on 2018-12-14.
//  Copyright © 2018 Frontcell AB. All rights reserved.
//

import UIKit

class WAWInfoController: UIViewController, UIScrollViewDelegate {
     
     struct Info {
          var header : String?
          var info : String?
          var actionButton : String?
          var onActionClicked: (() -> Void)?
     }
     
     @IBOutlet weak var headerLabel: UILabel!
     @IBOutlet weak var infoLabel: UILabel!
     @IBOutlet weak var actionButton: UIButton!
     @IBOutlet weak var titleActionButton: UIButton!
    
     
     var data : Info!
     
     class func setup(with info : Info) -> WAWInfoController {
          let vc = UIStoryboard(name: "WAW", bundle: nil).instantiateViewController(withIdentifier: "WAWInfoController") as! WAWInfoController
          vc.data = info
          return vc
     }
     
     
     override func viewDidLoad() {
          super.viewDidLoad()
          
          headerLabel.text = data.header
          infoLabel.text = data.info
          actionButton.setTitle(data.actionButton, for: .normal)
          
          actionButton.isHidden = data.actionButton == nil
          infoLabel.isHidden = data.info == nil
          headerLabel.isHidden = data.header == nil
          
          actionButton.outerShadow(show: true, animated: false)
          
          
     }
     
     override func viewWillAppear(_ animated: Bool) {
          navigationController?.setNavigationBarHidden(true, animated: true)
     }
     
     @IBAction func onActionButton(_ sender: Any) {
          data.onActionClicked?()
     }
    
     @IBAction func onActionBack(_ sender: Any) {
          navigationController?.popViewController(animated: true)
     }
     
     override var preferredStatusBarStyle: UIStatusBarStyle {
          return .default
     }
     
     /*
      // MARK: - Navigation
      
      // In a storyboard-based application, you will often want to do a little preparation before navigation
      override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
      // Get the new view controller using segue.destination.
      // Pass the selected object to the new view controller.
      }
      */
     
}
