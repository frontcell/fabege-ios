//
//  FabegeAnalytics.swift
//  fabege
//
//  Created by Oscar Kockum on 2019-01-22.
//  Copyright © 2019 Frontcell AB. All rights reserved.
//

import Foundation
import Firebase
import Crashlytics

class FabegeAnalytics {
     
     static let useCrashlytics = true
     
     class func trackEvent(_ name : String, parameters : [String : Any]?) {
          if useCrashlytics {
               Answers.logCustomEvent(withName: name, customAttributes: parameters)
          }
          Analytics.logEvent(name, parameters: parameters)
          
     }
     
     class func trackPageView(name : String) {
          Analytics.setScreenName(name, screenClass: nil)
     }
     
}
