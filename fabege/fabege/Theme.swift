//
//  Theme.swift
//  fabege
//
//  Created by Oscar Kockum on 2019-01-08.
//  Copyright © 2019 Frontcell AB. All rights reserved.
//

import UIKit

class WAWColors {
     let backgroundColor = UIColor(hexString: "FBFBFA")
     let purpleColor = UIColor(hexString: "8A2082")
     let grayColor = UIColor(hexString: "858278")
     
}

class WAWFonts {
     class func regular(size : CGFloat) -> UIFont {
          return UIFont(name: "ApercuPro-Regular", size: size)!
     }
     
     class func bold(size : CGFloat) -> UIFont {
          return UIFont(name: "ApercuPro-Bold", size: size)!
     }
}



extension UIColor {
     static let waw = WAWColors()
}


