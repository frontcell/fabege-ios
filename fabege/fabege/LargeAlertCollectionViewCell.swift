//
//  LargeAlertCollectionViewCell.swift
//  fabege
//
//  Created by Oscar Kockum on 2019-06-11.
//  Copyright © 2019 Frontcell AB. All rights reserved.
//

import UIKit



class LargeAlertCollectionViewCell: UICollectionViewCell {
     
     var onExitClicked: (() -> Void)?
     
     @IBOutlet weak var title: UILabel!
     @IBOutlet weak var message: UILabel!
     @IBOutlet weak var date: UILabel!
     
     static let df : DateFormatter = {
          let df = DateFormatter()
          // 10 juni 2019, 08:03
          df.dateFormat = "dd MMMM yyyy, HH:mm"
          return df
     }()
     
     func setAlert(alert : WAWAlert) {
          title.text = alert.title
          message.text = alert.message
          date.text = LargeAlertCollectionViewCell.df.string(from: alert.createdAt)
     }
     
     @IBAction func exitAction(_ sender: Any) {
          onExitClicked?()
     }
}
