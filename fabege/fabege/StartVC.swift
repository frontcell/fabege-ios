//
//  StartVC.swift
//  fabege
//
//  Created by Oscar Kockum on 2019-02-12.
//  Copyright © 2019 Frontcell AB. All rights reserved.


import UIKit
import StoreKit


class StartVC: UIViewController, WAWInitiator, SearchVCDelegate {
    
    @IBOutlet weak var propertyView: UIView!
    @IBOutlet weak var wawView: UIView!
    @IBOutlet weak var bicycleServiceView: UIView!
    @IBOutlet weak var alertCollectionView: UICollectionView!
    @IBOutlet weak var alertIcon: UIImageView!
    @IBOutlet weak var bicycleServiceIcon: UIImageView!
    @IBOutlet weak var bicycleServiceLabel: UILabel!
    
    private var alertViewHandler : AlertViewUIHandler?
    private weak var subscription : WAWService.Subscription?
    private weak var bicycleSubscription : WAWService.Subscription?
    
    var bicycleService : WAWServices.BicycleService?
    var bicycleEnabled = true
    var validBicycleLocations = 0
    
    deinit {
        subscription?.detach()
        bicycleSubscription?.detach()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        propertyView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(StartVC.didTapPropertyView)))
        wawView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(StartVC.didTapWAWView)))
        bicycleServiceView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(StartVC.didTapBicycleView)))
        validateApp()
        
        let service = WAWService()
        subscription = service.subscribeToServices(changes: { (services) in
            self.reloadUI(services: services)
        })
        bicycleSubscription = service.subscribeToBicycleServiceLocation { (locations) in
            self.validBicycleLocations = locations.count
            self.updateBicycleVisibility()

        }
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        
        if alertViewHandler == nil {
            alertViewHandler = AlertViewUIHandler(collectionView: alertCollectionView, alertIcon: alertIcon)

        }
    }
    
    func reloadUI(services : WAWServices?) {
        guard let bicycleService = services?.bicycleService else {
            bicycleServiceView.isHidden = true
            return
        }
        
        self.bicycleEnabled = bicycleService.enabled
        self.bicycleService = bicycleService
        if let url = URL(string: bicycleService.iconUrl) {
            bicycleServiceIcon.kf.setImage(with: url, options: [.transition(.fade(0.2))])
            bicycleServiceIcon.isHidden = false
        } else {
            bicycleServiceIcon.isHidden = true
        }
        bicycleServiceLabel.text = bicycleService.buttonTitle.insertNewLines()
        updateBicycleVisibility()
    }
    
    func updateBicycleVisibility() {
        let showBicyclesView = bicycleEnabled && validBicycleLocations > 0
        bicycleServiceView.isHidden = !showBicyclesView
    }
    
    @objc func didTapPropertyView() {
        loadProperties { (success) in
            if (model.hasFavorites()) {
                self.performSegue(withIdentifier: "properties", sender: nil)
            } else {
                self.performSegue(withIdentifier: "PresentSearch", sender: nil)
            }
        }
    }
    
    @objc func didTapWAWView() {
        initiateWAW()
    }
    
    func loadProperties(completion : @escaping ((Bool) -> ())) {
        let dialog = DialogController.display(from: self, title: nil, highlightedText: "Hämtar fastigheter", defaultText: nil, lightMode: true, dialogType: .loading)
        WebserviceInstance.loadStructures({ (response) -> Void in
            model.setProperties(response)
            dialog.dismissDialog(animated: true, completion: {
                completion(true)
            })
        }, failure: { (error) -> Void in
            dialog.dismissDialog(completion: nil)
            let alertView = UIAlertController(title: "Något gick fel", message: "Försök igen", preferredStyle: .alert)
            alertView.addAction(UIAlertAction(title: "OK", style: .default, handler: { (action) in
                completion(false)
            }))
            self.present(alertView, animated: true, completion: nil)
        })
    }
    
    @objc func didTapBicycleView() {
        if model.hasProperties() {
            self.continueToBicycleServiceView()
        } else {
            loadProperties { (success) in
                if success {
                    self.continueToBicycleServiceView()
                }
            }
        }
    }
    
    private func continueToBicycleServiceView() {
        if let favorite = model.getFavorites().first {
            performSegue(withIdentifier: "bicycleServiceView", sender: favorite)
        } else {
            performSegue(withIdentifier: "bicycleIntroView", sender: nil)
        }
    }
    
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "PresentSearch" {
            let navigationVC = segue.destination as! UINavigationController;
            let searchVC = navigationVC.visibleViewController as! SearchVC;
            searchVC.delegate = self
            searchVC.firstLaunch = true
        } else if let vc = segue.destination as? BicycleIntroViewController {
            vc.service = bicycleService!
        } else if let vc = segue.destination as? BicycleServiceVC {
            vc.selectedProperty = sender as? Property
            vc.service = bicycleService! 
        }
    }
    
    func didSelectProperty(_ property: Property) {
        
    }
    
    func didCloseView() {
        performSegue(withIdentifier: "properties", sender:nil)
    }
    
    
    
    override func viewWillAppear(_ animated: Bool) {
        navigationController?.setNavigationBarHidden(true, animated: true)
    }
    
    func productViewControllerDidFinish(_ viewController: SKStoreProductViewController) {
        dismiss(animated: true, completion: nil)
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}


