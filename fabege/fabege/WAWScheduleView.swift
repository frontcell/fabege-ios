//
//  WAWScheduleView.swift
//  fabege
//
//  Created by Oscar Kockum on 2018-12-19.
//  Copyright © 2018 Frontcell AB. All rights reserved.
//

import UIKit

protocol WAWScheduleViewListener : class {
     func onBookingsChanged(bookings : [WAWBookingReference])
}

class WAWScheduleView: UIView, TimeSlotStateListener {
     

     static let rowHeight = 49
     static let topPadding = 30
     static let bottomPadding = 50
     static let dateViewWidth = 68
     
     weak var bookingsListener : WAWScheduleViewListener?
     
     static let onlyDayFormatter : DateFormatter = {
          let df = DateFormatter()
          df.locale = Locale(identifier: "sv_SE")
          df.dateFormat = "EEE"
          return df
     }()
     
     static let shortDateFormatter : DateFormatter = {
          let df = DateFormatter()
          df.locale = Locale(identifier: "sv_SE")
          df.dateFormat = "d MMM"
          return df
     }()

     
     enum TimeSlotViewStatus {
          case unselected
          case selected
          case booked
          case closed
          case fullyBooked
          case empty
     }
     
     private lazy var mainStackView : UIStackView = {
          let view = UIStackView()
          view.translatesAutoresizingMaskIntoConstraints = false
          view.axis = .vertical
          return view
     }()
     
     private lazy var daysStackView : UIStackView = {
          let view = UIStackView()
          view.translatesAutoresizingMaskIntoConstraints = false
          view.axis = .vertical
          return view
     }()
     
     
     private lazy var slotTimeLabelContainer : UIStackView = {
          let stackView = UIStackView()
          stackView.distribution = .fillEqually
          stackView.translatesAutoresizingMaskIntoConstraints = false
          stackView.axis = .horizontal
          return stackView
     }()
     
     private lazy var headerView : UIView = {
          let view = UIView()
          view.backgroundColor = UIColor.waw.backgroundColor
          view.translatesAutoresizingMaskIntoConstraints = false
          view.heightAnchor.constraint(equalToConstant: CGFloat(WAWScheduleView.topPadding)).isActive = true
          view.addSubview(slotTimeLabelContainer)
          NSLayoutConstraint.activate([
               slotTimeLabelContainer.topAnchor.constraint(equalTo: view.topAnchor),
               slotTimeLabelContainer.leftAnchor.constraint(equalTo: view.leftAnchor, constant:CGFloat(WAWScheduleView.dateViewWidth)),
               slotTimeLabelContainer.rightAnchor.constraint(equalTo: view.rightAnchor),
               slotTimeLabelContainer.bottomAnchor.constraint(equalTo: view.bottomAnchor)])
          return view
     }()
     
  
     private var widthConstraint : NSLayoutConstraint?
     private var schedules = [WAWSchedule]()
     private var userId = ""
     private var propertyId = ""
     private var bookings = Set<WAWBookingReference>()

     var property : WAWProperty? {
          didSet {
               slotTimeLabelContainer.arrangedSubviews.forEach { (view) in
                    view.removeFromSuperview()
               }
               guard let property = property else {
                    return
               }
               for hours in property.openingHours {
                    let label = UILabel()
                    let start = hours.start.replacingOccurrences(of: ":00", with: "")
                    let end = hours.end.replacingOccurrences(of: ":00", with: "")
                    label.text = "kl \(start)-\(end)"
                    label.font = WAWFonts.bold(size: 15)
                    label.textAlignment = .left
                    slotTimeLabelContainer.addArrangedSubview(label)
               }
          }
     }
     
     required init?(coder aDecoder: NSCoder) {
          super.init(coder: aDecoder)
          setupView()
     }
     
     override init(frame: CGRect) {
          super.init(frame: frame)
          setupView()
     }
     
     func resetView() {
          bookings.removeAll()
          reloadDays()
     }
     
     private func setupView() {
          backgroundColor = UIColor.waw.backgroundColor
          translatesAutoresizingMaskIntoConstraints = false
          clipsToBounds = true
          let crossedView = ScheduleBackgroundView()
          crossedView.backgroundColor = UIColor.waw.backgroundColor
          crossedView.translatesAutoresizingMaskIntoConstraints = false
          addSubview(crossedView)
          addSubview(view: mainStackView, pinToEdges: true)

          NSLayoutConstraint.activate([
               crossedView.topAnchor.constraint(equalTo: topAnchor),
               crossedView.leftAnchor.constraint(equalTo: leftAnchor, constant:CGFloat(WAWScheduleView.dateViewWidth)),
               crossedView.rightAnchor.constraint(equalTo: rightAnchor),
               crossedView.bottomAnchor.constraint(equalTo: bottomAnchor)])
          mainStackView.addArrangedSubview(headerView)
          mainStackView.addArrangedSubview(daysStackView)
     }
     
     
     
     func setSchedule(schedules : [WAWSchedule], userId : String, property : WAWProperty) {
          self.schedules = schedules
          self.userId = userId
          self.propertyId = property.id
          self.property = property
          reloadDays()
          invalidateIntrinsicContentSize()
     }
     
     private func reloadDays() {
          daysStackView.arrangedSubviews.forEach { (view) in
               view.removeFromSuperview()
          }
          schedules.enumerated().forEach { (idx, schedule) in
               let view = buildDayview(schedule: schedule)
               daysStackView.addArrangedSubview(view)
          }
          
     }
     
     private func buildDayview(schedule : WAWSchedule) -> UIView {
          let date = schedule.loadDate()
          let view = UIStackView()
          view.axis = .horizontal
          view.spacing = 0
          // Date view
          let dateView = UIStackView()
          dateView.spacing = -5
          dateView.axis = .vertical
          let dayLabel = UILabel()
          dayLabel.textAlignment = .right
          dayLabel.font = WAWFonts.bold(size: 15)
          dayLabel.text = WAWScheduleView.onlyDayFormatter.string(from: date)
          
          let dateLabel = UILabel()
          dateLabel.text = WAWScheduleView.shortDateFormatter.string(from: date).lowercased()
          dateLabel.textAlignment = .right
          dateLabel.font = WAWFonts.regular(size: 14)
          dateView.addArrangedSubview(dayLabel)
          dateView.addArrangedSubview(dateLabel)
          let containerView = UIView()
          containerView.backgroundColor = UIColor.waw.backgroundColor
          dateView.translatesAutoresizingMaskIntoConstraints = false
          containerView.addSubview(dateView)
          containerView.widthAnchor.constraint(equalToConstant: CGFloat(WAWScheduleView.dateViewWidth)).isActive = true
          NSLayoutConstraint.activate([dateView.topAnchor.constraint(equalTo: containerView.topAnchor),
                                       dateView.leftAnchor.constraint(equalTo: containerView.leftAnchor),
                                       dateView.rightAnchor.constraint(equalTo: containerView.rightAnchor, constant:-7),
                                       dateView.bottomAnchor.constraint(equalTo: containerView.bottomAnchor)])
          
          view.addArrangedSubview(containerView)
          
          // Time slots
          view.addArrangedSubview(buildTimeSlotView(schedule: schedule))
          
          return view
     }
     
     private func isOngoingOrUpcoming(schedule : WAWSchedule, idx : Int) -> Bool {
          
          guard idx < (property?.openingHours.count ?? -1), let hours = property?.openingHours[idx],
               let endDate = DateUtil.timestamp(time: hours.end, dateKey: schedule.dateString) else {
               return true
          }
          if endDate < Date().timeIntervalSince1970 {
               return false
          }
          return true // default return to true.
     }
     
     private func buildTimeSlotView(schedule : WAWSchedule) -> UIView {
          let view = TimeSlotView()
          view.clickListener = self
          view.date = schedule.dateString
          view.translatesAutoresizingMaskIntoConstraints = false
          view.heightAnchor.constraint(equalToConstant: CGFloat(WAWScheduleView.rowHeight)).isActive = true
          var viewStates = [WAWScheduleView.TimeSlotViewStatus]()
          for (idx, slot) in schedule.timeSlots.enumerated() {
               
               guard isOngoingOrUpcoming(schedule: schedule, idx: idx) else {
                    viewStates.append(.empty)
                    continue
               }
               
               switch slot.status {
               case .available:
                    let booking = bookings.first { (reference) -> Bool in
                         return reference.date == schedule.dateString && reference.slotIdx == idx
                    }
                    if let booking = booking {
                         if booking.add {
                              viewStates.append(.selected)
                         } else {
                              viewStates.append(.unselected)
                         }
                    } else {
                         if slot.ids.contains(userId){
                              viewStates.append(.booked)
                         } else {
                              if let property = property, slot.ids.count >= property.availableSeats(dateKey: schedule.dateString) {
                                   viewStates.append(.fullyBooked)
                              }
                              else {
                                   viewStates.append(.unselected)
                              }
                         }
                    }
               case .closed:
                    viewStates.append(.closed)
               case .holiday:
                    viewStates.append(.empty)
               }
          }
          view.states = viewStates
          return view
     }
     
     override var intrinsicContentSize: CGSize {
          let height = CGFloat(WAWScheduleView.topPadding + WAWScheduleView.bottomPadding + schedules.count * WAWScheduleView.rowHeight)
          return CGSize(width:UIView.noIntrinsicMetric,height:height)
     }
     
     func onStateChanged(date: String, idx: Int, from : WAWScheduleView.TimeSlotViewStatus, to : WAWScheduleView.TimeSlotViewStatus) {
          let removeBooking = (WAWBookingReference(date: date, slotIdx: idx, propertyId: propertyId, add:false))
          let addBooking = (WAWBookingReference(date: date, slotIdx: idx, propertyId: propertyId, add:true))
          switch (from, to) {
          case (.unselected,.selected):
               if bookings.contains(removeBooking) {
                    bookings.remove(removeBooking)
               } else {
                    bookings.update(with: (WAWBookingReference(date: date, slotIdx: idx, propertyId: propertyId, add:true)))
               }
          case (.selected,.unselected):
               bookings.remove(addBooking)
          case (.booked,.unselected):
               bookings.update(with: (WAWBookingReference(date: date, slotIdx: idx, propertyId: propertyId, add:false)))
          default:
               break
          }
          bookingsListener?.onBookingsChanged(bookings: Array(bookings))
          reloadDays()
     }
     
   
 
}

protocol TimeSlotStateListener : class {
     func onStateChanged(date: String, idx: Int, from : WAWScheduleView.TimeSlotViewStatus, to : WAWScheduleView.TimeSlotViewStatus)
}

class TimeSlotView : UIView {
     
     var date = ""
     var startIdx = 0
     
     weak var clickListener : TimeSlotStateListener?
     
     var states = [WAWScheduleView.TimeSlotViewStatus]() {
          didSet {
               drawStates()
          }
     }
     
     private lazy var stackView : UIStackView = {
          let view = UIStackView()
          view.translatesAutoresizingMaskIntoConstraints = false
          view.axis = .horizontal
          view.distribution = .fillEqually
          view.alignment = .fill
          view.spacing = 5
          return view
     }()
     
     required init?(coder aDecoder: NSCoder) {
          super.init(coder: aDecoder)
          setupView()
     }
     
     override init(frame: CGRect) {
          super.init(frame: frame)
          setupView()
     }
     
     private func setupView() {
          backgroundColor = UIColor.waw.backgroundColor
          translatesAutoresizingMaskIntoConstraints = false
          addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(TimeSlotView.didTapView(sender:))))
          addSubview(stackView)
          NSLayoutConstraint.activate([
               stackView.topAnchor.constraint(equalTo: topAnchor),
               stackView.leftAnchor.constraint(equalTo: leftAnchor),
               stackView.rightAnchor.constraint(equalTo: rightAnchor),
               stackView.bottomAnchor.constraint(equalTo: bottomAnchor, constant:-5)])
     }
     
     private func drawStates() {
          stackView.arrangedSubviews.forEach { (view) in
               view.removeFromSuperview()
          }
          
          var mergedStates = [WAWScheduleView.TimeSlotViewStatus]()
          var lastState = WAWScheduleView.TimeSlotViewStatus.unselected
          states.forEach { (state) in
               if lastState != state || state == .unselected {
                    mergedStates.append(state)
               }
               lastState = state
          }
          
          for state in mergedStates {
               let view = UIView()
               switch state {
                    
               case .unselected:
                    view.backgroundColor = UIColor(hexString: "EDE4ED")
               case .selected:
                    view.backgroundColor = UIColor(hexString: "B98CBC")
                    view.addInnerShadow(width: 8)
               case .booked:
                    view.backgroundColor = UIColor(hexString: "7E1980")
                    let label = UILabel()
                    label.text = "Bokad"
                    label.textColor = UIColor.white
                    label.font = WAWFonts.regular(size: 15)
                    label.textAlignment = .center
                    view.addSubview(view: label, pinToEdges: true)
               case .closed:
                    view.backgroundColor = UIColor(hexString: "E5E1DC")
                    let label = UILabel()
                    label.text = "Stängt"
                    label.textColor = UIColor(hexString: "858278")
                    label.font = WAWFonts.regular(size: 15)
                    label.textAlignment = .center
                    view.addSubview(view: label, pinToEdges: true)

               case .empty:
                    view.backgroundColor = UIColor.clear
                    backgroundColor = UIColor.clear
               case .fullyBooked:
                    view.backgroundColor = UIColor(hexString: "E5E1DC")
                    let label = UILabel()
                    label.text = "Fullbokat"
                    label.textColor = UIColor(hexString: "858278")
                    label.font = WAWFonts.regular(size: 15)
                    label.textAlignment = .center
                    view.addSubview(view: label, pinToEdges: true)

               }

               stackView.addArrangedSubview(view)
          }
     }
     
     @objc private func didTapView(sender : UITapGestureRecognizer) {
          guard states.count > 0 else {
               return
          }
          let point = sender.location(in: self)
          let slotWidth = frame.width / CGFloat(states.count)
          
          let clickIdx = Int(point.x / slotWidth)
          
          let state = states[clickIdx]
          if state == .selected {
               states[clickIdx] = .unselected
               clickListener?.onStateChanged(date: date, idx: clickIdx, from: state, to:.unselected)
               //drawStates()
          } else if state == .unselected {
               states[clickIdx] = .selected
               clickListener?.onStateChanged(date: date, idx: clickIdx, from: state, to:.selected)
               //drawStates()
          } else if state == .booked {
               states[clickIdx] = .unselected
               clickListener?.onStateChanged(date: date, idx: clickIdx, from: state, to:.unselected)
               //drawStates()
          }
          
     }
     
    
}

class ScheduleBackgroundView : UIView {
     
     static let lineAngleInRadians : CGFloat = 0.41887902048
     static let lineSpacing : CGFloat = 10
     var endXDiff : CGFloat = 0
     let xStartingPosition : CGFloat = 2
     
     override func layoutSubviews() {
          let height = frame.height
          endXDiff = tan(ScheduleBackgroundView.lineAngleInRadians) * height
     }
     
     override func draw(_ rect: CGRect) {
          let aPath = UIBezierPath()
          aPath.lineWidth = 1
          var startX = xStartingPosition
          var endX = xStartingPosition - endXDiff
          while rect.width > endX {
               aPath.move(to: CGPoint(x:startX, y:0))
               aPath.addLine(to: CGPoint(x:endX, y:rect.height))
               startX += ScheduleBackgroundView.lineSpacing
               endX = startX - endXDiff
          }
          aPath.close()
          UIColor(hexString: "F3F0EE").set()
          aPath.stroke()

     }
}
