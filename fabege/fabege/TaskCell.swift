//
//  MatterCell.swift
//  fabege
//
//  Created by Patrik Billgert on 08/04/15.
//  Copyright (c) 2015 Frontcell AB. All rights reserved.
//

import UIKit

protocol TaskCellDelegate {
    func clickedURL(_ url: String)
}


class TaskCell: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var infoLabel: KILabel!
    @IBOutlet weak var dateLabel: UILabel!
    
    var delegate:TaskCellDelegate?
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()

        infoLabel.urlLinkTapHandler = { label, url, range in
            self.delegate?.clickedURL(url)
        }
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        if (NSFoundationVersionNumber <= NSFoundationVersionNumber_iOS_7_1) {
            contentView.layoutIfNeeded()
            infoLabel.preferredMaxLayoutWidth = infoLabel.frame.width
        }
    }
    
    
    
    // MRAK: ContextLabelDelegate
    
    /*func contextLabel(contextLabel: ContextLabel, beganTouchOf text: String, with linkRangeResult: LinkRangeResult) {
        //delegate?.clickedURL(text)
    }
    
    func contextLabel(contextLabel: ContextLabel, movedTouchTo text: String, with linkRangeResult: LinkRangeResult) {
        //infoLabel.text = "movedTouchTo: \(text)" + "\nRange: \(linkRangeResult.linkRange)"
    }
    
    func contextLabel(contextLabel: ContextLabel, endedTouchOf text: String, with linkRangeResult: LinkRangeResult) {
        delegate?.clickedURL(text)
    }*/
}
