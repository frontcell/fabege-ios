//
//  InsetsTextField.swift
//  fabege
//
//  Created by Patrik Billgert on 24/03/15.
//  Copyright (c) 2015 Frontcell AB. All rights reserved.
//

import UIKit

class InsetsTextField: UITextField {

    /*
    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func drawRect(rect: CGRect) {
        // Drawing code
    }
    */

    override func textRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.insetBy(dx: 10, dy: 0);
    }
    
    override func editingRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.insetBy(dx: 10, dy: 0);
    }
}
