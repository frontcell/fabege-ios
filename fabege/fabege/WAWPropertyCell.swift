//
//  WAWPropertyCell.swift
//  fabege
//
//  Created by Oscar Kockum on 2018-12-18.
//  Copyright © 2018 Frontcell AB. All rights reserved.
//

import UIKit
import Kingfisher

class WAWPropertyCell: UITableViewCell {
     
     @IBOutlet weak var propertyIcon: UIImageView!
     @IBOutlet weak var propertyLocation: UILabel!
     @IBOutlet weak var propertyTitle: UILabel!
     @IBOutlet weak var upcomingBookingsView: UIView!
     @IBOutlet weak var upcomingBookingsCount: UILabel!
     @IBOutlet weak var topView: UIView!
     @IBOutlet weak var alertIcon: UIImageView!
    
     var property : WAWProperty? {
          didSet {
               if let property = property {
                    propertyTitle.text = property.name
                    propertyLocation.text = property.address
                    if let url = URL(string: property.iconUrl) {
                         propertyIcon.kf.indicatorType = .activity
                         propertyIcon.kf.setImage(with: url)
                    }
               } else {
                    propertyTitle.text = nil
                    propertyIcon.image = nil
               }
          }
     }
     
     override func awakeFromNib() {
          super.awakeFromNib()
          // Initialization code
     }
     
     
     
}
