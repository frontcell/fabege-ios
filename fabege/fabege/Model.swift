//
//  Model.swift
//  fabege
//
//  Created by Patrik Billgert on 18/03/15.
//  Copyright (c) 2015 Frontcell AB. All rights reserved.
//

import UIKit
import CoreLocation
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}


var model: Model = Model()

class Model: NSObject {
    
    fileprivate var _properties: [Property] = []
    var tenant: Tenant!
    
    // MARK: - Lifecycle
    
    override init() {
        super.init()
        
        // Unwrapping saved tenant
        let defaults = UserDefaults.standard
        if let savedTenantDict: AnyObject = defaults.dictionary(forKey: "savedTenant") as AnyObject? {
            let savedTenant = Tenant()
            savedTenant.company = savedTenantDict.object(forKey: "company")! as? String
            savedTenant.name = savedTenantDict.object(forKey: "name")! as? String
            savedTenant.email = savedTenantDict.object(forKey: "email")! as? String
            savedTenant.phone = savedTenantDict.object(forKey: "phone")! as? String
            
            tenant = savedTenant
        } else {
            tenant = Tenant()
        }
    }
    
    func hasFavorites() -> Bool {
        if let favDicts = UserDefaults.standard.array(forKey: "favoriteDicts") {
            return favDicts.count > 0
        } else {
            return false
        }
    }
    
    func hasProperties() -> Bool {
        return _properties.count > 0
    }
    
    func bicycleServiceProperty() -> Property? {
        if let data = UserDefaults.standard.data(forKey: "bicycleServiceProperty"),
            let property = NSKeyedUnarchiver.unarchiveObject(with: data) as? Property {
            return property
        }
        return nil
    }
    
    func setBicycleServiceFavorites(property : Property?) {
        if let property = property {
            let data = NSKeyedArchiver.archivedData(withRootObject: property)
            UserDefaults.standard.set(data, forKey: "bicycleServiceProperty")
        } else {
            UserDefaults.standard.removeObject(forKey: "bicycleServiceProperty")
        }
        
    }
    
    func setFavorites(_ properties : [Property]) {
        var propertyDicts: [Dictionary<String, AnyObject>] = []
        var propertySet = Set<String>()
        
        for property in properties {
            let propertyDict = ["id": property.sourceId, "date": property.date] as [String : Any]
            propertyDicts.append(propertyDict as [String : AnyObject])
            propertySet.insert("\(property.propertyId)")
        }

        UserDefaults.standard.set(propertyDicts, forKey: "favoriteDicts")
        UserDefaults.standard.synchronize()
        
        if !AppDelegate.deviceToken().isEmpty {
            WebserviceInstance.uploadDeviceToken(AppDelegate.deviceToken(), propertyIds: propertySet.joined(separator: ","))
        }
    }
    
    func getFavorites() -> [Property] {
        var retVal = [Property]()
        // ugly / bad / strange code.. redo..
        if let favDicts = UserDefaults.standard.array(forKey: "favoriteDicts") {
            for dict in favDicts {
                let id = (dict as AnyObject).value(forKey: "id") as! Int
                let date = (dict as AnyObject).value(forKey: "date") as! Date
                
                for property in _properties {
                    if id == property.sourceId {
                        property.date = date
                        retVal.append(property)
                    }
                }
            }
             
            retVal.sort(by: {
                $0.date.compare($1.date as Date) == ComparisonResult.orderedAscending })
            }
        
        return retVal
    }
    
    
    
    // MARK: - NSUserDefaults
    
    func saveTenant() {
        let tenantDict: Dictionary<String, AnyObject> = [
            "company":model.tenant.company as AnyObject,
            "name":model.tenant.name as AnyObject,
            "email":model.tenant.email as AnyObject,
            "phone":model.tenant.phone as AnyObject,
        ]
        
        UserDefaults.standard.set(tenantDict, forKey: "savedTenant")
        UserDefaults.standard.synchronize()
    }
    
    func getProperties(_ distanceSort : Bool) -> [Property] {
        var sortedArray = Array(_properties)
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        
        if distanceSort == true && appDelegate.lastLocation != nil {
            sortedArray.sort(by: {
                let location1 = CLLocation(latitude: $0.coordinate.latitude, longitude: $0.coordinate.longitude)
                let location2 = CLLocation(latitude: $1.coordinate.latitude, longitude: $1.coordinate.longitude)
                return appDelegate.lastLocation.distance(from: location1) < appDelegate.lastLocation.distance(from: location2)
            })
        } else {
            sortedArray = sortedArray.sorted{$0.address < $1.address}
        }
        
        return sortedArray
    }
    
    func setProperties(_ properties : [Property]) {
        _properties = properties
    }
}
