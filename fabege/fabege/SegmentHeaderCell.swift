//
//  SegmentHeaderCell.swift
//  fabege
//
//  Created by Patrik Billgert on 26/03/15.
//  Copyright (c) 2015 Frontcell AB. All rights reserved.
//

import UIKit

protocol SegmentCellDelegate {
    func sortProperties(_ distance : Bool)
}

class SegmentHeaderCell: UITableViewCell {
    
    @IBOutlet weak var distanceButton: UIButton!
    @IBOutlet weak var alphabetButton: UIButton!
    
    var delegate:SegmentCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setSortMode(_ distance : Bool) {
        distanceButton.isSelected = distance
        alphabetButton.isSelected = !distance
        if distance {
            distanceButton.backgroundColor = UIColor(red: 226.0/255.0, green: 226.0/255.0, blue: 225.0/255.0, alpha: 1.0)
            alphabetButton.backgroundColor = UIColor(red: 234.0/255.0, green: 235.0/255.0, blue: 232.0/255.0, alpha: 1.0)
        } else {
            alphabetButton.backgroundColor = UIColor(red: 226.0/255.0, green: 226.0/255.0, blue: 225.0/255.0, alpha: 1.0)
            distanceButton.backgroundColor = UIColor(red: 234.0/255.0, green: 235.0/255.0, blue: 232.0/255.0, alpha: 1.0)
        }
    }
    
    @IBAction func distance(_ sender : AnyObject?) {
        setSortMode(true)
        delegate?.sortProperties(true)
    }

    @IBAction func alphabet(_ sender : AnyObject?) {
        setSortMode(false)
        delegate?.sortProperties(false)
    }
}
