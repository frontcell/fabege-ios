//
//  RegisterVC.swift
//  fabege
//
//  Created by Patrik Billgert on 18/03/15.
//  Copyright (c) 2015 Frontcell AB. All rights reserved.
//

import UIKit

class RegisterVC: UIViewController, UITextFieldDelegate, UITextViewDelegate, UIAlertViewDelegate, SearchVCDelegate, PhotoVCDelegate {
    
    @IBOutlet weak var addressTextField: UITextField!
    @IBOutlet weak var companyTextField: UITextField!
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var phoneTextField: UITextField!
    @IBOutlet weak var descriptionTextView: UITextView!
    @IBOutlet weak var bottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var imageViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var imageViewWidthConstraint: NSLayoutConstraint!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var imageButton: UIButton!
    @IBOutlet weak var removeButton: UIButton!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var registerButton: UIButton!
    @IBOutlet weak var loadingView: UIView!
    
    var activeTextField: UITextField!
    var chosenImage: UIImage?
    var selectedProperty : Property!
    let delay = 0.2 * Double(NSEC_PER_SEC)
    
    @IBOutlet weak var callInfoStackView: UIStackView!
    
    
    // MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationItem.title = "Service & felanmälan"
        
        companyTextField.text = model.tenant?.company
        nameTextField.text = model.tenant?.name
        emailTextField.text = model.tenant?.email
        phoneTextField.text = model.tenant?.phone
        
        showImageView(false)
        setupAppearance()
        registerForKeyboardNotifications()
        updateProperty()
        
        callInfoStackView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(RegisterVC.didTapCallView)))
        
        
    }
     
     override var preferredStatusBarStyle: UIStatusBarStyle {
          return .lightContent
     }
     
     override func viewWillAppear(_ animated: Bool) {
          setNeedsStatusBarAppearanceUpdate()
           navigationController?.setNavigationBarHidden(false, animated: true)
     }
    
    @objc func didTapCallView() {
        UIApplication.shared.openURL(URL(string: "tel://+4620990990")!)
    }
    
    func updateProperty() {
        addressTextField.text = selectedProperty.address
    }
    
    
    // MARK: - Navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "PresentSearch" {
            let navigationVC = segue.destination as! UINavigationController;
            let searchVC = navigationVC.visibleViewController as! SearchVC;
            searchVC.changeProperty = true
            searchVC.selectedProperty = selectedProperty
            searchVC.favoriteProperties =  model.getFavorites()
            searchVC.delegate = self
        } else if segue.identifier == "PresentPhoto" {
            let navigationVC = segue.destination as! UINavigationController;
            let vc = navigationVC.visibleViewController as! PhotoVC;
            vc.delegate = self
        }
    }
    
    
    
    // MARK: - IBActions
    
    @IBAction func dismissKeyboard(_ sender: AnyObject) {
        if activeTextField != nil && activeTextField.isFirstResponder {
            activeTextField.resignFirstResponder()
        } else if descriptionTextView.isFirstResponder {
            descriptionTextView.resignFirstResponder()
        }
    }
    
    @IBAction func removeImage(_ sender: AnyObject) {
        let alertView = UIAlertController(title: "Är du säker?", message: "", preferredStyle: .alert)
        alertView.addAction(UIAlertAction(title: "Ta bort", style: .default, handler: { (action) in
            self.showImageView(false)
        }))
        alertView.addAction(UIAlertAction(title: "Avbryt", style: .cancel, handler: { (action) in
            
        }))
        present(alertView, animated: true, completion: nil)
    }
    
    
    @IBAction func changeProperty(_ sender: AnyObject) {
        
    }
    
    @IBAction func register(_ sender: AnyObject) {
        if hasFilledOutForm() {
            if isEmail(emailTextField.text!) == true {
                showActivityIndicator(true)
                
                model.tenant.company = companyTextField.text
                model.tenant.name = nameTextField.text
                model.tenant.email = emailTextField.text
                model.tenant.phone = phoneTextField.text
                
                WebserviceInstance.createTask(selectedProperty.sourceId, company: companyTextField.text!, contact: nameTextField.text!, email: emailTextField.text!, phone: phoneTextField.text!, description: descriptionTextView.text!, image: chosenImage, completion: { () -> Void in
                    self.showActivityIndicator(false)
                    let alertView = UIAlertController(title: "Tack för din anmälan.", message: "En bekräftelse har nu skickats till din e-post.", preferredStyle: .alert)
                    alertView.addAction(UIAlertAction(title: "OK", style: .default, handler: { (action) in
                        _ = self.navigationController?.popViewController(animated: false)
                    }))
                    self.present(alertView, animated: true, completion: nil)
                    FabegeAnalytics.trackEvent("Service_Request_Success", parameters: ["PropertyName" : self.selectedProperty.name,
                                                        "PropertyAdress" : self.selectedProperty.address,
                                                        "Image" : self.chosenImage != nil ? 1 : 0])
                    
                }) { (error) -> Void in
                    self.showActivityIndicator(false)
                    let alertView = UIAlertController(title: "Något gick fel", message: "Försök igen senare", preferredStyle: .alert)
                    alertView.addAction(UIAlertAction(title: "OK", style: .cancel, handler: { (action) in
                    }))
                    self.present(alertView, animated: true, completion: nil)
                    
                    FabegeAnalytics.trackEvent("Service_Request_Fail", parameters: ["PropertyName" : self.selectedProperty.name,
                                                                            "PropertyAdress" : self.selectedProperty.address,
                                                                            "Image" : self.chosenImage != nil ? 1 : 0])
                }
                model.saveTenant()
            } else {
                let alertView = UIAlertController(title: "Fyll i din e-post", message: nil, preferredStyle: .alert)
                alertView.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
                present(alertView, animated: true, completion: nil)
                
            }
        } else {
            let alertView = UIAlertController(title: "Fyll i alla fält", message: nil, preferredStyle: .alert)
            alertView.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
            present(alertView, animated: true, completion: nil)
        }
    }
    
    
    
    // MARK: - IOAlertViewDelegate
    
    
    
    
    // MARK: - UITextFieldDelegate
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        activeTextField = textField
        
        self.scrollView.scrollRectToVisible(self.activeTextField.frame, animated: true)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == addressTextField {
            companyTextField.becomeFirstResponder()
        } else if textField == companyTextField {
            nameTextField.becomeFirstResponder()
        } else if textField == nameTextField {
            emailTextField.becomeFirstResponder()
        } else if textField == emailTextField {
            phoneTextField.becomeFirstResponder()
        } else {
            descriptionTextView.becomeFirstResponder()
            return false
        }
        
        return true
    }
    
    
    
    // MARK: - UITextFieldDelegate
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        self.scrollView.scrollRectToVisible(self.descriptionTextView.frame, animated: true)
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if text == "\n" {
            //textView.resignFirstResponder()
            //return false
        }
        
        return true
    }
    
    
    
    // MARK: - Keyboard Notifications
    
    func registerForKeyboardNotifications() {
        NotificationCenter.default.addObserver(self, selector: #selector(RegisterVC.keyboardWasShown(_:)), name: UIResponder.keyboardDidShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(RegisterVC.keyboardWillBeHidden(_:)), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    @objc func keyboardWasShown(_ notification:Notification) {
        var info = (notification as NSNotification).userInfo!
        let keyboardFrame: CGRect = (info[UIResponder.keyboardFrameEndUserInfoKey] as! NSValue).cgRectValue
        
        view.layoutIfNeeded()
        UIView.animate(withDuration: 0.2, animations: {
            self.bottomConstraint.constant = keyboardFrame.size.height
            self.view.layoutIfNeeded()
        })
        
        if self.activeTextField != nil && self.activeTextField.isFirstResponder {
            self.scrollView.scrollRectToVisible(self.activeTextField.frame, animated: true)
        } else if self.descriptionTextView.isFirstResponder {
            self.scrollView.scrollRectToVisible(self.descriptionTextView.frame, animated: true)
        }
    }
    
    @objc func keyboardWillBeHidden(_ notification:Notification) {
        view.layoutIfNeeded()
        UIView.animate(withDuration: 0.2, animations: { () -> Void in
            self.bottomConstraint.constant = 44
            self.view.layoutIfNeeded()
        })
    }
    
    
    
    // MARK: - Other
    
    func isEmail(_ string: String) -> Bool {
        do {
            let regex = try NSRegularExpression(pattern: "^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,4}$", options: NSRegularExpression.Options.caseInsensitive)
            return regex.firstMatch(in: string, options: [], range: NSMakeRange(0, string.characters.count)) != nil
        } catch {
            return false;
        }
        
    }
    
    func showImageView(_ show: Bool) {
        if show {
            removeButton.isHidden = false
            imageView.isHidden = false
            imageViewHeightConstraint.constant = max(imageView.bounds.size.width,imageView.frame.width)
        } else {
            view.layoutIfNeeded()
            UIView.animate(withDuration: 0.2, animations: { () -> Void in
                self.removeButton.isHidden = true
                self.imageView.isHidden = true
                self.imageView.image = nil
                self.imageViewHeightConstraint.constant = 0
                self.view.layoutIfNeeded()
            })
        }
    }
    
    func setupAppearance() {
        let greyColor = UIColor(red: 213.0/255.0, green: 213.0/255.0, blue: 213.0/255.0, alpha: 1.0).cgColor
        
        addressTextField.layer.borderWidth = 1
        addressTextField.layer.borderColor = greyColor
        companyTextField.layer.borderWidth = 1
        companyTextField.layer.borderColor = greyColor
        nameTextField.layer.borderWidth = 1
        nameTextField.layer.borderColor = greyColor
        emailTextField.layer.borderWidth = 1
        emailTextField.layer.borderColor = greyColor
        phoneTextField.layer.borderWidth = 1
        phoneTextField.layer.borderColor = greyColor
        descriptionTextView.layer.borderWidth = 1
        descriptionTextView.layer.borderColor = greyColor
        descriptionTextView.textAlignment = .left;
        descriptionTextView.textContainerInset = UIEdgeInsets.init(top: 10, left: 5, bottom: 10, right: 5);
    }
    
    func didSelectProperty(_ property : Property) {
        selectedProperty = property
        updateProperty()
    }
     
     func didCloseView() {
          
     }
    
    func didTakePhoto(_ image: UIImage) {
        chosenImage = image
        imageView.image = image
        showImageView(true)
    }
    
    func hasFilledOutForm() -> Bool {
        return companyTextField.hasText && nameTextField.hasText && emailTextField.hasText && descriptionTextView.hasText
    }
    
    func showActivityIndicator(_ show: Bool) {
        UIView.animate(withDuration: 0.2, animations: {
            if show {
                self.loadingView.alpha = 1
            } else {
                self.loadingView.alpha = 0
            }
        }, completion: {
            (value: Bool) in
        })
    }
}
