//
//  SeparatorView.swift
//  Kyrkguide
//
//  Created by Patrik Billgert on 31/03/15.
//  Copyright (c) 2015 Frontcell AB. All rights reserved.
//

import UIKit

class SeparatorView: UIView {

    /*
    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func drawRect(rect: CGRect) {
        // Drawing code
    }
    */

    override func layoutSubviews() {
        super.layoutSubviews()
        
        let sortaPixel = 1.0 / UIScreen.main.scale;
        
        let topSeparatorView = UIView(frame: CGRect(x: 0, y: 0, width: frame.size.width, height: sortaPixel))
        topSeparatorView.isUserInteractionEnabled = false
        topSeparatorView.backgroundColor = backgroundColor
        
        addSubview(topSeparatorView)
        
        backgroundColor = UIColor.clear
        isUserInteractionEnabled = false
    }
}
