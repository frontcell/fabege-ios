//
//  WAWInstallParakeyVC.swift
//  fabege
//
//  Created by Oscar Kockum on 2019-01-14.
//  Copyright © 2019 Frontcell AB. All rights reserved.
//

import UIKit
import StoreKit

class WAWInstallParakeyVC : UIViewController, SKStoreProductViewControllerDelegate {
     
     @IBOutlet weak var titleLabel: UILabel!
     @IBOutlet weak var extraInfoLabel: UILabel!
     @IBOutlet weak var messageLabel: UILabel!
     @IBOutlet weak var connectButton: UIButton!
     @IBOutlet weak var installButton: UIButton!
     
     var onFinished: (() -> Void)?

     
     @IBAction func onActionButton(_ sender: Any) {
          let vc = SKStoreProductViewController()
          let params = [SKStoreProductParameterITunesItemIdentifier:1001574067]
          vc.delegate = self
          vc.loadProduct(withParameters: params) { (finished, error) in
               
          }
          self.present(vc, animated: true, completion: nil)
           NotificationCenter.default.addObserver(self, selector: #selector(HomeVC.didBecomeActive), name: UIApplication.didBecomeActiveNotification, object: nil)
     }
     
     override func viewWillAppear(_ animated: Bool) {
          toggleViewMode()
     }
     
     @objc func didBecomeActive() {
          toggleViewMode()
     }
     
     private func toggleViewMode() {
          let service = WAWService()
          
          installButton.isHidden = service.isKeyAppInstalled()
          connectButton.isHidden = !service.isKeyAppInstalled()
          
          if service.isKeyAppInstalled() {
               titleLabel.text = "parakey_header_installed".localize()
               messageLabel.text = "parakey_info_installed".localize()
               let extraText = "parakey_extra_info_installed".localize()
               extraInfoLabel.text = extraText
               extraInfoLabel.isHidden = extraText.isEmpty
          } else {
               titleLabel.text = "parakey_header_not_installed".localize()
               messageLabel.text = "parakey_info_not_installed".localize()
               let extraText = "parakey_extra_info_not_installed".localize()
               extraInfoLabel.text = extraText
               extraInfoLabel.isHidden = extraText.isEmpty
          }
     }
     
     func productViewControllerDidFinish(_ viewController: SKStoreProductViewController) {
         toggleViewMode()
         dismiss(animated: true, completion: nil)
     }
     
     
     
     
     @IBAction func onActionBack(_ sender: Any) {
          navigationController?.popViewController(animated: true)
     }
     
     @IBAction func actionConnect(_ sender: Any) {
          let service = WAWService()
          service.launchKeyApp(origin: "")
          onFinished?()
     }
}
