//
//  WAWEmailCheckController.swift
//  fabege
//
//  Created by Oscar Kockum on 2018-12-11.
//  Copyright © 2018 Frontcell AB. All rights reserved.
//

import UIKit

protocol WAWEmailDelegate : class {
     func didValidateEmail()
     func didCancel()
}

class WAWEmailCheckController : UIViewController, UITextFieldDelegate {
     
     @IBOutlet weak var textField: UITextField!
     @IBOutlet weak var scrollView: UIScrollView!
     @IBOutlet weak var actionButton: UIButton!
     @IBOutlet weak var headerText: UILabel!
     @IBOutlet weak var infoText: UILabel!
     @IBOutlet weak var extraInfoText: UILabel!
     @IBOutlet weak var contentStackView: UIStackView!
     @IBOutlet weak var emailTextContainer: UIView!
     @IBOutlet weak var mailIcon: UIImageView!
    @IBOutlet weak var inputContainer: UIStackView!
     var service = WAWService()
     private var isEmailVerified = false
     
     weak var delegate: WAWEmailDelegate?
     
     override func viewDidLoad() {
          //openMailButon.alignTextBelow(spacing: 5)
          actionButton.isEnabled = false
          emailTextContainer.addInnerShadow()
          mailIcon.tintColor = UIColor(hexString: "9B9B9B")
          headerText.text = "email_start_header".localize()
          infoText.text = "email_start_info".localize()
          textField.placeholder = "email_start_placeholder".localize()
          let extraText = "email_start_extra_info".localize()
          extraInfoText.text = extraText
          extraInfoText.isHidden = extraText.isEmpty

          
     }
     
     override func viewWillAppear(_ animated: Bool) {
          registerForKeyboardDidShowNotification { [weak self] (notification, size) in
               if let scrollView = self?.scrollView {
                    scrollView.contentInset.bottom = size.height
                    scrollView.scrollIndicatorInsets.bottom = size.height
                    DispatchQueue.main.async {
                         self?.scrollToBottom()
                    }
               }
          }
          
          registerForKeyboardWillHideNotification { [weak self] (notification) in
               self?.scrollView.contentInset.bottom = 0
               self?.scrollView.scrollIndicatorInsets.bottom = 0
          }
     }
     
     override func viewWillDisappear(_ animated: Bool) {
          NotificationCenter.default.removeObserver(self)
     }
     
     
     
     private func scrollToBottom() {
          scrollView.scrollRectToVisible(inputContainer.frame, animated: true)
     }
     
     private func localEmailValidation(newText : String) {
          actionButton.isEnabled = newText.isValidEmail()
    }
     
     private func validateEmail() {
          guard let text = textField.text, text.count > 3 else {
               return
          }
          textField.resignFirstResponder()
          let dialog = DialogController.display(from: self, title: "Snart klar", highlightedText: "Verifierar e-postadress", defaultText: nil, lightMode: true, dialogType: .loading)
          
          service.isEmailAvailable(email: text) { user in
               if let user = user {
                    let completion : (Bool, Error?) -> () = { (success, error) in
                         dialog.dismissDialog(completion: {
                              if success {
                                   self.enableSuccessMode()
                              } else {
                                   self.showErrorDialog()
                              }
                         })
                    }
                    self.service.initiateCustomTokenAuthentication(userId: user.id, completion: completion)
                    //self.service.initiateAuthentication(email: text, completion: completion)
               } else {
                    dialog.updateDialog(title:"Hoppsan", highlightedText:"Du verkar inte vara registrerad hos oss.", defaultText: "Prata med din arbetsgivare för mer information. Kontrollera annars att din  e-postadress är korrekt ifylld.", lightMode: false, dialogType: .error)
               }
          }
     }
     
     private func showErrorDialog() {
          _ = DialogController.display(from: self, title:"Hoppsan", highlightedText:"Något gick fel, vänligen försök igen", defaultText: nil, lightMode: false, dialogType: .error)
     }
     
     private func enableSuccessMode() {
          isEmailVerified = true
          headerText.text = "email_success_header".localize()
          infoText.text = "email_success_info".localize()
          actionButton.setTitle("email_success_action_button_text".localize(), for: .normal)
          extraInfoText.isHidden = true
          headerText.fadeTransition(0.3)
          infoText.fadeTransition(0.3)
          
          UIView.animate(withDuration: 0.3) {
               self.emailTextContainer.isHidden = true
               self.view.layoutIfNeeded()
          }
          self.textField.resignFirstResponder()
          
     }
     
     @IBAction func actionButton(_ sender: Any) {
        if isEmailVerified {
            let mailURL = URL(string: "message://")!
            if UIApplication.shared.canOpenURL(mailURL) {
                 UIApplication.shared.open(mailURL, options: [:], completionHandler: nil)
            }
        } else {
          validateEmail()
        }
          
     }
     
     @IBAction func onActionBack(_ sender: Any) {
          navigationController?.popViewController(animated: true)
     }
     
     func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
          if let text = textField.text,
               let textRange = Range(range, in: text) {
               let updatedText = text.replacingCharacters(in: textRange,
                                                          with: string)
               localEmailValidation(newText: updatedText)
               mailIcon.tintColor = updatedText.isEmpty ? UIColor(hexString: "9B9B9B") : UIColor.black
               
          }
          return true
     }
     
     func textFieldShouldReturn(_ textField: UITextField) -> Bool {
          guard let text = textField.text, text.count > 3 else {
               return false
          }
          validateEmail()
          return true
     }
     
}

