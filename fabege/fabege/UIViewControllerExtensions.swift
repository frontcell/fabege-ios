//
//  UIViewControllerExtensions.swift
//  fabege
//
//  Created by Oscar Kockum on 2018-12-17.
//  Copyright © 2018 Frontcell AB. All rights reserved.
//

import UIKit

extension UIViewController {
     
     func registerForKeyboardDidShowNotification(usingBlock block: ((Notification, CGSize) -> Void)? = nil) {
          NotificationCenter.default.addObserver(forName: UIResponder.keyboardDidShowNotification, object: nil, queue: nil, using: { (notification) -> Void in
               if let keyboardSize = notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? CGRect {
                    block?(notification, keyboardSize.size)
               }
          })
     }
     
     func registerForKeyboardWillShowNotification(usingBlock block: ((Notification, CGSize) -> Void)? = nil) {
          NotificationCenter.default.addObserver(forName: UIResponder.keyboardWillShowNotification, object: nil, queue: nil, using: { (notification) -> Void in
               if let keyboardSize = notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? CGRect {
                    if let userInfo = notification.userInfo {
                         let duration:TimeInterval = (userInfo[UIResponder.keyboardAnimationDurationUserInfoKey] as? NSNumber)?.doubleValue ?? 0
                         let animationCurveRawNSN = userInfo[UIResponder.keyboardAnimationCurveUserInfoKey] as? NSNumber
                         let animationCurveRaw = animationCurveRawNSN?.uintValue ?? UIView.AnimationOptions.curveEaseInOut.rawValue
                         let animationCurve:UIView.AnimationOptions = UIView.AnimationOptions(rawValue: animationCurveRaw)
                         
                         UIView.animate(withDuration: duration,
                                        delay: TimeInterval(0),
                                        options: animationCurve,
                                        animations: {
                                             block?(notification, keyboardSize.size)
                                             
                         },
                                        completion: nil)
                    }
                    
                    
               }
          })
     }
     
     func registerForKeyboardWillHideNotification(usingBlock block: ((Notification) -> Void)? = nil) {
          NotificationCenter.default.addObserver(forName: UIResponder.keyboardWillHideNotification, object: nil, queue: nil, using: { (notification) -> Void in
               block?(notification)
          })
     }
     
}

