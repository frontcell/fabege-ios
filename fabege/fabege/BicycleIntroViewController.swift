//
//  BicycleIntroViewController.swift
//  fabege
//
//  Created by Oscar Kockum on 2020-01-22.
//  Copyright © 2020 Frontcell AB. All rights reserved.
//

import UIKit
import Kingfisher

class BicycleIntroViewController: UIViewController, SearchVCDelegate {
   

    @IBOutlet weak var introImageView: UIImageView!
    @IBOutlet weak var introTextView: UILabel!
    @IBOutlet weak var continueButton: UIButton!
    
    var service : WAWServices.BicycleService!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        introTextView.text = service.intro.info.insertNewLines().replacingOccurrences(of: " kr", with: "\u{00a0}kr")
        continueButton.setTitle(service.intro.buttonTitle, for: .normal)
        if let url = URL(string:service.intro.imageUrl) {
            introImageView.kf.setImage(
                with: url,
                placeholder: UIImage(named: "bicycle_intro"),
                options: nil)
            {
                result in
                print(result)
            }
        }
        continueButton.outerShadow(show: true, animated: false)
        
        // Do any additional setup after loading the view.
    }
    
    @IBAction func actionContinue(_ sender: Any) {
        guard !model.hasProperties() else {
            continueAfterPropertiesLoad()
            return
        }
        let dialog = DialogController.display(from: self, title: nil, highlightedText: "Hämtar fastigheter", defaultText: nil, lightMode: true, dialogType: .loading)
        WebserviceInstance.loadStructures({ (response) -> Void in
            dialog.dismissDialog(animated: true, completion: {
                model.setProperties(response)
                self.continueAfterPropertiesLoad()
            })
        }, failure: { (error) -> Void in
            dialog.dismissDialog(completion: nil)
            let alertView = UIAlertController(title: "Något gick fel", message: "Försök igen", preferredStyle: .alert)
            alertView.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alertView, animated: true, completion: nil)
        })
    }
    
    private func continueAfterPropertiesLoad() {
        if let favorite = model.getFavorites().first {
            //model.setBicycleServiceFavorites(property: favorite)
            performSegue(withIdentifier: "showBicycleListView", sender: favorite)
            /*let dc = DialogController.display(from: self, title: "Hej!", highlightedText: "Vi ser att du har valt vår fastighet \(favorite.name) på \(favorite.address). Ligger ditt kontor där?", defaultText: nil, lightMode: true, dialogType: .yesNo)
            dc.onYesActionClicked = {
                dc.dismissDialog {
                    model.setBicycleServiceFavorites(property: favorite)
                    self.performSegue(withIdentifier: "showBicycleListView", sender: nil)
                }
            }
            dc.onNoActionClicked = {
                dc.dismissDialog {
                    self.performSegue(withIdentifier: "ShowPropertySelection", sender: nil)
                }
            }*/
        } else {
            let dc = DialogController.display(from: self, title: "Hoppsan!", highlightedText: "Vi ser att du inte har valt någon fastighet än. Klicka på länken så fixar vi det först.", defaultText: nil, lightMode: true, dialogType: .customButton("Välj din fastighet"))
            dc.onCustomButtonClicked = {
                dc.dismissDialog {
                    self.performSegue(withIdentifier: "ShowPropertySelection", sender: nil)
                }
            }
            
        }
    }
    @IBAction func actionBack(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
        if segue.identifier == "ShowPropertySelection" {
            let navigationVC = segue.destination as! UINavigationController;
            let searchVC = navigationVC.visibleViewController as! SearchVC;
            searchVC.changeProperty = false
            if let favorite = model.bicycleServiceProperty() {
                searchVC.selectedProperty = favorite
            } else {
                searchVC.selectedProperty = model.getFavorites().first
            }
            searchVC.delegate = self
        } else if segue.identifier == "showBicycleListView" {
            let vc = segue.destination as! BicycleServiceVC
            vc.selectedProperty = sender as? Property
            vc.service = service
            vc.idxOfViewControllerToRemove = navigationController?.viewControllers.index(of: self)
        }
    }
    
    func didSelectProperty(_ property: Property) {

    }
       
    func didCloseView() {
        if let favorite = model.getFavorites().first {
            performSegue(withIdentifier: "showBicycleListView", sender: favorite)
        }
        
    }
       
    

}
