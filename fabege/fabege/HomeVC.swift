//
//  HomeVC.swift
//  fabege
//
//  Created by Patrik Billgert on 18/03/15.
//  Copyright (c) 2015 Frontcell AB. All rights reserved.
//

import UIKit
import WebKit

fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
     switch (lhs, rhs) {
     case let (l?, r?):
          return l < r
     case (nil, _?):
          return true
     default:
          return false
     }
}

fileprivate func > <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
     switch (lhs, rhs) {
     case let (l?, r?):
          return l > r
     default:
          return rhs < lhs
     }
}


class HomeVC: UIViewController, UIScrollViewDelegate, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, WAWInitiator {
     
     @IBOutlet weak var previousButton: UIButton!
     @IBOutlet weak var nextButton: UIButton!
     @IBOutlet weak var collectionView: UICollectionView!
     @IBOutlet weak var pageControl: UIPageControl!
     @IBOutlet weak var notificationsLabel: UILabel!
     @IBOutlet weak var notificationsContainerView: UIView!
     @IBOutlet var mainFeatureButtons: [UIButton]!
     
     var favorites: [Property] = []
     var propertyId2Tasks : [Int : [Task]] = [:]
     var propertyId2WebView : [Int : WKWebView] = [:]
     var didLayoutSubviews: Bool = false
     
     @IBOutlet weak var gridVertical: UIStackView!
     @IBOutlet var gridTopSpacing: NSLayoutConstraint!
     @IBOutlet weak var gridHorizontalOne: UIStackView!
     @IBOutlet weak var gridHorizontalTwo: UIStackView!

     
     
     // MARK: - Lifecycle
     
     override func viewDidLoad() {
          super.viewDidLoad()
          
          NotificationCenter.default.addObserver(self, selector: #selector(HomeVC.didEnterBackground), name: UIApplication.didEnterBackgroundNotification, object: nil)
          NotificationCenter.default.addObserver(self, selector: #selector(HomeVC.didBecomeActive), name: UIApplication.didBecomeActiveNotification, object: nil)
          
          let smallImages = [UIImage(named: "write_icon_small"),
                             UIImage(named: "cone_icon_small"),
                             UIImage(named: "house_icon_small"),
                             UIImage(named: "help_icon_small")]
          
          for (idx, button) in mainFeatureButtons.enumerated() {
               if Device.IS_IPHONE_5_OR_LESS {
                    button.setImage(smallImages[idx], for: .normal)
               }
               
               button.titleLabel?.numberOfLines = 2
               button.titleLabel?.textAlignment = .center
               button.alignTextBelow(spacing: 10)
          }
          
          if Device.IS_IPHONE_5_OR_LESS {
               gridTopSpacing.constant = 10
               gridHorizontalOne.spacing = 10
               gridHorizontalTwo.spacing = 10
               
          }
          
          notificationsContainerView.isHidden = true
          notificationsContainerView.outerShadow(show: true, height: 5, animated: false)
     }
     
     @objc func didEnterBackground() {
          propertyId2WebView.removeAll()
     }
     
     @objc func didBecomeActive() {
          updatePreloadedWebViews()
          
     }
     
     func updatePreloadedWebViews() {
          if favorites.count > 0 {
               for favorite in self.favorites {
                    if self.propertyId2WebView[favorite.propertyId] == nil {
                         let webView = WKWebView(frame: CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: self.view.frame.size.height))
                         let url = URLUtils.urlForProperty(favorite)
                         webView.load(URLRequest(url: url))
                         self.propertyId2WebView[favorite.propertyId] = webView
                    }
               }
          }
     }
     
     override var preferredStatusBarStyle: UIStatusBarStyle {
          return .default
     }
     
     override var prefersStatusBarHidden: Bool {
          return false
     }
     
     override func viewWillAppear(_ animated: Bool) {
          navigationController?.setNavigationBarHidden(true, animated: true)
          setNeedsStatusBarAppearanceUpdate()
          
          favorites = model.getFavorites()
          
          collectionView.reloadData()
          
          if favorites.count <= 1 {
               previousButton.isHidden = true
               nextButton.isHidden = true
          } else {
               previousButton.isHidden = true
               nextButton.isHidden = true
          }
          
          if didLayoutSubviews == true {
               selectCurrentProperty()
          }
          
          updatePreloadedWebViews()
          
          
          var ids = [Int]()
          for favorite in favorites {
               ids.append(favorite.propertyId)
          }
          
          WebserviceInstance.getDetails(ids, completion: { (response) -> Void in
               self.propertyId2Tasks = response
               self.updateCurrentPage()
          }) { (error) -> Void in
               
          }
     }
     
     override func viewDidLayoutSubviews() {
          super.viewDidLayoutSubviews()
          
          selectCurrentProperty()
          didLayoutSubviews = true
     }
     
     override func didReceiveMemoryWarning() {
          super.didReceiveMemoryWarning()
          // Dispose of any resources that can be recreated.
     }
     
     
     
     // MARK: - Navigation
     
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
          if segue.identifier == "ShowFAQ" {
               let favorite = favorites[pageControl.currentPage]
               let faqVC = segue.destination as! FaqVC;
               faqVC.selectedProperty = favorite
          } else if segue.identifier == "ShowWeb" {
               let webVC = segue.destination as! WebVC;
               if let url = sender as? URL {
                    webVC.url = url
               } else {
                    let favorite = favorites[pageControl.currentPage]
                    webVC.infoMode = true
                    webVC.selectedProperty = favorite
                    if let webView = propertyId2WebView[favorite.propertyId] {
                         webVC.webView = webView
                    }
                    FabegeAnalytics.trackEvent("Page_View", parameters: ["PropertyName" : favorite.name,
                                                                         "PropertyAdress" : favorite.address,
                                                                         "Page" : "Read more"])
               }
               
               
               
               
          } else if segue.identifier == "ShowRegister" {
               let favorite = favorites[pageControl.currentPage]
               let registerVC = segue.destination as! RegisterVC;
               registerVC.selectedProperty = favorite
               
               FabegeAnalytics.trackEvent("Page_View", parameters: ["PropertyName" : favorite.name,
                                                                    "PropertyAdress" : favorite.address,
                                                                    "Page" : "Service Request"])
               
               
          } else if segue.identifier == "ShowMatters" {
               let favorite = favorites[pageControl.currentPage]
               let tasksVC = segue.destination as! TasksVC;
               tasksVC.tasks = propertyId2Tasks[favorite.propertyId]!
               FabegeAnalytics.trackEvent("Page View", parameters: ["PropertyName" : favorite.name,
                                                                    "PropertyAdress" : favorite.address,
                                                                    "Page" : "Ongoing Work"])
               
          } else if segue.identifier == "PresentSearch" {
               FabegeAnalytics.trackEvent("Page_View", parameters: ["Page" : "Search"])
               
          }
     }
     
     
     
     // MARK: - UICollectionView DataSource
     
     func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
          return favorites.count
     }
     
     func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
          let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "FAVORITE_CELL", for: indexPath) as! FavoriteCell
          
          let property = favorites[(indexPath as NSIndexPath).row]
          
          cell.addressLabel.text = property.address
          cell.nameLabel.text = property.name
          
          return cell
     }
     
     
     
     // MARK: - UICollectionViewDelegate FlowLayout
     
     func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
          return CGSize(width: collectionView.bounds.size.width, height: collectionView.bounds.size.height)
     }
     
     
     
     // MARK: - UIScrollViewDelegate
     
     func scrollViewDidEndScrollingAnimation(_ scrollView: UIScrollView) {
          updateCurrentPage()
     }
     
     func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
          updateCurrentPage()
     }
     
     
     // MARK: - IBActions
     @IBAction func wawAction(_ sender: Any) {
          initiateWAW()
     }
     
     @IBAction func goBack(_ sender: Any) {
          self.navigationController?.popViewController(animated: true)
     }
     
     @IBAction func previous(_ sender: AnyObject) {
          let indexPath = IndexPath(item: pageControl.currentPage - 1, section: 0)
          if (indexPath as NSIndexPath).item != -1 {
               collectionView.scrollToItem(at: indexPath, at: UICollectionView.ScrollPosition(), animated: true)
               pageControl.currentPage = (indexPath as NSIndexPath).item
          }
     }
     
     @IBAction func next(_ sender: AnyObject) {
          let indexPath = IndexPath(item: pageControl.currentPage + 1, section: 0)
          if (indexPath as NSIndexPath).item != favorites.count {
               collectionView.scrollToItem(at: indexPath, at: UICollectionView.ScrollPosition(), animated: true)
               pageControl.currentPage = (indexPath as NSIndexPath).item
          }
     }
     
     @IBAction func register(_ sender: AnyObject) {
          if !collectionView.isDecelerating {
               performSegue(withIdentifier: "ShowRegister", sender: nil)
          }
     }
     
     @IBAction func matters(_ sender: AnyObject) {
          let favorite = favorites[pageControl.currentPage]
          let tasks = propertyId2Tasks[favorite.propertyId]
          if tasks?.count > 0 {
               if !collectionView.isDecelerating {
                    let favorite = favorites[pageControl.currentPage]
                    if let tasks = propertyId2Tasks[favorite.propertyId] {
                         if (tasks.count > 0) {
                              performSegue(withIdentifier: "ShowMatters", sender: nil)
                         }
                    }
               }
          } else {
               let alertView = UIAlertView(title: "Det finns inga pågående arbeten", message: nil, delegate: self, cancelButtonTitle: "OK")
               alertView.show()
          }
     }
     
     @IBAction func information(_ sender: AnyObject) {
          if !collectionView.isDecelerating {
               performSegue(withIdentifier: "ShowWeb", sender: nil)
          }
     }
     
     @IBAction func questions(_ sender: AnyObject) {
          if !collectionView.isDecelerating {
               //performSegue(withIdentifier: "ShowFAQ", sender: nil)
               performSegue(withIdentifier: "ShowWeb", sender: URL(string: "https://www.fabege.se/vara-kunder/vanliga-fragor/"))
          }
     }
     
     // MARK: - Other
     
     func selectCurrentProperty() {
          let index = UserDefaults.standard.integer(forKey: "propertyIndex")
          if index < collectionView.numberOfItems(inSection: 0) {
               let selectedIndexPath = IndexPath(item: index, section: 0)
               collectionView.scrollToItem(at: selectedIndexPath, at: UICollectionView.ScrollPosition(), animated: false)
               
               pageControl.numberOfPages = favorites.count
               pageControl.currentPage = index
          }
     }
     
     func updateCurrentPage() {
          let pageWidth = collectionView.frame.size.width
          let page: Int = Int(collectionView.contentOffset.x / pageWidth)
          pageControl.currentPage = page
          
          if pageControl.currentPage < favorites.count {
               let favorite = favorites[pageControl.currentPage]
               if let tasks = propertyId2Tasks[favorite.propertyId] {
                    notificationsLabel.text = "\(tasks.count)"
                    notificationsContainerView.isHidden = tasks.isEmpty ? true : false
               } else {
                    notificationsLabel.text = "0"
                    notificationsContainerView.isHidden = true
               }
          }
          UIView.animate(withDuration: 0.15, animations: { () -> Void in
               self.previousButton.alpha = page > 0 ? 1 : 0
               self.nextButton.alpha = (page < self.favorites.count - 1) ? 1 : 0
          }) 
          
          UserDefaults.standard.set(page, forKey: "propertyIndex")
          UserDefaults.standard.synchronize()
     }
}

