//
//  WAWRouter.swift
//  fabege
//
//  Created by Oscar Kockum on 2018-12-13.
//  Copyright © 2018 Frontcell AB. All rights reserved.
//

import UIKit
import StoreKit


class WAWRouter {
     
     var navigationController : UINavigationController
     var service = WAWService()
     
     init(navigationController : UINavigationController) {
          self.navigationController = navigationController
          
     }
   
     func initialize() {
          let status = service.status()
          switch status {
          case .unknown:
               FabegeAnalytics.trackEvent("WAW_Register_start", parameters:nil)
               var info = WAWInfoController.Info()
               info.actionButton = "tutorial_action_first".localize()
               info.header = "tutorial_header_first".localize()
               info.info = "tutorial_info_first".localize()
               info.info = NSLocalizedString("tutorial_info_first", comment: "")
               info.onActionClicked = {
                    self.showEmailView()
               }
               let vc = WAWInfoController.setup(with: info)
               navigationController.pushViewController(vc, animated: true)
          case .onboarding:
               FabegeAnalytics.trackEvent("WAW_Register_success", parameters:nil)
               showOnboarding()
          case .parakey:
               showParakeyScreen()
          case .ready:
               showPropertyListView()
          }
          
     }
     
     private func showEmailView() {
          let vc = UIStoryboard(name: "WAW", bundle: nil).instantiateViewController(withIdentifier: "WAWEmailCheckController") as! WAWEmailCheckController
          navigationController.pushViewController(vc, animated: true)
     }
     
     private func showPropertyListView() {
          let vc = UIStoryboard(name: "WAW", bundle: nil).instantiateViewController(withIdentifier: "WAWListPropertiesVC") as! WAWListPropertiesVC
          navigationController.pushViewController(vc, animated: true)
     }
     
     private func displayAuthenticationError() {
          _ = DialogController.display(from: self.navigationController, title: "Problem", highlightedText: "Problem med autentiseringen", defaultText: "Vänligen försök igen!")
     }
     
     private func showOnboarding() {
          self.navigationController.popToRootViewController(animated: false)
          let vc = UIStoryboard(name: "WAW", bundle: nil).instantiateViewController(withIdentifier: "WAWOnboardingContainerVC") as! WAWOnboardingContainerVC
          self.navigationController.pushViewController(vc, animated: false)
     }
     
     
     fileprivate func showParakeyScreen() {
          let vc = UIStoryboard(name: "WAW", bundle: nil).instantiateViewController(withIdentifier: "WAWInstallParakeyVC") as! WAWInstallParakeyVC
          vc.onFinished =  {
               self.service.setParakeyConnected()
               self.showOnboarding()
          }
          self.navigationController.popToRootViewController(animated: false)
          self.navigationController.pushViewController(vc, animated: false)
     }
     
     func handleAuthLink(token : String) -> Bool {
          let status = service.status()
          
          guard status == .unknown else {
               initialize()
               return true
          }
          
          service.continueSignInWithToken(token) { (email, error) in
               guard error == nil else {
                    self.displayAuthenticationError()
                    return
               }
               self.initialize()
          }
          return true
     }
     
     func handleDynamicLink(link : String) -> Bool {
          let status = service.status()
          
          guard status == .unknown else {
               initialize()
               return true
          }
         
          service.continueSignIn(link: link) { (email, error) in
               guard error == nil  else {
                    self.displayAuthenticationError()
                    return
               }
               self.initialize()
          }
          return true
     }
     
     
     
     
     
     
}
