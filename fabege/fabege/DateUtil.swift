//
//  DateUtil.swift
//  fabege
//
//  Created by Oscar Kockum on 2019-01-10.
//  Copyright © 2019 Frontcell AB. All rights reserved.
//

import Foundation

class DateUtil {
     
     static let dateKeyFormatter : DateFormatter = {
          let df = DateFormatter()
          df.dateFormat = "yyyyMMdd"
          df.locale = Locale(identifier: "sv_SE")
          return df
     }()
     
     static let dateKeyTimeFormatter : DateFormatter = {
          let df = DateFormatter()
          df.dateFormat = "yyyyMMdd HH:mm"
          df.locale = Locale(identifier: "sv_SE")
          return df
     }()
     
     static let dateTimeTimeFormatter : DateFormatter = {
          let df = DateFormatter()
          df.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
          df.locale = Locale(identifier: "sv_SE")
          return df
     }()
     
     class func timestamp(time : String, dateKey : String) -> TimeInterval? {
          let dateString = "\(dateKey) \(time)"
          if let date = dateKeyTimeFormatter.date(from: dateString) {
               return date.timeIntervalSince1970
          }
          return nil
     }
    
    static let dateMonthFormatter : DateFormatter = {
          let df = DateFormatter()
                   df.dateFormat = "d MMM"
                   df.locale = Locale(identifier: "sv_SE")
                   return df
    }()
     
}

extension Date {
     
     func dateKey() -> String {
          return DateUtil.dateKeyFormatter.string(from: self)
     }
     
     func dateKeyWithTime() -> String {
          return DateUtil.dateKeyTimeFormatter.string(from: self)
     }
    
    func endOfDay() -> Date? {
        var components = Calendar.current.dateComponents([.year, .month, .day, .hour, .minute, .second], from: self)
        components.hour = 23
        components.minute = 59
        components.second = 59
        let retVal = Calendar.current.date(from: components)
        return retVal
    }
     
}

