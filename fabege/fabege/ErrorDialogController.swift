//
//  ErrorDialogController.swift
//  fabege
//
//  Created by Oscar Kockum on 2018-12-18.
//  Copyright © 2018 Frontcell AB. All rights reserved.
//

import UIKit

class DialogController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
     
     class func display(from vc : UIViewController) {
          let dialog = DialogController(nibName: "ErrorDialogView", bundle: nil)
          dialog.providesPresentationContextTransitionStyle = true
          dialog.definesPresentationContext = true
          dialog.modalPresentationStyle = .overCurrentContext
          dialog.modalTransitionStyle = .crossDissolve
          //customAlert.delegate = self
          vc.present(dialog, animated: true, completion: nil)
     }
     
    

     @IBAction func actionClose(_ sender: Any) {
          dismiss(animated: true, completion: nil)
     }
     /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
