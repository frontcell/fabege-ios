//
//  String+Localization.swift
//  fabege
//
//  Created by Oscar Kockum on 2019-02-13.
//  Copyright © 2019 Frontcell AB. All rights reserved.
//

import Foundation

extension String {
     func localize() -> String {
          return NSLocalizedString(self, comment: "")
     }
}
