//
//  UIKitExtensions.swift
//  fabege
//
//  Created by Oscar Kockum on 2018-12-17.
//  Copyright © 2018 Frontcell AB. All rights reserved.
//

import UIKit


extension UIImage {
    func resized(withPercentage percentage: CGFloat) -> UIImage? {
        let canvas = CGSize(width: size.width * percentage, height: size.height * percentage)
        return UIGraphicsImageRenderer(size: canvas, format: imageRendererFormat).image {
            _ in draw(in: CGRect(origin: .zero, size: canvas))
        }
    }
    func resized(toWidth width: CGFloat) -> UIImage? {
        let canvas = CGSize(width: width, height: CGFloat(ceil(width/size.width * size.height)))
        return UIGraphicsImageRenderer(size: canvas, format: imageRendererFormat).image {
            _ in draw(in: CGRect(origin: .zero, size: canvas))
        }
    }
}

extension UIButton {
     
     func alignTextBelow(spacing: CGFloat = 6.0) {
          if let image = self.imageView?.image {
               let imageSize: CGSize = image.size
               self.titleEdgeInsets = UIEdgeInsets(top: spacing, left: -imageSize.width, bottom: -(imageSize.height), right: 0.0)
               let labelString = NSString(string: self.titleLabel!.text!)
               let titleSize = labelString.size(withAttributes: [NSAttributedString.Key.font: self.titleLabel!.font])
               self.imageEdgeInsets = UIEdgeInsets(top: -(titleSize.height + spacing), left: 0.0, bottom: 0.0, right: -titleSize.width)
          }
     }
     
     func underline(color : UIColor? = nil) {
          if let textString = self.titleLabel?.text {
               let attributedString = NSMutableAttributedString(string: "\(textString) " )
               attributedString.addAttribute(.underlineStyle, value: NSUnderlineStyle.single.rawValue, range: NSRange(location: 0, length: attributedString.length - 1))
               if let color = color {
                    attributedString.addAttribute(.underlineColor, value: color, range: NSRange(location: 0, length: attributedString.length - 1))
               }
               attributedString.addAttribute(.font, value: titleLabel!.font, range: NSRange(location: 0, length: attributedString.length - 1))
               setAttributedTitle(attributedString, for: .normal)
          }
     }
}

extension UINavigationController {
     open override var preferredStatusBarStyle: UIStatusBarStyle {
          return topViewController?.preferredStatusBarStyle ?? .default
     }
}

class ShadowContainerView: UIView {
     
     var horizontal = false
     
     override public class var layerClass: Swift.AnyClass {
          return CAGradientLayer.self
     }
     
     init(horizontal : Bool) {
          self.horizontal = horizontal
          super.init(frame : CGRect.zero)
          setupView()
     }
     
     override init(frame: CGRect) {
          super.init(frame:frame)
          setupView()
     }
     
     required init?(coder aDecoder: NSCoder) {
          super.init(coder: aDecoder)
          setupView()
     }
     
     private func setupView() {
          guard let gradientLayer = self.layer as? CAGradientLayer else { return }
          
          gradientLayer.colors = [UIColor.black.withAlphaComponent(0.5).cgColor,
                                  UIColor.black.withAlphaComponent(0.0).cgColor]
          gradientLayer.locations = [0.0,1.0]
          if horizontal {
               gradientLayer.startPoint = CGPoint(x: 0.0, y: 0.0)
               gradientLayer.endPoint = CGPoint(x: 1.0, y: 0.0)
          }
     }

}

class InnerShadowView : UIView {
     
     var width : CGFloat = 5
     
     override public class var layerClass: Swift.AnyClass {
          return CAShapeLayer.self
     }
     
     override init(frame: CGRect) {
          super.init(frame:frame)
          setupView()
     }
     
     required init?(coder aDecoder: NSCoder) {
          super.init(coder: aDecoder)
          setupView()
     }
     
     override func layoutSubviews() {
          guard let shapeLayer = self.layer as? CAShapeLayer else { return }
          
          let largerRect = CGRect(x:bounds.origin.x - width,
                                  y: bounds.origin.y - width,
                                  width: bounds.size.width + width,
                                  height: bounds.size.height + width);
          
          // Create the larger rectangle path.
          shapeLayer.shadowRadius = width
          let path = CGMutablePath()
          path.addRect(largerRect)
          let bezier = UIBezierPath(roundedRect: bounds, cornerRadius: cornerRadius)
          path.addPath(bezier.cgPath)
          path.closeSubpath()
          shapeLayer.path = path
     }
     
     private func setupView() {
          clipsToBounds = true
          
          guard let shapeLayer = self.layer as? CAShapeLayer else { return }
          shapeLayer.fillRule = .evenOdd
          shapeLayer.shadowOpacity = 1
          shapeLayer.shouldRasterize = true
          shapeLayer.shadowColor = UIColor.black.withAlphaComponent(1).cgColor
     }
}

extension CALayer {
     func applySketchShadow(
          color: UIColor = .black,
          alpha: Float = 0.5,
          x: CGFloat = 0,
          y: CGFloat = 2,
          blur: CGFloat = 4,
          spread: CGFloat = 0)
     {
          shadowColor = color.cgColor
          shadowOpacity = alpha
          shadowOffset = CGSize(width: x, height: y)
          shadowRadius = blur / 2.0
          if spread == 0 {
               shadowPath = nil
          } else {
               let dx = -spread
               let rect = bounds.insetBy(dx: dx, dy: dx)
               shadowPath = UIBezierPath(rect: rect).cgPath
          }
     }
}

extension UIView {
     
     func addSubview(view : UIView, pinToEdges : Bool) {
          addSubview(view)
          if pinToEdges {
               view.translatesAutoresizingMaskIntoConstraints = false
               NSLayoutConstraint.activate([view.topAnchor.constraint(equalTo: topAnchor),
                                            view.leftAnchor.constraint(equalTo: leftAnchor),
                                            view.rightAnchor.constraint(equalTo: rightAnchor),
                                            view.bottomAnchor.constraint(equalTo: bottomAnchor)])
          }
    
     }
     
     @IBInspectable var cornerRadius: CGFloat {
          get {
               return layer.cornerRadius
          }
          set {
               layer.cornerRadius = newValue
          }
     }
     
     @IBInspectable var enableOuterShadow: Bool {
          get {
               return true
          }
          set {
               outerShadow(show: true, animated: false)
          }
     }
     
     func outerShadow(show : Bool, color : UIColor = UIColor(hexString: "6A6A6A"), alpha : Float = 0.5, height: CGFloat = 10, blur: CGFloat = 15, animated : Bool) {
          if show && layer.shadowOffset.height != height {
               layer.applySketchShadow(color: color, alpha: alpha, x: 0, y: height, blur: blur, spread: 0)
               if animated {
                    CATransaction.begin()
                    let opacityAnimation = CABasicAnimation(keyPath: "shadowOpacity")
                    opacityAnimation.fromValue = 0.0
                    opacityAnimation.toValue = 0.5
                    opacityAnimation.duration = 0.2
                    opacityAnimation.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeOut)
                    opacityAnimation.fillMode = CAMediaTimingFillMode.both
                    layer.add(opacityAnimation, forKey: opacityAnimation.keyPath)
                    
                    let offsetAnimation = CABasicAnimation(keyPath: "shadowOffset")
                    offsetAnimation.fromValue = CGSize(width: 0, height: 0)
                    offsetAnimation.toValue = CGSize(width: 0, height: height)
                    offsetAnimation.duration = 0.2
                    offsetAnimation.timingFunction = opacityAnimation.timingFunction
                    offsetAnimation.fillMode = opacityAnimation.fillMode
                    layer.add(offsetAnimation, forKey: offsetAnimation.keyPath)
                    CATransaction.commit()
               }
               clipsToBounds = false
          } else if !show {
               layer.shadowOffset = CGSize(width: 0, height: 0)
               layer.shadowOpacity = 0.0
               layer.shadowColor = UIColor.clear.cgColor
          }
     }
     
     func fadeTransition(_ duration:CFTimeInterval) {
          let animation = CATransition()
          animation.timingFunction = CAMediaTimingFunction(name:
               CAMediaTimingFunctionName.easeInEaseOut)
          animation.type = CATransitionType.fade
          animation.duration = duration
          layer.add(animation, forKey: CATransitionType.fade.rawValue)
     }
     
     public func addInnerShadow(width : CGFloat) {
          let shadow = InnerShadowView()
          shadow.width = width
          shadow.backgroundColor = UIColor.clear
          shadow.translatesAutoresizingMaskIntoConstraints = false
          addSubview(shadow)
          NSLayoutConstraint.activate([shadow.leftAnchor.constraint(equalTo: leftAnchor),shadow.rightAnchor.constraint(equalTo: rightAnchor), shadow.topAnchor.constraint(equalTo: topAnchor), shadow.bottomAnchor.constraint(equalTo: bottomAnchor)])
     }
     
     
     public func addInnerShadow(height : CGFloat = 2) {
          let topShadow = ShadowContainerView()
          topShadow.backgroundColor = UIColor.clear
          topShadow.translatesAutoresizingMaskIntoConstraints = false
          addSubview(topShadow)
          NSLayoutConstraint.activate([topShadow.heightAnchor.constraint(equalToConstant: height), topShadow.leftAnchor.constraint(equalTo: leftAnchor),topShadow.rightAnchor.constraint(equalTo: rightAnchor), topShadow.topAnchor.constraint(equalTo: topAnchor)])
          
          let leftShadow = ShadowContainerView(horizontal: true)
          leftShadow.backgroundColor = UIColor.clear
          leftShadow.translatesAutoresizingMaskIntoConstraints = false
          addSubview(leftShadow)
          NSLayoutConstraint.activate([leftShadow.widthAnchor.constraint(equalToConstant: height), leftShadow.topAnchor.constraint(equalTo: topAnchor, constant:height / 2),leftShadow.bottomAnchor.constraint(equalTo: bottomAnchor), leftShadow.leftAnchor.constraint(equalTo: leftAnchor)])
          
     }
}

extension UIColor {
     func hexString() -> String {
          var r:CGFloat = 0
          var g:CGFloat = 0
          var b:CGFloat = 0
          var a:CGFloat = 0
          
          getRed(&r, green: &g, blue: &b, alpha: &a)
          
          let rgb:Int = (Int)(r*255)<<16 | (Int)(g*255)<<8 | (Int)(b*255)<<0
          
          return NSString(format:"#%06x", rgb) as String
     }
     convenience init(hexString: String) {
          let hex = hexString.trimmingCharacters(in: CharacterSet.alphanumerics.inverted)
          var int = UInt32()
          Scanner(string: hex).scanHexInt32(&int)
          let a, r, g, b: UInt32
          switch hex.count {
          case 3: // RGB (12-bit)
               (a, r, g, b) = (255, (int >> 8) * 17, (int >> 4 & 0xF) * 17, (int & 0xF) * 17)
          case 6: // RGB (24-bit)
               (a, r, g, b) = (255, int >> 16, int >> 8 & 0xFF, int & 0xFF)
          case 8: // ARGB (32-bit)
               (a, r, g, b) = (int >> 24, int >> 16 & 0xFF, int >> 8 & 0xFF, int & 0xFF)
          default:
               (a, r, g, b) = (255, 0, 0, 0)
          }
          self.init(red: CGFloat(r) / 255, green: CGFloat(g) / 255, blue: CGFloat(b) / 255, alpha: CGFloat(a) / 255)
     }
}

extension NSLayoutConstraint {
     
     @IBInspectable var exactPixels: Int {
          get {
               return Int(constant * UIScreen.main.scale)
          }
          set {
               constant = CGFloat(newValue) / UIScreen.main.scale
          }
     }
}


extension UILabel {
     
     func underline(color : UIColor? = nil) {
          if let textString = self.text {
               let attributedString = NSMutableAttributedString(string: "\(textString) " )
               attributedString.addAttribute(.underlineStyle, value: NSUnderlineStyle.single.rawValue, range: NSRange(location: 0, length: attributedString.length - 1))
               if let color = color {
                    attributedString.addAttribute(.underlineColor, value: color, range: NSRange(location: 0, length: attributedString.length - 1))
               }
               attributedString.addAttribute(.font, value: font, range: NSRange(location: 0, length: attributedString.length - 1))
               attributedText = attributedString
          }
     }
}
