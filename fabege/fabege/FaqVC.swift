//
//  QuestionsVC.swift
//  fabege
//
//  Created by Patrik Billgert on 08/04/15.
//  Copyright (c) 2015 Frontcell AB. All rights reserved.
//

import UIKit


class FaqVC: UIViewController {

    var selectedProperty: Property!
    
    
    
    // MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()

        navigationItem.title = "Vanliga frågor"
        
        let backButton: UIBarButtonItem = UIBarButtonItem(title:"", style:.plain, target:self, action:nil)
        navigationItem.backBarButtonItem = backButton
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
     override var preferredStatusBarStyle: UIStatusBarStyle {
          return .lightContent
     }
     override func viewWillAppear(_ animated: Bool) {
          setNeedsStatusBarAppearanceUpdate()
           navigationController?.setNavigationBarHidden(false, animated: true)
     }
    
    
    // MARK: - Navigation

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "ShowWeb" {
            let webVC = segue.destination as! WebVC;
            webVC.selectedProperty = selectedProperty
            
            let tag = sender as! Int
            if tag == 100 {
                webVC.navTitle = "Hissar"
                webVC.urlSuffix = "#elevators"
            } else if tag == 101 {
                webVC.navTitle = "Städning"
                webVC.urlSuffix = "#cleaning"
            } else if tag == 102 {
                webVC.navTitle = "Miljö och energi"
                webVC.urlSuffix = "#environmentandenergy"
            } else if tag == 103 {
                webVC.navTitle = "Säkerhet och inpassering"
                webVC.urlSuffix = "#security"
            } else if tag == 104 {
                webVC.navTitle = "Skyltning"
                webVC.urlSuffix = "#signs"
            } else {
                webVC.navTitle = "Parkering"
                webVC.urlSuffix = "#parking"
            }
            
          FabegeAnalytics.trackEvent("Page_View", parameters: ["PropertyName" : self.selectedProperty.name,
                                                               "PropertyAdress" : self.selectedProperty.address,
                                                               "Page" : "FAQ",
                                                               "FAQType" : webVC.urlSuffix.replacingOccurrences(of: "#", with: "")])

        }
    }
    

    
    // MARK: - IBActions
    
    @IBAction func elevators(_ sender: UIButton) {
        performSegue(withIdentifier: "ShowWeb", sender: sender.tag)
    }
    
    @IBAction func cleaning(_ sender: UIButton) {
        performSegue(withIdentifier: "ShowWeb", sender: sender.tag)
    }
    
    @IBAction func environment(_ sender: UIButton) {
        performSegue(withIdentifier: "ShowWeb", sender: sender.tag)
    }
    
    @IBAction func security(_ sender: UIButton) {
        performSegue(withIdentifier: "ShowWeb", sender: sender.tag)
    }
    
    @IBAction func boards(_ sender: UIButton) {
        performSegue(withIdentifier: "ShowWeb", sender: sender.tag)
    }
}
