//
//  WAWService.swift
//  fabege
//
//  Created by Oscar Kockum on 2018-12-11.
//  Copyright © 2018 Frontcell AB. All rights reserved.
//

import Foundation
import FirebaseFirestore
import Firebase
import FirebaseAuth
import Alamofire

class WAWService {
    
    static let WAWServiceVersion = 1
    
    class Subscription {
        private var listener : ListenerRegistration
        
        init(listener : ListenerRegistration) {
            self.listener = listener
        }
        func detach() {
            listener.remove()
        }
    }
    
    enum Status {
        case unknown
        case parakey
        case onboarding
        case ready
    }
    
    
    
    static let parakeyUrl = URL(string: "parakeyapp://")!
    
    static let InvalidId = "-1"
    
    var userId : String {
        guard let user = user else {
            return WAWService.InvalidId
        }
        return user.id
    }
    
    func updateDeviceToken(deviceToken : String) {
        guard let user = user, user.deviceToken != deviceToken else {
            return
        }
        let db = Firestore.firestore()
        db.runTransaction({ (transaction, error) -> Any? in
            let ref = db.collection("user").document(user.id)
            if let doc = try? transaction.getDocument(ref), let data = doc.data() {
                var user = WAWUser(id: doc.documentID, data: data)
                user.deviceToken = deviceToken
                transaction.updateData(user.data, forDocument: ref)
                self.user = user
            }
            return nil
        }
        ) { (object, error) in
            if let error = error {
                print("Transaction failed: \(error)")
            } else {
                print("Transaction successfully committed!")
            }
        }
    }
    
    func status() -> Status {
        guard isAuthenticatedWithFirebase() else {
            return .unknown
        }
        guard UserDefaults.standard.bool(forKey: Key.parakeyConnected.rawValue) else {
            return .parakey
        }
        guard UserDefaults.standard.bool(forKey: Key.onboardingShown.rawValue) else {
            return .onboarding
        }
        return .ready
    }
    
    func logout() {
        try? Auth.auth().signOut()
        UserDefaults.standard.removeObject(forKey: Key.onboardingShown.rawValue)
        UserDefaults.standard.removeObject(forKey: Key.parakeyConnected.rawValue)
        UserDefaults.standard.removeObject(forKey: Key.email.rawValue)
        UserDefaults.standard.removeObject(forKey: Key.user.rawValue)
    }
    
    func validateUser() {
        if status() == .ready {
            guard let user = user else {
                return
            }
            fetchUser(userId: user.id) { (updatedUser, error) in
                guard updatedUser != nil else {
                    // user is gone... log out
                    self.logout()
                    return
                }
                let db = Firestore.firestore()
                db.runTransaction({ (transaction, error) -> Any? in
                    let ref = db.collection("user").document(user.id)
                    if let doc = try? transaction.getDocument(ref), let data = doc.data() {
                        var user = WAWUser(id: doc.documentID, data: data)
                        _ = user.removeInvalidKeys()
                        transaction.updateData(user.data, forDocument: ref)
                        self.user = user
                    }
                    return nil
                }
                ) { (object, error) in
                    if let error = error {
                        print("Transaction failed: \(error)")
                    } else {
                        print("Transaction successfully committed!")
                    }
                }
            }
            
            
        }
    }
    
    func fetchAppConfig(completion : @escaping ((WAWAppConfig?) -> ())) {
        let db = Firestore.firestore()
        db.collection("appConfig").document("ios").getDocument { (snapshot, error) in
            guard let document = snapshot else {
                print("Error fetching document: \(error!)")
                completion(nil)
                return
            }
            if let data = document.data() {
                completion(WAWAppConfig(data: data))
            }
        }
    }
    
    func isKeyAppInstalled() -> Bool {
        //return false
        #if !targetEnvironment(simulator)
        return UIApplication.shared.canOpenURL(WAWService.parakeyUrl)
        #else
        return true
        #endif
        
    }
    
    func subscribeToServices(changes : @escaping ((WAWServices?) -> ())) -> Subscription {
        let db = Firestore.firestore()
        let listener = db.collection("appConfig").document("services").addSnapshotListener { (documentSnapshot, error) in
            guard let document = documentSnapshot else {
                print("Error fetching document: \(error!)")
                changes(nil)
                return
            }
            if let data = document.data() {
                let services = WAWServices(data: data)
                changes(services)
            }
        }
        
        return Subscription(listener: listener)
    }
    
    func subscribeToBicycleServiceLocation(changes : @escaping (([WAWBicycleServiceLocation]) -> ())) -> Subscription {
        let db = Firestore.firestore()
        let listener = db.collection("bicycle_service").addSnapshotListener { (documentSnapshot, error) in
            guard let snapshot = documentSnapshot else {
                print("Error fetching document: \(error!)")
                return
            }
            let currentTimestamp = Date().timeIntervalSince1970
            var items = snapshot.documents.compactMap({ (document) -> WAWBicycleServiceLocation? in
                if let item = WAWBicycleServiceLocation(data: document.data()), item.endDate.timeIntervalSince1970 > currentTimestamp {
                    return item
                }
                return nil
            })
            items.sort { (lhs, rhs) -> Bool in
                return lhs.endDate < rhs.endDate
            }
            changes(items)
        }
        
        return Subscription(listener: listener)
    }
    
    func subscribeToAppConfig(changes : @escaping ((WAWAppConfig?) -> ())) -> Subscription {
        let db = Firestore.firestore()
        let listener = db.collection("appConfig").document("ios").addSnapshotListener { (documentSnapshot, error) in
            guard let document = documentSnapshot else {
                print("Error fetching document: \(error!)")
                changes(nil)
                return
            }
            if let data = document.data() {
                changes(WAWAppConfig(data: data))
            }
        }
        
        return Subscription(listener: listener)
    }
    
    func subscribeToFAQ(changes : @escaping (([WAWFAQ]?) -> ())) -> Subscription {
        let db = Firestore.firestore()
        let listener = db.collection("faq").addSnapshotListener { (documentSnapshot, error) in
            guard let snapshot = documentSnapshot else {
                print("Error fetching document: \(error!)")
                return
            }
            let schedules = snapshot.documents.compactMap({ (document) -> WAWFAQ in
                let faqItem = WAWFAQ(data: document.data())
                return faqItem
            })
            changes(schedules)
        }
        
        return Subscription(listener: listener)
    }
    
    func subscribeToUserChanges(changes : @escaping ((WAWUser) -> ())) -> Subscription? {
        guard let user = user else {
            return nil
        }
        let db = Firestore.firestore()
        let users = db.collection("user").whereField("email", isEqualTo: user.email)
        let listener = users.addSnapshotListener { (documentSnapshot, error) in
            guard let snapshot = documentSnapshot else {
                print("Error fetching document: \(error!)")
                return
            }
            if !snapshot.metadata.hasPendingWrites {
                if let doc = snapshot.documents.first {
                    let user = WAWUser(id: doc.documentID, data: doc.data())
                    self.user = user
                    changes(user)
                    return
                }
            }
        }
        
        return Subscription(listener: listener)
    }
    
    func subscribeToScheduleChanges(property : WAWProperty, changes : @escaping (([WAWSchedule]?) -> ())) -> Subscription {
        let db = Firestore.firestore()
        let listener = db.collection("schedules/\(property.id)/days").addSnapshotListener { (documentSnapshot, error) in
            guard let snapshot = documentSnapshot else {
                print("Error fetching document: \(error!)")
                changes(nil)
                return
            }
            if !snapshot.metadata.hasPendingWrites {
                let schedules = snapshot.documents.compactMap({ (document) -> WAWSchedule in
                    let schedule = WAWSchedule(date: document.documentID, data: document.data())
                    return schedule
                })
                changes(schedules)
            }
        }
        
        return Subscription(listener: listener)
    }
    
    func subscribeToProperty(property : WAWProperty, changes : @escaping ((WAWProperty?) -> ())) -> Subscription {
        let db = Firestore.firestore()
        let listener = db.collection("property").document(property.id).addSnapshotListener { (documentSnapshot, error) in
            guard let document = documentSnapshot else {
                print("Error fetching document: \(error!)")
                changes(nil)
                return
            }
            if !document.metadata.hasPendingWrites, let data = document.data() {
                if let property = WAWProperty(id: document.documentID, data: data) {
                    changes(property)
                }
            }
        }
        
        return Subscription(listener: listener)
    }
    
    private class func sortAndFilterAlerts(alerts : [WAWAlert]) -> [WAWAlert] {
        let now = NSDate().timeIntervalSince1970
        let available = alerts.filter({ (alert) -> Bool in
            return now > alert.publishAt && now < alert.unpublishAt
        })
        let sorted = available.sorted { (lhs, rhs) -> Bool in
            return lhs.publishAt > rhs.publishAt
        }
        return sorted
    }
    
    func subscribeToAlerts(changes : @escaping (([WAWAlert]?) -> ())) -> Subscription {
        if let cached = cachedAlerts {
            changes(WAWService.sortAndFilterAlerts(alerts: cached))
        }
        let db = Firestore.firestore()
        let listener = db.collection("alerts").addSnapshotListener { (documentSnapshot, error) in
            guard let snapshot = documentSnapshot else {
                print("Error fetching document: \(error!)")
                return
            }
            let collection = snapshot.documents.compactMap({ (document) -> WAWAlert? in
                if let item = WAWAlert(id: document.documentID, data: document.data()) {
                    return item
                }
                return nil
            })
            self.cachedAlerts = collection
            changes(WAWService.sortAndFilterAlerts(alerts: collection))
        }
        
        return Subscription(listener: listener)
    }
    
    func subscribeToProperties(changes : @escaping (([WAWProperty]?) -> ())) -> Subscription {
        if let cached = cachedProperties {
            changes(cached)
        }
        let db = Firestore.firestore()
        
        let listener = db.collection("property").addSnapshotListener { (documentSnapshot, error) in
            guard let snapshot = documentSnapshot else {
                print("Error fetching document: \(error!)")
                changes(nil)
                return
            }
            if !snapshot.metadata.hasPendingWrites {
                var properties = snapshot.documents.compactMap({ (document) -> WAWProperty? in
                    if let property = WAWProperty(id: document.documentID, data: document.data()), property.visible {
                        return property
                    }
                    return nil
                })
                properties.sort(by: { (lhs, rhs) -> Bool in
                    return lhs.name.localizedCompare(rhs.name) == .orderedAscending
                })
                self.cachedProperties = properties
                changes(properties)
            }
        }
        return Subscription(listener: listener)
        
    }
    
    
    func launchKeyApp(origin : String) {
        guard let user = user else {
            return
        }
        if UIApplication.shared.canOpenURL(WAWService.parakeyUrl) {
            let url = URL(string: "parakeyapp://parakey?username=\(user.email)&code=\(user.parakeyPassword)&urlscheme=fabegeapp://parakey/")
            UIApplication.shared.open(url!, options: [:]) { (completion) in
                
            }
        }
    }
    
    
    private func isAuthenticatedWithFirebase() -> Bool {
        guard let firebaseUser = Auth.auth().currentUser, firebaseUser.uid == user?.id else {
            return false
        }
        return true
    }
    
    func isEmailAvailable(email : String, completion : @escaping ((WAWUser?) -> ())) {
        let db = Firestore.firestore()
        let users = db.collection("user").whereField("email", isEqualTo: email.lowercased())
        
        users.getDocuments() { (querySnapshot, err) in
            if let doc = querySnapshot?.documents.first {
                let user = WAWUser(id: doc.documentID, data: doc.data())
                completion(user)
                return
            } else if let err = err {
                print("Error getting documents: \(err)")
            }
            completion(nil)
        }
    }
    
    func fetchUser(completion : @escaping ((WAWUser?) -> ())) {
        guard let user = user else {
            return completion(nil)
        }
        fetchUser(email: user.email) { (user, error) in
            completion(user)
        }
    }
    
    func fetchUser(userId : String, completion : @escaping ((WAWUser?, Error?) -> ())) {
        let db = Firestore.firestore()
        let user = db.collection("user").document(userId)
        user.getDocument { (documentSnapshot, error) in
            if let doc = documentSnapshot, let data = doc.data() {
                let user = WAWUser(id: doc.documentID, data: data)
                self.user = user
                completion(user, error)
                return
            } else if let error = error {
                print("Error getting documents: \(error)")
            }
            completion(nil, error)
        }
    }
    
    func fetchUser(email : String, completion : @escaping ((WAWUser?, Error?) -> ())) {
        let db = Firestore.firestore()
        let users = db.collection("user").whereField("email", isEqualTo: email.lowercased())
        
        users.getDocuments() { (querySnapshot, error) in
            if let doc = querySnapshot?.documents.first {
                let user = WAWUser(id: doc.documentID, data: doc.data())
                self.user = user
                completion(user, error)
                return
            } else if let error = error {
                print("Error getting documents: \(error)")
            }
            completion(nil, error)
            
        }
    }
    
    func setupBookings(property : WAWProperty, bookings : [WAWBookingReference], completion : @escaping ((Bool) -> ())) {
        guard let user = user else {
            completion(false)
            return
        }
        let db = Firestore.firestore()
        db.runTransaction({ (transaction, errorPointer) -> Any? in
            var references = Set<ScheduleWriterReference>()
            
            for booking in bookings {
                var ref = references.first(where: { (reference) -> Bool in
                    return reference.schedule.dateString == booking.date
                })
                if ref == nil {
                    let documentRef = db.document("schedules/\(property.id)/days/\(booking.date)/")
                    if let doc = try? transaction.getDocument(documentRef), let data = doc.data() {
                        let schedule = WAWSchedule(date: booking.date, data: data)
                        let maxSeats = property.availableSeats(dateKey: schedule.dateString)
                        ref = ScheduleWriterReference(reference: documentRef, schedule: schedule, maxSeats:maxSeats)
                        references.insert(ref!)
                    }
                    
                }
                if var ref = ref {
                    ref.bookings.append(booking)
                    references.update(with: ref)
                }
            }
            
            for ref in references {
                ref.writeData(userId: user.id, transaction: transaction)
            }
            return nil
        }) { (object, error) in
            if let error = error {
                print("Transaction failed: \(error)")
                completion(false)
            } else {
                print("Transaction successfully committed!")
                completion(true)
            }
        }
    }
    
    func continueSignIn(link : String, completion : @escaping ((String?, Error?) -> ())) {
        if let email = UserDefaults.standard.string(forKey: Key.email.rawValue),
           Auth.auth().isSignIn(withEmailLink: link) {
            Auth.auth().signIn(withEmail: email, link: link) { (user, error) in
                if let error = error {
                    completion(email, error)
                }
                self.fetchUser(email: email, completion: { (user, error) in
                    self.user = user
                    completion(email, error)
                    
                })
            }
        } else {
            completion(nil, nil)
        }
    }
    
    func continueSignInWithToken(_ token : String, completion : @escaping ((Bool, Error?) -> ())) {
        if let userId = UserDefaults.standard.string(forKey: Key.userId.rawValue) {
            Auth.auth().signIn(withCustomToken: token) { (user, error) in
                if let user = user?.user {
                    guard user.uid == userId else {
                        completion(false, nil)
                        return
                    }
                    self.fetchUser(userId: userId, completion: { (user, error) in
                        self.user = user
                        completion(true, error)
                    })
                } else {
                    completion(false, error)
                    return
                }
            }
        } else {
            completion(false, nil)
        }
        
    }
    
    func initiateCustomTokenAuthentication(userId : String, completion : @escaping ((Bool, Error?) -> ())) {
        UserDefaults.standard.set(userId, forKey: Key.userId.rawValue)
        Alamofire.request("https://us-central1-fabege-b28bd.cloudfunctions.net/authentificate?uid=\(userId)")
            .response { (response) in
                if response.response?.statusCode == 200 {
                    completion(true, nil)
                } else {
                    completion(false, response.error)
                }
            }
    }
    
    func initiateAuthentication(email : String, completion : @escaping ((Bool, Error?) -> ())) {
        
        let actionCodeSettings = ActionCodeSettings()
        actionCodeSettings.url = URL(string: "https://fabege.page.link/ey3j")
        actionCodeSettings.handleCodeInApp = true
        actionCodeSettings.setIOSBundleID(Bundle.main.bundleIdentifier!)
        actionCodeSettings.dynamicLinkDomain = "fabege.page.link"
        actionCodeSettings.setAndroidPackageName("se.fabege.app",
                                                 installIfNotAvailable: false, minimumVersion: "12")
        Auth.auth().sendSignInLink(toEmail:email,
                                   actionCodeSettings: actionCodeSettings) { error in
            if let error = error {
                completion(false, error)
                return
            }
            UserDefaults.standard.set(email, forKey: Key.email.rawValue)
            completion(true,nil)
        }
    }
    
    
    
    
    private struct ScheduleWriterReference : Hashable {
        
        
        var reference : DocumentReference
        var schedule : WAWSchedule
        var maxSeats : Int
        var bookings = [WAWBookingReference]()
        
        init(reference : DocumentReference, schedule : WAWSchedule, maxSeats : Int) {
            self.reference = reference
            self.schedule = schedule
            self.maxSeats = maxSeats
        }
        
        func hash(into hasher: inout Hasher) {
            hasher.combine(schedule.dateString)
        }
        
        static func == (lhs: WAWService.ScheduleWriterReference, rhs: WAWService.ScheduleWriterReference) -> Bool {
            return lhs.schedule.dateString == rhs.schedule.dateString
        }
        
        func writeData(userId: String, transaction: Transaction) {
            var modifiedSchedule = schedule
            for booking in bookings {
                if modifiedSchedule.timeSlots.count > booking.slotIdx {
                    var slot = schedule.timeSlots[booking.slotIdx]
                    if booking.add && !slot.ids.contains(userId) && maxSeats > slot.ids.count {
                        slot.ids.append(userId)
                    } else if !booking.add, let idx = slot.ids.firstIndex(of: userId) {
                        slot.ids.remove(at: idx)
                    }
                    modifiedSchedule.timeSlots[booking.slotIdx] = slot
                }
            }
            transaction.updateData(modifiedSchedule.data, forDocument: reference)
        }
    }
    
    
}


// User defaults quick storage
extension WAWService {
    
    private enum Key : String {
        case parakeyConnected = "parakeyConnected"
        case onboardingShown = "WAWOnboardingShown"
        case user = "WAWUser"
        case cachedProperties = "cachedProperties2"
        case email = "WAWUserId"
        case userId = "FIRUserId"
        case cachedAlerts = "cachedAlerts"
    }
    
    private var cachedProperties : [WAWProperty]? {
        get {
            if let data = UserDefaults.standard.object(forKey: Key.cachedProperties.rawValue) as? Data {
                let decoder = JSONDecoder()
                if let user = try? decoder.decode([WAWProperty].self, from: data) {
                    return user
                }
            }
            return nil
        }
        set {
            guard let newValue = newValue else {
                UserDefaults.standard.set(nil, forKey: Key.cachedProperties.rawValue)
                return
            }
            
            let encoder = JSONEncoder()
            if let encoded = try? encoder.encode(newValue) {
                UserDefaults.standard.set(encoded, forKey: Key.cachedProperties.rawValue)
            }
        }
    }
    
    private var cachedAlerts : [WAWAlert]? {
        get {
            if let data = UserDefaults.standard.object(forKey: Key.cachedAlerts.rawValue) as? Data {
                let decoder = JSONDecoder()
                if let user = try? decoder.decode([WAWAlert].self, from: data) {
                    return user
                }
            }
            return nil
        }
        set {
            guard let newValue = newValue else {
                UserDefaults.standard.set(nil, forKey: Key.cachedAlerts.rawValue)
                return
            }
            
            let encoder = JSONEncoder()
            if let encoded = try? encoder.encode(newValue) {
                UserDefaults.standard.set(encoded, forKey: Key.cachedAlerts.rawValue)
            }
        }
    }
    
    private var user : WAWUser? {
        get {
            if let data = UserDefaults.standard.object(forKey: Key.user.rawValue) as? Data {
                let decoder = JSONDecoder()
                if let user = try? decoder.decode(WAWUser.self, from: data) {
                    return user
                }
            }
            return nil
        }
        set {
            guard let newValue = newValue else {
                UserDefaults.standard.set(nil, forKey: Key.user.rawValue)
                return
            }
            
            let encoder = JSONEncoder()
            if let encoded = try? encoder.encode(newValue) {
                UserDefaults.standard.set(encoded, forKey: Key.user.rawValue)
            }
        }
    }
    
    func setParakeyConnected() {
        UserDefaults.standard.set(true, forKey: Key.parakeyConnected.rawValue)
    }
    
    func setOnboardingShown() {
        UserDefaults.standard.set(true, forKey: Key.onboardingShown.rawValue)
    }
}

extension WAWUser {
    
    func activeKeys(property : WAWProperty) -> Int {
        let keys = accessKeys.filter { return $0.components()?.propertyId == property.id }
        return keys.count
    }
    
    func hasValidAccessForProperties(properties : [WAWProperty]) -> Bool {
        let currentTimestamp = Date().timeIntervalSince1970
        for key in accessKeys {
            let cComps = key.components()
            let cProperty = properties.first { (property) -> Bool in
                return property.id == cComps?.propertyId
            }
            guard let property = cProperty,
                  let comps = cComps,
                  property.openingHours.count > comps.slotIdx else {
                continue
            }
            let hours = property.openingHours[comps.slotIdx]
            guard let beginDate = DateUtil.timestamp(time: hours.start, dateKey: comps.dateKey),
                  let endDate = DateUtil.timestamp(time: hours.end, dateKey: comps.dateKey) else {
                continue
            }
            if currentTimestamp > beginDate && currentTimestamp < endDate {
                return true
            }
        }
        return false
    }
    
    
}

extension WAWSchedule {
    
    func isOngoingOrUpcoming(hours : WAWProperty.OpeningHours) -> Bool {
        let currentTimestamp = Date().timeIntervalSince1970
        guard let endDate = DateUtil.timestamp(time: hours.end, dateKey: dateString) else {
            return false
        }
        if currentTimestamp > endDate {
            return false
        }
        return true
    }
    
}
