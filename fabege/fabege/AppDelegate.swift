//
//  AppDelegate.swift
//  fabege
//
//  Created by Patrik Billgert on 18/03/15.
//  Copyright (c) 2015 Frontcell AB. All rights reserved.
//

import UIKit
import CoreLocation
import Fabric
import Crashlytics
import Firebase
import FirebaseDynamicLinks
import UserNotifications
import FirebaseMessaging

let NotificationLocationUpdates = "SKNotificationLocationUpdates"


@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, CLLocationManagerDelegate {
    
    var window: UIWindow?
    var locationManager : CLLocationManager!
    var lastLocation : CLLocation!
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        setupAppearance()
        //model.setBicycleServiceFavorites(property: nil)

        
        FirebaseApp.configure()
        Fabric.with([Crashlytics.self])
        Messaging.messaging().delegate = self
        AppConfigService.shared.configure()
        
        //refreshFirebaseID()
        if WAWService().status() == .ready {
            registerForPUSH()
        }
        
        return true
    }
    
    class func deviceToken() -> String {
        if let token = UserDefaults.standard.object(forKey: "deviceToken") as! String? {
            return token as String
        } else {
            return ""
        }
    }
    
    func refreshFirebaseID() {
        InstanceID.instanceID().instanceID { (result, error) in
            if let error = error {
                print("Error fetching remote instance ID: \(error)")
            } else if let result = result {
                print("Remote instance ID token: \(result.token)")
                WAWService().updateDeviceToken(deviceToken: result.token)
            }
        }
    }
    
    func registerForPUSH() {
        // For iOS 10 display notification (sent via APNS)
        UNUserNotificationCenter.current().delegate = self
        
        let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
        
        UNUserNotificationCenter.current().requestAuthorization(
            options: authOptions,
            completionHandler: { granted, error in
                if let error = error {
                    print(error)
                }
        })
        UIApplication.shared.registerForRemoteNotifications()
        
    }
    
    
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        print("### \(error)")
    }
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        let token = deviceToken.map { String(format: "%02.2hhx", $0) }.joined()
        print("### apns \(token)")
        Messaging.messaging().apnsToken = deviceToken
    }
    
    
    func setupLocationManager() {
        guard locationManager == nil else {
            return
        }
        locationManager = CLLocationManager()
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.startUpdatingLocation()
        
        let authstate = CLLocationManager.authorizationStatus()
        if(authstate == CLAuthorizationStatus.notDetermined){
            print("Not Authorised")
            if #available(iOS 8.0, *) {
                locationManager.requestWhenInUseAuthorization()
            }
        }
    }
    
    func application(_ application: UIApplication, continue userActivity: NSUserActivity, restorationHandler: @escaping ([UIUserActivityRestoring]?) -> Void) -> Bool {
        guard let url = userActivity.webpageURL else {
            return false
        }
        if let navVC = window?.rootViewController as? UINavigationController {
            return WAWRouter(navigationController: navVC).handleDynamicLink(link: url.absoluteString)
        }
        return false
    }
    
    
    @available(iOS 9.0, *)
    func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any] = [:]) -> Bool {
        return application(app, open: url,
                           sourceApplication: options[UIApplication.OpenURLOptionsKey.sourceApplication] as? String,
                           annotation: "")
    }
    
    func application(_ application: UIApplication, open url: URL, sourceApplication: String?, annotation: Any) -> Bool {
        if DynamicLinks.dynamicLinks().dynamicLink(fromCustomSchemeURL: url) != nil {
            return true
        } else if let fabegeURL = FabegeURL.parse(url: url) {
            switch fabegeURL.type {
                case .parakey:
                    if let vc = window?.rootViewController {
                        _ = DialogController.display(from: vc, title: "Välkommen", highlightedText: "Dörren är nu öppen", defaultText: nil, lightMode: true, dialogType: .openDoor)
                }
                case .authenticate:
                    if let navVC = window?.rootViewController as? UINavigationController, let token = fabegeURL.path {
                        return WAWRouter(navigationController: navVC).handleAuthLink(token: token)
                }
            }
        }
        return false
    }
    
    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
        UIApplication.shared.applicationIconBadgeNumber = 0
        WAWService().validateUser()
        
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    
    
    
    // MARK: CLLocationManagerDelegate
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let locationArray = locations as NSArray
        let locationObj = locationArray.lastObject as! CLLocation
        
        self.lastLocation = locationObj
        
        NotificationCenter.default.post(name: Notification.Name(rawValue: NotificationLocationUpdates), object: nil)
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        print("status \(status)")
    }
    
    func switchToWaw() {
        
    }
    
    // MARK: UI
    
    func setupAppearance() {
        UIBarButtonItem.appearance(whenContainedInInstancesOf: [UINavigationBar.self]).tintColor = UIColor.white
        UINavigationBar.appearance().barTintColor = UIColor(red: 110.0/255.0, green: 30.0/255.0, blue: 130.0/255.0, alpha: 1.0)
        UINavigationBar.appearance().tintColor = UIColor.white
        UINavigationBar.appearance().titleTextAttributes = convertToOptionalNSAttributedStringKeyDictionary([
            NSAttributedString.Key.foregroundColor.rawValue:UIColor.white,
            NSAttributedString.Key.font.rawValue:UIFont(name: "HelveticaNeue", size: 18)!
        ])
        
        let application = UIApplication.shared
        application.statusBarStyle = .lightContent
    }
}


// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertToOptionalNSAttributedStringKeyDictionary(_ input: [String: Any]?) -> [NSAttributedString.Key: Any]? {
    guard let input = input else { return nil }
    return Dictionary(uniqueKeysWithValues: input.map { key, value in (NSAttributedString.Key(rawValue: key), value)})
}


extension AppDelegate : MessagingDelegate, UNUserNotificationCenterDelegate {
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String) {
        print("### Token: \(fcmToken)")
        WAWService().updateDeviceToken(deviceToken: fcmToken)
    }
    
    // Receive displayed notifications for iOS 10 devices.
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                willPresent notification: UNNotification,
                                withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        let userInfo = notification.request.content.userInfo
        
        // With swizzling disabled you must let Messaging know about the message, for Analytics
        // Messaging.messaging().appDidReceiveMessage(userInfo)
        
        // Print message ID.
        //if let messageID = userInfo[gcmMessageIDKey] {
        //     print("Message ID: \(messageID)")
        //}
        
        // Print full message.
        print(userInfo)
        
        // Change this to your preferred presentation option
        completionHandler([])
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                didReceive response: UNNotificationResponse,
                                withCompletionHandler completionHandler: @escaping () -> Void) {
        let userInfo = response.notification.request.content.userInfo
        // Print message ID.
        //if let messageID = userInfo[gcmMessageIDKey] {
        //     print("Message ID: \(messageID)")
        //}
        
        // Print full message.
        print(userInfo)
        
        completionHandler()
    }
    
}


struct FabegeURL {
    
    var type : UrlType
    var path : String?
    
    enum UrlType {
        case parakey
        case authenticate
    }
    
    static func parse(url : URL) -> FabegeURL? {
        let comps = URLComponents(url: url, resolvingAgainstBaseURL: true)
        guard let host = comps?.host else {
            return nil
        }
        if host == "parakey" {
            return FabegeURL(type: .parakey, path: nil)
        } else if host == "authenticate" {
            var path = comps?.path
            path?.removeFirst()
            if let path = path {
                return FabegeURL(type: .authenticate, path:path)
            }
            
        }
        return nil
    }
    
}
