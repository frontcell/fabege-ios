//
//  WAWScheduleVC.swift
//  fabege
//
//  Created by Oscar Kockum on 2018-12-19.
//  Copyright © 2018 Frontcell AB. All rights reserved.
//

import UIKit
//import JGProgressHUD
import Kingfisher
import MapKit
import PieCharts
import FirebasePerformance

class WAWScheduleVC: UIViewController, WAWScheduleViewListener {
     
     static let UserKeyValidation = true
     
     @IBOutlet weak var mainScrollView: UIScrollView!
     @IBOutlet weak var scheduleView: WAWScheduleView!
     @IBOutlet weak var bookingsButton: UIButton!
     @IBOutlet weak var keyButton: UIButton!
     @IBOutlet weak var propertyTitle: UILabel!
     @IBOutlet weak var propertyAddress: UILabel!
     
     @IBOutlet weak var aboutTitle: UILabel!
     @IBOutlet weak var propertyImageView: UIImageView!
     @IBOutlet weak var propertyDescription: UITextView!
     
     @IBOutlet weak var wifiButton: UIButton!
     @IBOutlet weak var printersButton: UIButton!
     @IBOutlet weak var coffeeButton: UIButton!
     @IBOutlet weak var conferenceButton: UIButton!
     @IBOutlet weak var facilityInfoText: UILabel!
     @IBOutlet weak var wifiUsername: UILabel!
     @IBOutlet weak var wifiPassword: UILabel!
     @IBOutlet weak var wifiCredentialsView: UIStackView!
     
     @IBOutlet weak var conferenceRoomsLabel: UILabel!
     @IBOutlet weak var seatsLabel: UILabel!
     @IBOutlet weak var phoneRoomsLabel: UILabel!
     @IBOutlet weak var goodToKnowText: UITextView!
     
     @IBOutlet weak var dotSeats: UIView!
     @IBOutlet weak var dotConferences: UIView!
     @IBOutlet weak var dotPhone: UIView!
     
     @IBOutlet weak var pieChart: PieChart!
     
     @IBOutlet weak var addressLabel: UILabel!
     @IBOutlet weak var mapView: MKMapView!
     
     @IBOutlet weak var contactImageView: UIImageView!
     @IBOutlet weak var contactName: UILabel!
     @IBOutlet weak var contactTitle: UILabel!
     @IBOutlet weak var contactEmail: UILabel!
     @IBOutlet weak var contactTel: UILabel!
     @IBOutlet weak var faqButton: UIButton!
     @IBOutlet weak var alertCollectionView: UICollectionView!
     @IBOutlet weak var alertIcon: UIImageView!
    
     private var alertViewHandler : AlertViewUIHandler!
     var property : WAWProperty!
     var bookings = [WAWBookingReference]()
     var service = WAWService()
     var timer : Timer?
    
    private var trace : Trace?
    private var inBookingsRequest = false {
        didSet {
            if inBookingsRequest {
                trace?.stop()
                trace = Performance.startTrace(name: "bookDesks")
            } else {
                trace?.stop()
            }
        }
    }
     private var dialogController : DialogController?
     
     static let df : DateFormatter = {
          let df = DateFormatter()
          df.dateFormat = "yyyyMMdd"
          return df
     }()
     
     var facilityType : FacilityType? = .wifi
     
     enum FacilityType : Int {
          case wifi
          case printers
          case coffee
          case conference
          
          static func parse(value : Int) -> FacilityType {
               switch value {
               case FacilityType.wifi.rawValue:
                    return .wifi
               case FacilityType.printers.rawValue:
                    return .printers
               case FacilityType.coffee.rawValue:
                    return .coffee
               case FacilityType.conference.rawValue:
                    return .conference
               default:
                    return .wifi
               }
          }
     }
     
     var subscriptions = [WAWService.Subscription]()
     
     deinit {
          subscriptions.forEach { (subscription) in
               subscription.detach()
          }
     }
     
     
     fileprivate func updatePropertyInfo() {
          propertyTitle.text = property.name
          propertyAddress.text = property.address

          aboutTitle.text = "Om \(property.name)"
          if let url = URL(string: property.imageUrl) {
               propertyImageView.kf.indicatorType = .activity
               propertyImageView.kf.setImage(with: url)
          }
          propertyDescription.formatHTML(text: property.info)
          seatsLabel.text = "\(property.capacity.seats) arbetsplatser"
          conferenceRoomsLabel.text = "\(property.capacity.conferenceRooms) mötesrum"
          //phoneRoomsLabel.text = "\(property.capacity.phoneRooms) telefonrum"
          phoneRoomsLabel.isHidden = true
          goodToKnowText.formatHTML(text: property.goodToKnow)
          addressLabel.text = property.address
          
          let coordinate = CLLocationCoordinate2D(latitude: property.lat, longitude: property.lon)
          let regionRadius: CLLocationDistance = 500
          let coordinateRegion = MKCoordinateRegion(center: coordinate,
                                                    latitudinalMeters: regionRadius * 2.0, longitudinalMeters: regionRadius * 2.0)
          mapView.setRegion(coordinateRegion, animated: false)
          mapView.removeAnnotations(mapView.annotations)
          let point = MKPointAnnotation()
          point.coordinate = coordinate
          mapView.addAnnotation(point)
          
          if let url = URL(string: property.contact.imageUrl) {
               contactImageView.kf.setImage(with: url)
          }
          contactName.text = property.contact.name
          contactTitle.text = property.contact.title
          contactEmail.text = property.contact.email
          contactTel.text = property.contact.tel.toPhoneNumber()
          contactEmail.underline()
          contactTel.underline()
          
          updateFacilityInfo(type: facilityType)
          
          pieChart.referenceAngle = 270
          pieChart.clear()
          pieChart.models = [
               /*PieSliceModel(value: Double(property.capacity.phoneRooms), color: UIColor(hexString: "E6E1DC")),*/
               PieSliceModel(value: Double(property.capacity.conferenceRooms), color:UIColor(hexString: "8B1984").withAlphaComponent(0.5)),
               PieSliceModel(value: Double(property.capacity.seats), color: UIColor(hexString: "8B1984"))
          ]
          
     }
    
    
     override func viewDidLoad() {
          super.viewDidLoad()
    
          alertViewHandler = AlertViewUIHandler(collectionView: alertCollectionView, alertIcon: alertIcon, propertyId: property.id)
          scheduleView.bookingsListener = self
          
          wifiButton.tag = FacilityType.wifi.rawValue
          printersButton.tag = FacilityType.printers.rawValue
          coffeeButton.tag = FacilityType.coffee.rawValue
          conferenceButton.tag = FacilityType.conference.rawValue
          updatePropertyInfo()
          mapView.delegate = self
          goodToKnowText.textContainerInset = UIEdgeInsets.zero
          propertyDescription.textContainerInset = UIEdgeInsets.zero
          goodToKnowText.textContainer.lineFragmentPadding = 0;
          propertyDescription.textContainer.lineFragmentPadding = 0;
          propertyAddress.isUserInteractionEnabled = true
          propertyAddress.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(WAWScheduleVC.didTapMapView)))          
          mapView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(WAWScheduleVC.didTapMapView)))
          
          contactEmail.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(WAWScheduleVC.didTapEmail)))
          contactTel.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(WAWScheduleVC.didTapTel)))
          contactEmail.isUserInteractionEnabled = true
          contactTel.isUserInteractionEnabled = true
          
          dotSeats.outerShadow(show: true, color: UIColor.black, alpha: 0.38, height: 2, blur: 3, animated: false)
          dotPhone.outerShadow(show: true, color: UIColor.black, alpha: 0.38, height: 2, blur: 3, animated: false)
          dotConferences.outerShadow(show: true, color: UIColor.black, alpha: 0.38, height: 2, blur: 3, animated: false)
          
          var subscription = service.subscribeToScheduleChanges(property: property) { [weak self] (schedules) in
               guard let schedules = schedules, schedules.count > 0 else {
                    // show error and go back?
                    return
               }
               if let this = self {
                    this.scheduleView.setSchedule(schedules: schedules, userId: this.service.userId, property: this.property)
               }
          }
          subscriptions.append(subscription)
          
          subscription = service.subscribeToProperty(property: property) { [weak self] (property) in
               self?.property = property
               self?.updatePropertyInfo()
          }
          subscriptions.append(subscription)
          
          let userSubscription = service.subscribeToUserChanges { [weak self] (user) in
               self?.onUserUpdated(user: user)
          }
          if let userSubscription = userSubscription {
               subscriptions.append(userSubscription)
          }
          
          
          updateView()
     }
     
     override func viewDidAppear(_ animated: Bool) {
          FabegeAnalytics.trackPageView(name: property.name)
     }
     
     @objc private func didTapMapView() {
          let coordinate = CLLocationCoordinate2D(latitude: property.lat, longitude: property.lon)
          let regionRadius: CLLocationDistance = 500
          let coordinateRegion = MKCoordinateRegion(center: coordinate,
                                                    latitudinalMeters: regionRadius * 2.0, longitudinalMeters: regionRadius * 2.0)
          
          let options = [MKLaunchOptionsMapCenterKey:NSValue(mkCoordinate:coordinateRegion.center),MKLaunchOptionsMapSpanKey:NSValue(mkCoordinateSpan:coordinateRegion.span)]
          
          let placemark = MKPlacemark(coordinate: coordinate, addressDictionary: nil)
          let mapitem = MKMapItem(placemark: placemark)
          mapitem.name = property.name
          mapitem.openInMaps(launchOptions: options)
     }
     
     @objc func didTapEmail() {
          if !property.contact.email.isEmpty {
               if let url = URL(string: "mailto:\(property.contact.email)") {
                    UIApplication.shared.open(url, options: [:], completionHandler: nil)
               }
          }
     }
     
     @objc func didTapTel() {
          if !property.contact.tel.isEmpty {
               var number = property.contact.tel
               if number.starts(with: "46") {
                    number = "+" + number
               }
               guard let url = URL(string: "tel://" + property.contact.tel) else { return }
               UIApplication.shared.open(url)
          }
     }
     
     private func updateView(animated : Bool = false) {
          
          let hasChanges = !self.bookings.isEmpty
          if animated {
               UIView.transition(with: self.bookingsButton,
                                 duration: 0.3,
                                 options: .transitionCrossDissolve,
                                 animations: { self.bookingsButton.isEnabled = hasChanges },
                                 completion: nil)
          } else {
               self.bookingsButton.isEnabled = hasChanges
          }
          self.bookingsButton.outerShadow(show: hasChanges, animated: animated)
          
          let isModifying = self.bookings.contains { return $0.add == false }
          if hasChanges && isModifying {
               self.bookingsButton.setTitle("Ändra bokning", for: .normal)
          } else if hasChanges {
               self.bookingsButton.setTitle("Boka", for: .normal)
          }
          
          self.keyButton.outerShadow(show: true, animated: animated)
          
          
     }
     
     func onBookingsChanged(bookings : [WAWBookingReference]) {
          self.bookings = bookings
          updateView(animated: true)
     }
     
     
     
     fileprivate func validateUserHasAccessKeysForBookings(user: WAWUser) -> Bool {
          let userBookings = user.bookings
          var isMissingBookingKeys = false
          for booking in bookings {
               if booking.add && !userBookings.contains(booking) {
                    isMissingBookingKeys = true
               }
          }
          if !isMissingBookingKeys {
               // all are validated!
               return true
          } else {
               // wait
               return false
          }
     }
     
     private func onUserUpdated(user : WAWUser) {
          if user.hasValidAccessForProperties(properties: [property]) {
               keyButton.outerShadow(show: true, animated: true)
               keyButton.isEnabled = true
          } else {
               keyButton.outerShadow(show: false, animated: true)
               keyButton.isEnabled = false
          }
          
          if inBookingsRequest {
               if validateUserHasAccessKeysForBookings(user: user) {
                    showSuccessDialog()
               }
          }
     }
     
     private func updateFacilityInfo(type : FacilityType?, animated : Bool = false) {
          wifiButton.tintColor = type == .wifi ? UIColor.waw.purpleColor : UIColor.black
          printersButton.tintColor = type == .printers ? UIColor.waw.purpleColor : UIColor.black
          coffeeButton.tintColor = type == .coffee ? UIColor.waw.purpleColor : UIColor.black
          conferenceButton.tintColor = type == .conference ? UIColor.waw.purpleColor : UIColor.black
          if animated {
               UIView.beginAnimations(nil, context: nil)
          }
          let showWifi = type == .wifi && property.facilities.wifi.username != nil && property.facilities.wifi.password != nil
          self.wifiCredentialsView.isHidden = !showWifi
          
          if let type = type {
               facilityInfoText.isHidden = false
               switch type {
               case .wifi:
                    facilityInfoText.formatHTML(text: property.facilities.wifi.info)
                    wifiUsername.text = property.facilities.wifi.username
                    wifiPassword.text = property.facilities.wifi.password
               case .printers:
                    facilityInfoText.formatHTML(text: property.facilities.printers)
               case .coffee:
                    facilityInfoText.formatHTML(text: property.facilities.coffee)
               case .conference:
                    facilityInfoText.formatHTML(text: property.facilities.conference)
               }
          } else {
               facilityInfoText.isHidden = true
          }
          view.layoutIfNeeded()
          if animated {
               UIView.commitAnimations()
          }
          
     }
     
     
     private func dismissProgressView() {
          dismiss(animated: true, completion: nil)
     }
     
     private func showProgressView(status : String) {
          if let dialogController = dialogController, dialogController.type == .loading {
               dialogController.highlightedText = status
          } else {
               dismissProgressView()
               dialogController = DialogController.display(from: self, title: "Snart klar", highlightedText: status, defaultText: nil, lightMode: true, dialogType: .loading)
          }
     }
     
     private func showSuccessDialog() {
          FabegeAnalytics.trackEvent("WAW_Booking_success", parameters:nil)
          inBookingsRequest = false
          dismissProgressView()
          timer?.invalidate()
          
          let output = DateFormatter()
          output.dateFormat = "EEE d MMM"
          output.locale = Locale(identifier: "sv_SE")
          
          
          var result = ""
          for (idx, booking) in bookings.sorted().enumerated() {
               guard booking.add else {
                    continue
               }
               if idx > 0 {
                    result += "\n"
               }
               let date = WAWScheduleVC.df.date(from: booking.date)!
               result += output.string(from: date)
               result += " kl. "
               
               let start = property.openingHours[booking.slotIdx].start.replacingOccurrences(of: ":00", with: "")
               let end = property.openingHours[booking.slotIdx].end.replacingOccurrences(of: ":00", with: "")
               result += "\(start)-\(end)"
          }
          let dialogType : DialogController.DialogType
          if result.isEmpty {
               result = (bookings.count == 1 ? "Bokningen är borttagen" : "Bokningarna är borttagna")
               dialogType = .noAction
          } else {
               dialogType = .addCalendar
          }
          let currentBookings = bookings
          dialogController = DialogController.display(from: self, title: "Din bokning", highlightedText: result, defaultText: nil, lightMode: true, dialogType: dialogType)
          dialogController?.onAddCalendarClicked = { [weak self] in
               if let wSelf = self {
                    wSelf.dismissProgressView()
                    wSelf.addEvents(bookings: currentBookings)
               }
          }
          resetInput()
          updateView(animated: true)
     }
     
     private func addEvents(bookings : [WAWBookingReference]) {
          let events = bookings.compactMap({ (reference) -> CalendarService.Event? in
               guard reference.add,
                    let startTimestamp = DateUtil.timestamp(time: property.openingHours[reference.slotIdx].start, dateKey: reference.date),
                    let endTimestamp = DateUtil.timestamp(time: property.openingHours[reference.slotIdx].end, dateKey: reference.date) else {
                    return nil
               }
               let event = CalendarService.Event(title: property.name, startDate: Date(timeIntervalSince1970: startTimestamp), endDate: Date(timeIntervalSince1970: endTimestamp), description: property.goodToKnow, latitude: property.lat, longitude:property.lon, id:reference.identifier)
               
               return event
          })
          dismiss(animated: true, completion: nil)
          if !bookings.isEmpty {
               CalendarService.addEvents(events: events, completion: { (success) in
                    var successText : String
                    var failedText : String
                    if bookings.count > 1 {
                         successText = "Bokningarna är tillagda i din kalender"
                         failedText = "Kunde inte lägga till bokningarna i din kalender"
                    } else {
                         successText = "Bokningen är tillagd i din kalender"
                         failedText = "Kunde inte lägga till bokningen i din kalender"
                    }
                    if success {
                         self.dialogController = DialogController.display(from: self, title: "Klart!", highlightedText: successText, defaultText: nil, lightMode: true)
                    } else {
                         self.dialogController = DialogController.display(from: self, title: "Problem", highlightedText: failedText, defaultText: nil, lightMode: true)
                    }
               })
          }
     }
     
     private func resetInput() {
          bookings.removeAll()
          scheduleView.resetView()
     }
     
     private func showFailedDialog() {
          FabegeAnalytics.trackEvent("WAW_Booking_fail", parameters:nil)
          inBookingsRequest = false
          timer?.invalidate()
          dismissProgressView()
          dialogController = DialogController.display(from: self, title: "Problem", highlightedText: "Just nu verkar det vara problem med bokningen", defaultText: "Vänligen verifiera att du fått en nyckel eller försök igen.", lightMode: true)
          
          resetInput()
          updateView(animated: true)
     }
     
     private func validateUserBookings() {
          service.fetchUser { (user) in
               if let user = user {
                    if self.validateUserHasAccessKeysForBookings(user: user) {
                         self.showSuccessDialog()
                    } else {
                         self.showFailedDialog()
                    }
               } else {
                    self.showFailedDialog()
               }
          }
     }
     
     @IBAction func actionBook(_ sender: Any) {
          guard !inBookingsRequest || !bookings.isEmpty else {
               return
          }
          inBookingsRequest = true
          let containsRemoving = self.bookings.contains { return $0.add == false }
          if containsRemoving {
               dialogController = DialogController.display(from: self, title: "Vänta!", highlightedText: "Är du säker på att du vill avboka din plats?", defaultText: nil, lightMode: true, dialogType: .yesNo)
               dialogController?.onYesActionClicked = { [weak self] in
                    self?.startBookingProcedure()
               }
               dialogController?.onNoActionClicked = { [weak self] in
                    self?.dismiss(animated: true, completion: nil)
               }
          } else {
               startBookingProcedure()
          }
          
          
     }
     
     func startBookingProcedure() {
          let bookingsToRemove = bookings.filter { return $0.add == false }
          let containsRemoving = !bookingsToRemove.isEmpty
          let progressStatus = containsRemoving ? "Ändrar bokning" : "Bekräftar bokning"
          showProgressView(status: progressStatus)
          FabegeAnalytics.trackEvent("WAW_Booking_start", parameters:nil)
          service.setupBookings(property: property, bookings: bookings) { (success) in
               if success {
                    if !bookingsToRemove.isEmpty {
                         CalendarService.removeCalendarItem(items: bookingsToRemove)
                    }
                    if WAWScheduleVC.UserKeyValidation {
                         self.showProgressView(status: "Uppdaterar dina nycklar")
                         self.timer?.invalidate()
                         self.timer = Timer.scheduledTimer(withTimeInterval: 20, repeats: false, block: { (timer) in
                              if self.inBookingsRequest {
                                   self.validateUserBookings()
                              }
                         })
                    } else {
                         self.showSuccessDialog()
                    }
               } else {
                    self.showFailedDialog()
               }
          }
     }
     
     @IBAction func actionOpenKeyApp(_ sender: Any) {
          FabegeAnalytics.trackEvent("WAW_Open_door", parameters:nil)
          service.launchKeyApp(origin: "")
     }
     
     @IBAction func actionBack(_ sender: Any) {
          if !bookings.isEmpty {
               dialogController = DialogController.display(from: self, title: "Vänta!", highlightedText: "Vill du verkligen avbryta bokningen?", defaultText: nil, lightMode: true, dialogType: .yesNo)
               dialogController?.onYesActionClicked = { [weak self] in
                    self?.navigationController?.popViewController(animated: true)
                    self?.dismiss(animated: false, completion:nil)

               }
               dialogController?.onNoActionClicked = { [weak self] in
                    self?.dismiss(animated: true, completion: nil)
               }
          } else {
               navigationController?.popViewController(animated: true)
          }
     }
     
     @IBAction func actionFacilityInfo(_ sender: UIButton) {
          let type = FacilityType.parse(value: sender.tag)
          if type == facilityType {
               facilityType = nil
          } else {
               facilityType = type
          }
          updateFacilityInfo(type: facilityType, animated: false)
     }
     
}

extension WAWScheduleVC : MKMapViewDelegate {
     
     func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
          if annotation is MKPointAnnotation {
               var annotationView = mapView.dequeueReusableAnnotationView(withIdentifier: "pin")
               if annotationView == nil {
                    annotationView = MKAnnotationView(annotation: annotation, reuseIdentifier: "pin")
                    annotationView?.canShowCallout = false
               } else {
                    annotationView?.annotation = annotation
               }
               
               annotationView?.image = #imageLiteral(resourceName: "purple_map_pin")
               return annotationView
          }
          return nil
     }
     
}

fileprivate extension WAWUser {
     var bookings : [WAWBookingReference] {
          var retVal = [WAWBookingReference]()
          for accessKey in accessKeys {
               if let comps = accessKey.components() {
                    retVal.append(WAWBookingReference(date:comps.dateKey, slotIdx:comps.slotIdx, propertyId:comps.propertyId))
               }
          }
          return retVal
     }
     
}

extension WAWScheduleVC : UIScrollViewDelegate {
     func scrollViewDidScroll(_ scrollView: UIScrollView) {
          let alpha = (68.0 - scrollView.contentOffset.y) / 68.0
          if alpha < 0 {
               faqButton.alpha = 0
          } else if alpha > 1 {
               faqButton.alpha = 1
          } else {
               faqButton.alpha = alpha
          }
     }
}

extension WAWBookingReference : CalendarItem {
     var id: String {
          return identifier
     }
     
     
}
