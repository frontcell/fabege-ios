//
//  URLUtils.swift
//  fabege
//
//  Created by Oscar Kockum on 2016-03-18.
//  Copyright © 2016 Frontcell AB. All rights reserved.
//

import Foundation

private let BaseURL = "https://www.fabege.se/fastigheter"

class URLUtils {
     
     class func urlForProperty(_ property : Property, suffix : String? = nil) -> URL {
          let string = cleanString(property.name)
          if let hashtag = suffix {
               return URL(string: string+hashtag)!
          } else {
               return URL(string: string)!
          }
          
     }
     
     class func cleanString(_ string: String) -> String {
          let toArray = string.components(separatedBy: " ")
          let newString = toArray.joined(separator: "-").replacingOccurrences(of: "å", with: "a", options: NSString.CompareOptions.literal, range: nil).replacingOccurrences(of: "ä", with: "a", options: NSString.CompareOptions.literal, range: nil).replacingOccurrences(of: "ö", with: "o", options: NSString.CompareOptions.literal, range: nil)
          return "\(BaseURL)/\(newString)"
     }
     
}
