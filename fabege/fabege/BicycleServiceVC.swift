//
//  BicycleServiceVC.swift
//  fabege
//
//  Created by Oscar Kockum on 2020-01-08.
//  Copyright © 2020 Frontcell AB. All rights reserved.
//

import UIKit
import CoreLocation
import MapKit
import Contacts

class BicycleServiceVC: UIViewController, UITableViewDataSource, UITableViewDelegate {

    @IBOutlet weak var tableview: UITableView!
    @IBOutlet weak var infoText: UILabel!
    
    var sectionedLocations = [[WAWBicycleServiceLocation]]()
    var idxOfViewControllerToRemove : Int?
    var serviceLocations : [WAWBicycleServiceLocation]? {
        didSet {
            sortLocations()
        }
    }
    var subscription : WAWService.Subscription?
    var service : WAWServices.BicycleService!
    var selectedProperty : Property!
    
    deinit {
        subscription?.detach()
    }
    
    override func viewDidLoad() {
        self.infoText.text = service.serviceDescription.insertNewLines().replacingOccurrences(of: " kr", with: "\u{00a0}kr")

        super.viewDidLoad()
        tableview.tableFooterView = UIView()
        tableview.register(CustomHeader.self,
                           forHeaderFooterViewReuseIdentifier: "sectionHeader")
        tableview.sectionHeaderHeight = 66
        subscription = WAWService().subscribeToBicycleServiceLocation { [unowned self] (locations) in
            self.serviceLocations = locations
            self.tableview.reloadData()
        }
        
        if let idx = idxOfViewControllerToRemove {
            navigationController?.viewControllers.remove(at: idx)
            idxOfViewControllerToRemove = nil
        }
        
        
        self.title = " " // for empty back button
    }
    
    override func viewWillAppear(_ animated: Bool) {
         navigationController?.setNavigationBarHidden(true, animated: true)
    }
    
    private func sortLocations() {
        guard let locations = serviceLocations, !locations.isEmpty else {
            sectionedLocations.removeAll()
            return
        }
        let propertyLocation = CLLocation(latitude: selectedProperty.coordinate.latitude, longitude: selectedProperty.coordinate.longitude)
        let sortedLocations = locations.sorted { (lhs, rhs) -> Bool in
            let lDist = lhs.location.distance(from: propertyLocation)
            let rDist = rhs.location.distance(from: propertyLocation)
            return lDist < rDist
        }
        let closeLimitInMeters : CLLocationDistance = 2000 // 2km
        var closeLocations = [WAWBicycleServiceLocation]()
        var notCloseLocations = [WAWBicycleServiceLocation]()
        for serviceLocation in sortedLocations {
            if serviceLocation.location.distance(from: propertyLocation) < closeLimitInMeters {
                closeLocations.append(serviceLocation)
            } else if (closeLocations.isEmpty) {
                closeLocations.append(serviceLocation)
            } else {
                notCloseLocations.append(serviceLocation)
            }
        }
        sectionedLocations.removeAll()
        sectionedLocations.append(closeLocations)
        sectionedLocations.append(notCloseLocations)
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return sectionedLocations[section].count
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return sectionedLocations.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "BicycleServiceLocationCell", for: indexPath) as! BicycleServiceLocationCell
        cell.setup(with: sectionedLocations[indexPath.section][indexPath.row])
        cell.topView.isHidden = indexPath.row > 0

        return cell
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let view = tableView.dequeueReusableHeaderFooterView(withIdentifier:
                    "sectionHeader") as! CustomHeader
        if section == 0 {
            
            view.title.text = "Närmast dig (" + (model.getFavorites().first?.address ?? "") + ")"
        } else {
            view.title.text = "Övriga ställen"
        }

        return view
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let vc = segue.destination as? WebVC, let idx = tableview.indexPathForSelectedRow {
            let location = sectionedLocations[idx.section][idx.row]
            vc.url = URL(string: location.url)
            vc.navTitle = location.name
            vc.showActivityIndicator = true
            vc.adjustContentWidth = false
        }
    }
    
    @IBAction func goBack(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    class CustomHeader: UITableViewHeaderFooterView {
        let title = UILabel()

        override init(reuseIdentifier: String?) {
            super.init(reuseIdentifier: reuseIdentifier)
            configureContents()
        }
        
        required init?(coder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
        
        func configureContents() {
            title.translatesAutoresizingMaskIntoConstraints = false

            title.translatesAutoresizingMaskIntoConstraints = false
            title.font = WAWFonts.bold(size: 18)
            title.minimumScaleFactor = 0.7
            title.adjustsFontSizeToFitWidth = true
            
            title.textColor = .black
            
            contentView.addSubview(title)

            NSLayoutConstraint.activate([
                title.leadingAnchor.constraint(equalTo: contentView.leadingAnchor,
                       constant: 21),
                title.trailingAnchor.constraint(equalTo:
                    contentView.trailingAnchor, constant: -21),
                title.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -9)
            ])
        }
    }

}


class BicycleServiceLocationCell : UITableViewCell {
    
    @IBOutlet weak var location: UILabel!
    @IBOutlet weak var date: UILabel!
    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var locationStackView: UIStackView!
    
    var serviceLocation : WAWBicycleServiceLocation?
    
    override func awakeFromNib() {
        super.awakeFromNib()
         locationStackView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(BicycleServiceLocationCell.didTapLocationView)))
    }
    
    func setup(with location : WAWBicycleServiceLocation) {
        serviceLocation = location
        self.location.text = location.address
        var dateString = "\(DateUtil.dateMonthFormatter.string(from: location.startDate).lowercased().replacingOccurrences(of: ".", with: ""))"
        if location.spanOverMultipleDays {
            dateString += " - \(DateUtil.dateMonthFormatter.string(from: location.endDate).lowercased().replacingOccurrences(of: ".", with: ""))"
        }
        self.date.text = dateString
    }
    
    @objc func didTapLocationView() {
        guard let serviceLocation = serviceLocation else {
            return
        }
        let addressDict = [CNPostalAddressStreetKey: serviceLocation.address]
        let placemark = MKPlacemark(coordinate: serviceLocation.location.coordinate, addressDictionary: addressDict)
        
        let sfsStore = MKMapItem(placemark: placemark)
        let launchOptions = [String: Any]()
        sfsStore.name = serviceLocation.name
        
        sfsStore.openInMaps(launchOptions: launchOptions)
    }
}

