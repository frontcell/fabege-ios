//
//  Property.swift
//  fabege
//
//  Created by Patrik Billgert on 02/04/15.
//  Copyright (c) 2015 Frontcell AB. All rights reserved.
//
/*
 <PropertyId>13</PropertyId>
 <SourceId>111</SourceId>
 <DestinationId xsi:nil="true" />
 <Name>- Fastigheten</Name>
 <Descr>Bacchus 3</Descr>
 <Longitude>0</Longitude>
 <Latitude>0</Latitude>*/

import UIKit
import CoreLocation
import SWXMLHash

class Property: NSObject, NSCoding {
    
    var address: String
    var coordinate: CLLocationCoordinate2D
    var name: String
    var propertyId = Int.min
    var sourceId: Int
    var date = Date(timeIntervalSince1970: 0)
    
    init(element: XMLIndexer) {
        address = element["Name"].element!.text
        name = element["Descr"].element!.text
        sourceId = Int(element["SourceId"].element!.text) ?? -1
        
        if let longitude = element["Longitude"].element?.text, let latitude = element["Latitude"].element?.text {
            coordinate = CLLocationCoordinate2DMake((latitude as NSString).doubleValue, (longitude as NSString).doubleValue)
        } else {
            coordinate = kCLLocationCoordinate2DInvalid
        }
        if let value = element["PropertyId"].element?.text {
            propertyId = Int(value)!
        }
    }
    
    func encode(with coder: NSCoder) {
        coder.encode(address, forKey: "address")
        coder.encode(coordinate.latitude, forKey: "coordinate.lat")
        coder.encode(coordinate.longitude, forKey: "coordinate.lon")
        coder.encode(name, forKey: "name")
        coder.encode(propertyId, forKey: "propertyId")
        coder.encode(sourceId, forKey: "sourceId")
        coder.encode(date, forKey: "date")
    }
    
    required init?(coder: NSCoder) {
        address = coder.decodeObject(forKey: "address") as! String
        let lat = coder.decodeDouble(forKey: "coordinate.lat")
        let lon = coder.decodeDouble(forKey: "coordinate.lon")
        coordinate = CLLocationCoordinate2D(latitude: lat, longitude: lon)
        name = coder.decodeObject(forKey: "name") as! String
        propertyId = coder.decodeInteger(forKey: "propertyId")
        sourceId = coder.decodeInteger(forKey: "sourceId")
        if let date = coder.decodeObject(forKey: "date") as? Date {
            self.date = date
        }
    }
    
    
}
