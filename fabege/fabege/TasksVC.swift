//
//  MattersVC.swift
//  fabege
//
//  Created by Patrik Billgert on 18/03/15.
//  Copyright (c) 2015 Frontcell AB. All rights reserved.
//

import UIKit


class TasksVC: UIViewController, UITableViewDelegate, UITableViewDataSource, TaskCellDelegate {

    @IBOutlet weak var tableView: UITableView!
    
    var tasks: [Task] = []
    var prototypeCell = TaskCell()

    
    
    // MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()

        navigationItem.title = "Pågående arbeten"
        
        if (NSFoundationVersionNumber > NSFoundationVersionNumber_iOS_7_1) {
            tableView.estimatedRowHeight = 78
            tableView.rowHeight = UITableView.automaticDimension
        }
        
        tableView.contentInset = UIEdgeInsets.init(top: 30, left: 0, bottom: 0, right: 0)
        
        prototypeCell = tableView.dequeueReusableCell(withIdentifier: "TASK_CELL") as! TaskCell
        
        // TODO: Track the user action that is important for you.
    }
     
     override var preferredStatusBarStyle: UIStatusBarStyle {
          return .lightContent
     }
     
     override func viewWillAppear(_ animated: Bool) {
          setNeedsStatusBarAppearanceUpdate()
           navigationController?.setNavigationBarHidden(false, animated: true)
     }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    // MARK: UITableViewDataSource
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tasks.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "TASK_CELL", for: indexPath) as! TaskCell
        
        cell.delegate = self
        configureCell(cell, indexPath: indexPath)
        
        return cell
    }
    
    

    // MARK: UITableViewDelegate
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        configureCell(prototypeCell, indexPath: indexPath)
        prototypeCell.layoutIfNeeded()
        
        let size = prototypeCell.contentView.systemLayoutSizeFitting(UIView.layoutFittingCompressedSize) as CGSize
        return size.height;
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if NSFoundationVersionNumber > NSFoundationVersionNumber_iOS_7_1 {
            return UITableView.automaticDimension
        }
        
        configureCell(prototypeCell, indexPath: indexPath)
        prototypeCell.layoutIfNeeded()

        let size = prototypeCell.contentView.systemLayoutSizeFitting(UIView.layoutFittingCompressedSize) as CGSize
        return size.height;
    }
    
    
    
    // MARK: TaskCellDelegate
    
    func clickedURL(_ url: String) {
        if url.hasPrefix("http://") {
            if let link = URL(string: url) {
                UIApplication.shared.openURL(link)
            }
        } else {
            let url = "http://" + url
            if let link = URL(string: url) {
                UIApplication.shared.openURL(link)
            }
        }
    }
    
    
    
    // MARK: Other
    
    func configureCell(_ cell: UITableViewCell, indexPath: IndexPath) {
        if cell.isKind(of: TaskCell.self) {
            let taskCell = cell as! TaskCell
            
            let task = tasks[(indexPath as NSIndexPath).row]
            
            taskCell.titleLabel.text = task.title
            taskCell.infoLabel.text = task.information.stripHTML()
            if let date = task.date {
                taskCell.dateLabel.text = date
                taskCell.dateLabel.isHidden = false
            } else {
                taskCell.dateLabel.isHidden = true
            }
        }
    }
}



extension String {
    func stripHTML() -> String {
        let encodedData = self.data(using: String.Encoding.utf8)!
        let attributedOptions = [convertFromNSAttributedStringDocumentAttributeKey(NSAttributedString.DocumentAttributeKey.documentType): convertFromNSAttributedStringDocumentType(NSAttributedString.DocumentType.html)]
        
        do {
            let attributedString = try NSAttributedString(data: encodedData, options: convertToNSAttributedStringDocumentReadingOptionKeyDictionary(attributedOptions), documentAttributes: nil)
            return attributedString.string
        } catch {
            return ""
        }

    }
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromNSAttributedStringDocumentAttributeKey(_ input: NSAttributedString.DocumentAttributeKey) -> String {
	return input.rawValue
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromNSAttributedStringDocumentType(_ input: NSAttributedString.DocumentType) -> String {
	return input.rawValue
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertToNSAttributedStringDocumentReadingOptionKeyDictionary(_ input: [String: Any]) -> [NSAttributedString.DocumentReadingOptionKey: Any] {
	return Dictionary(uniqueKeysWithValues: input.map { key, value in (NSAttributedString.DocumentReadingOptionKey(rawValue: key), value)})
}
