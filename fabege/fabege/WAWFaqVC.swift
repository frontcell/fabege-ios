//
//  WAWFaqVC.swift
//  fabege
//
//  Created by Oscar Kockum on 2019-01-22.
//  Copyright © 2019 Frontcell AB. All rights reserved.
//

import UIKit

class WAWFaqVC : UIViewController, UITableViewDataSource, UITableViewDelegate {
     @IBOutlet weak var tableView: UITableView!
     
     var faq : [WAWFAQ]?
     var selectedFaqIdx : IndexPath?
     
     var subscription : WAWService.Subscription?
     
     deinit {
          subscription?.detach()
     }
     
     override func viewDidLoad() {
          let service = WAWService()
          subscription = service.subscribeToFAQ { [weak self] (faq) in
               self?.updateView(faq: faq)
          }
          tableView.tableFooterView = UIView()
     }
     
     override func viewDidAppear(_ animated: Bool) {
          FabegeAnalytics.trackPageView(name: "WAW_FAQ")
     }
     
     func updateView(faq : [WAWFAQ]?) {
          self.faq = faq
          tableView.reloadData()
     }
     
     func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
          return faq?.count ?? 0
     }
     
     func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
          let cell = tableView.dequeueReusableCell(withIdentifier: "WAWFaqCell", for: indexPath) as! WAWFaqCell
          cell.topView.isHidden = indexPath.row > 0
          cell.setFAQ(faq: faq![indexPath.row], selected:indexPath == selectedFaqIdx)
          return cell
     }
     
     func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
          var rowsToReload = [IndexPath]()
          if let idx = selectedFaqIdx {
               rowsToReload.append(idx)
          }
          if selectedFaqIdx != indexPath {
               selectedFaqIdx = indexPath
          } else {
               selectedFaqIdx = nil

          }
          rowsToReload.append(indexPath)
          tableView.reloadRows(at: rowsToReload, with: .automatic)
     }
     
     @IBAction func actionBack(_ sender: Any) {
          navigationController?.popViewController(animated: true)
     }
     
     
}

class WAWFaqCell : UITableViewCell {
     @IBOutlet weak var stackview: UIStackView!
     @IBOutlet weak var faqTitle: UILabel!
    @IBOutlet weak var arrow: UIImageView!
     @IBOutlet weak var topView: UIView!
     
     var faq : WAWFAQ?
     
     func setFAQ(faq : WAWFAQ, selected : Bool) {
          self.faq = faq
          faqTitle.text = faq.title
          stackview.arrangedSubviews.forEach { $0.removeFromSuperview() }
          if selected {
               for (idx, question) in faq.questions.enumerated() {
                    let label = UILabel()
                    label.font = WAWFonts.regular(size: 15)
                    label.tag = idx
                    stackview.addArrangedSubview(label)
                    label.text = question.question
                    label.underline(color: UIColor.waw.purpleColor)
                    let answersLabel = UITextView()
                    answersLabel.font = WAWFonts.regular(size: 13)
                    answersLabel.tag = idx
                    answersLabel.isHidden = false
                    answersLabel.formatHTML(text: question.answer)
                    answersLabel.isScrollEnabled = false
                    answersLabel.dataDetectorTypes = [.phoneNumber, .link]
                    answersLabel.textContainerInset = UIEdgeInsets.zero
                    answersLabel.textContainer.lineFragmentPadding = 0
                    answersLabel.backgroundColor = UIColor.clear
                    answersLabel.isEditable = false
                    stackview.addArrangedSubview(answersLabel)
               }
          }
          arrow.transform = CGAffineTransform(rotationAngle: selected ? CGFloat(-Double.pi / 2) : CGFloat(Double.pi / 2));

     }
     
     /*@objc func didTapLabel(tap : UITapGestureRecognizer) {
          if let idx = tap.view?.tag, let faq = faq, idx < faq.questions.count {
               stackview.arrangedSubviews.forEach { (view) in
                    if view.tag == idx {
               view.isHidden = false
                    }
               }
               
          }
     }*/
     
}


