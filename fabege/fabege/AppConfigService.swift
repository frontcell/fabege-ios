//
//  AppConfigService.swift
//  fabege
//
//  Created by Oscar Kockum on 2020-01-07.
//  Copyright © 2020 Frontcell AB. All rights reserved.
//

import Foundation


class AppConfigService {
    
    static let shared = AppConfigService()
    
    private var subscription : WAWService.Subscription?
    private var config : WAWAppConfig?
    
    private init() {
        
    }
    
    deinit {
        subscription?.detach()
    }
    
    func configure() {
        guard subscription == nil else {
            return
        }
        subscription?.detach()
        subscription = WAWService().subscribeToAppConfig(changes: { [unowned self] (config) in
            self.config = config
        })
    }
    
    func validateApp(completion : @escaping ((Bool) -> ())) {
        if let config = config {
            completion(config.isValid())
        } else {
            WAWService().fetchAppConfig { (config) in
                self.config = config
                completion(config?.isValid() ?? false)
            }
        }
    }
}
