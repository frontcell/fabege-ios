//
//  ErrorDialogController.swift
//  fabege
//
//  Created by Oscar Kockum on 2018-12-18.
//  Copyright © 2018 Frontcell AB. All rights reserved.
//

import UIKit

class DialogController: UIViewController {
    
    @IBOutlet weak var dialogView: UIView!
    @IBOutlet weak var highlightedTextLabel: UILabel!
    @IBOutlet weak var defaultTextLabel : UILabel!
    @IBOutlet weak var titleLabel : UILabel!
    @IBOutlet weak var closeButton: UIButton!
    @IBOutlet weak var errorImage: UIImageView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var addToCalendarButton: UIButton!
    @IBOutlet weak var yesButton: UIButton!
    @IBOutlet weak var openAppstoreButton: UIButton!
    @IBOutlet weak var noButton: UIButton!
    @IBOutlet weak var customButton: UIButton!
    @IBOutlet weak var doorIcon: UIImageView!
    
    var onYesActionClicked: (() -> Void)?
    var onNoActionClicked: (() -> Void)?
    var onAddCalendarClicked: (() -> Void)?
    var onAppstoreClicked: (() -> Void)?
    var onCustomButtonClicked: (() -> Void)?
    
    var dTitle : String?
    var highlightedText : String?
    var defaultText : String?
    var lightMode = false
    
    var type = DialogType.noAction
    
    enum DialogType: Equatable {
        case noAction
        case error
        case loading
        case addCalendar
        case yesNo
        case openDoor
        case appStore
        case customButton(String)
        
        public static func ==(lhs: DialogType, rhs:DialogType) -> Bool {
            
            switch (lhs,rhs) {
            case (.noAction, .noAction):
                return true
            case (.error, .error):
                return true
            case (.loading, .loading):
                return true
            case (.addCalendar, .addCalendar):
                return true
            case (.yesNo, .yesNo):
                return true
            case (.openDoor, .openDoor):
                return true
            case (.appStore, .appStore):
                return true
            case (.customButton, .customButton):
                return true
            default:
                return false
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        dialogView.outerShadow(show: true, animated: true)
        updateDialog(title: dTitle, highlightedText: highlightedText, defaultText: defaultText, lightMode: lightMode, dialogType: type)
        
        // Do any additional setup after loading the view.
    }
    
    func updateDialog(title : String?, highlightedText : String?, defaultText : String?, lightMode : Bool = false, dialogType : DialogType = .noAction) {
        titleLabel.text = title
        highlightedTextLabel.text = highlightedText
        defaultTextLabel.text = defaultText
        
        yesButton.underline(color: UIColor.waw.purpleColor)
        noButton.underline(color: UIColor.waw.purpleColor)
        openAppstoreButton.underline(color: UIColor.waw.purpleColor)
        
        if lightMode {
            dialogView.backgroundColor = UIColor.waw.backgroundColor
            closeButton.tintColor = UIColor.waw.purpleColor
            titleLabel.textColor = UIColor.waw.grayColor
            defaultTextLabel.textColor = UIColor.waw.purpleColor
            highlightedTextLabel.textColor = UIColor.waw.purpleColor
            
        } else {
            dialogView.backgroundColor = UIColor.waw.purpleColor
            closeButton.tintColor = UIColor.white
            titleLabel.textColor = UIColor.white
            defaultTextLabel.textColor = UIColor.white
            highlightedTextLabel.textColor = UIColor.white
        }
        
        defaultTextLabel.isHidden = defaultText == nil
        switch dialogType {
            case .loading:
                activityIndicator.startAnimating()
            case .customButton(let value):
                customButton.setTitle(value, for: .normal)
                customButton.underline(color: UIColor.waw.purpleColor)
            default:
                break
        }
        
        activityIndicator.isHidden = dialogType != .loading
        errorImage.isHidden = dialogType != .error
        addToCalendarButton.isHidden = dialogType != .addCalendar
        yesButton.isHidden = dialogType != .yesNo
        noButton.isHidden = dialogType != .yesNo
        openAppstoreButton.isHidden = dialogType != .appStore
        doorIcon.isHidden = dialogType != .openDoor
        customButton.isHidden = dialogType != .customButton("")
        
        closeButton.isHidden = dialogType == .yesNo || dialogType == .loading || dialogType == .appStore
    }
    
    class func display(from vc : UIViewController, title : String?, highlightedText : String?, defaultText : String?, lightMode : Bool = false, dialogType : DialogType = .noAction) -> DialogController {
        let dialog = DialogController(nibName: "DialogViewController", bundle: nil)
        dialog.providesPresentationContextTransitionStyle = true
        dialog.definesPresentationContext = true
        dialog.modalPresentationStyle = .overCurrentContext
        dialog.modalTransitionStyle = .crossDissolve
        dialog.dTitle = title
        dialog.highlightedText = highlightedText
        dialog.defaultText = defaultText
        dialog.lightMode = lightMode
        dialog.type = dialogType
        //customAlert.delegate = self
        vc.present(dialog, animated: true, completion: nil)
        return dialog
    }
    
    @IBAction func actionAddCalendar(_ sender: Any) {
        onAddCalendarClicked?()
    }
    
    @IBAction func actionYes(_ sender: Any) {
        onYesActionClicked?()
    }
    
    @IBAction func actionOpenAppstore(_ sender: Any) {
        onAppstoreClicked?()
    }
    
    @IBAction func actionNo(_ sender: Any) {
        onNoActionClicked?()
    }
    
    func dismissDialog(animated : Bool = true, completion : (()->Void)?) {
        dismiss(animated: animated, completion: completion)
    }
    
    @IBAction func actionClose(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func actionCustomButton(_ sender: Any) {
        onCustomButtonClicked?()
    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
