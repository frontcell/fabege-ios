//
//  WAWModels.swift
//  fabege
//
//  Created by Oscar Kockum on 2018-12-18.
//  Copyright © 2018 Frontcell AB. All rights reserved.
//

import Foundation
import CoreLocation

protocol FirebaseDocument {
     var data : [String : Any] { get }
}



struct WAWBookingReference : Hashable {
     var date : String
     var slotIdx : Int
     var propertyId : String
     var add : Bool
     
     func hash(into hasher: inout Hasher) {
          hasher.combine(date)
          hasher.combine(slotIdx)
          hasher.combine(propertyId)
     }
     
     var identifier : String {
          return "\(propertyId)/\(date)/\(slotIdx)"
     }
}

extension WAWBookingReference {
     init(date : String, slotIdx : Int, propertyId: String) {
          self.date = date
          self.slotIdx = slotIdx
          self.propertyId = propertyId
          self.add = true
     }
}

struct WAWUser : FirebaseDocument, Codable {
     
     struct AccessKey : FirebaseDocument, Codable {
          var path : String
          var accessId : String
          
          init?(data : [String : Any]) {
               guard let path = data["path"] as? String,
                    let id = data["id"] as? String else {
                    return nil
               }
               self.path = path
               self.accessId = id
          }
          
          var data : [String : Any] {
               var data = [String : Any]()
               data["path"] = path
               data["id"] = accessId
               return data
          }
          
          struct PathComponents {
               var propertyId : String
               var dateKey : String
               var slotIdx : Int
          }
          
          func components() -> PathComponents? {
               var pathList = path.split(separator: "/")
               if pathList.count == 3 {
                    let comps = PathComponents(propertyId: String(pathList[0]), dateKey: String(pathList[1]), slotIdx: Int(pathList[2])!)
                    return comps
               } else {
                    return nil
               }
          }

     }
     
     struct ErrorLog : FirebaseDocument, Codable {
          var errorMessage : String
          var path : String
          
          init(data : [String : Any]) {
               errorMessage = data["errorMessage"] as! String
               path = data["path"] as! String
          }
          
          var data : [String : Any] {
               var data = [String : Any]()
               data["path"] = path
               data["errorMessage"] = errorMessage
               return data
          }
     }
     
     var id : String
     var email : String
     var firstname : String?
     var lastname : String?
     var parakeyUserId : String?
     var deviceToken : String?
     var accessKeys = [AccessKey]()
     var errorLogs = [ErrorLog]()
     
     var parakeyPassword : String {
          //return "\(id)_321_fabege"
          return "\(email)_321_fabege"
     }
     
     
     init(id: String, data : [String : Any]) {
          self.id = id
          email = data["email"] as! String
          firstname = data["firstname"] as? String
          lastname = data["lastname"] as? String
          
          if let items = data["accessKeys"] as? [[String : Any]] {
               items.forEach { (item) in
                    if let key = AccessKey(data: item) {
                         accessKeys.append(key)
                    }
               }
          }
          
          if let items = data["failedAccessKeys"] as? [[String : Any]] {
               items.forEach { (item) in
                    errorLogs.append(ErrorLog(data: item))
               }
          }
          
          parakeyUserId = data["parakeyUserId"] as? String
          deviceToken = data["deviceToken"] as? String
     }
     
     // returns true if invalid keys
     mutating func removeInvalidKeys() -> Bool {
          var validKeys = [AccessKey]()
          let currentDateKey = Date().dateKey()
          for key in accessKeys {
               if let comps = key.components(), comps.dateKey >= currentDateKey {
                    validKeys.append(key)
               }
          }
          let changed = validKeys.count != accessKeys.count
          accessKeys = validKeys
          return changed
     }
     
     func hasValidAccessForNow() -> Bool {
          let currentDateKey = Date().dateKey()
          for key in accessKeys {
               if let comps = key.components(), comps.dateKey == currentDateKey {
                    return true
               }
          }
          return false
     }

     var data : [String : Any] {
          var data = [String : Any]()
          data["email"] = email
          data["firstname"] = firstname
          data["lastname"] = lastname
          data["accessKeys"] =  accessKeys.map { (item) -> [String : Any] in
               return item.data
          }
          data["failedAccessKeys"] = errorLogs.map { (item) -> [String : Any] in
               return item.data
          }
          if let userId = parakeyUserId {
               data["parakeyUserId"] = userId
          }
          if let token = deviceToken {
               data["deviceToken"] = token
          }
          
          return data
     }
}

struct WAWServices {
    
    struct BicycleService {
        struct Intro {
            var buttonTitle : String
            var imageUrl : String
            var info : String
            
            init(data : [String : Any]) {
                buttonTitle = data["buttonTitle"] as! String
                imageUrl = data["imageUrl"] as! String
                info = data["info"] as! String
            }
        }
        
        var buttonTitle : String
        var serviceDescription : String
        var endDate : Date
        var startDate : Date
        var iconUrl : String
        var intro : Intro
        var enabled : Bool
        
        
        init(data : [String : Any]) {
            enabled = data["enabled"] as! Bool
            buttonTitle = data["buttonTitle"] as! String
            startDate = DateUtil.dateKeyFormatter.date(from: data["startDate"] as! String)!
            endDate = DateUtil.dateKeyFormatter.date(from: data["endDate"] as! String)!
            iconUrl = data["iconUrl"] as! String
            serviceDescription = data["description"] as! String
            intro = Intro(data: data["intro"] as! [String : Any])
        }
    }
    
    var bicycleService : BicycleService?
    
    init(data : [String : Any]) {
        if let data = data["bicycle"] as? [String : Any] {
            bicycleService = BicycleService(data:data)
        }
        
    }
}

struct WAWBicycleServiceLocation {
    var address : String
    var endDate : Date
    var startDate : Date
    var spanOverMultipleDays : Bool
    var name : String
    var url : String
    var location : CLLocation
    
    
    init?(data : [String : Any]) {
        guard let address = data["address"] as? String,
        let name = data["name"] as? String,
        let endDateStr = data["endDate"] as? String,
        let startDateStr = data["startDate"] as? String,
        let endDate = DateUtil.dateKeyFormatter.date(from: endDateStr)?.endOfDay(),
        let startDate = DateUtil.dateKeyFormatter.date(from: startDateStr),
        let published = data["published"] as? Bool,
        published == true,
        let coords = data["coordinates"] as? [String : Any],
        let lat = coords["lat"] as? Double,
        let lon = coords["lon"] as? Double,
        let url = data["url"] as? String else {
            print(data)
            return nil
        }
        self.location = CLLocation(latitude: lat, longitude: lon)
        self.address = address
        self.endDate = endDate
        self.startDate = startDate
        self.url = url
        self.name = name
        self.spanOverMultipleDays = endDateStr != startDateStr
        
    }
}

struct WAWAppConfig : Codable {
     var minAllowedVersion : Int
     
     init(data : [String : Any]) {
          minAllowedVersion = data["minAllowedVersion"] as! Int
     }
    
    func isValid() -> Bool {
        let build = Bundle.main.infoDictionary?["CFBundleVersion"] as? String ?? "0"
        let buildNumber = Int(build) ?? 0
        return buildNumber >= minAllowedVersion
    }
}

struct WAWFAQ : Codable {
     
     struct Question : Codable {
          var question : String
          var answer : String
          
          init(data : [String : Any]) {
               question = data["title"] as! String
               answer = data["text"] as! String
          }
     }
     
     var questions = [Question]()
     var title : String
     
     init(data : [String : Any]) {
          title = data["name"] as! String
          if let qs = data["questions"] as? [[String : Any]] {
               qs.forEach { (item) in
                    questions.append(Question(data: item))
               }
          }
     }
}

struct WAWTimeSlot : FirebaseDocument {
     
     enum Status : String {
          case available = "available"
          case closed = "closed"
          case holiday = "holiday"
          
          static func parse(value : String) -> Status {
               switch value.lowercased() {
               case available.rawValue:
                    return .available
               case closed.rawValue:
                    return .closed
               case holiday.rawValue:
                    return .holiday
               default:
                    return .closed
               }
          }
     }
     
     var ids : [String]
     var status : Status
     
     init(data : [String : Any]) {
          status = Status.parse(value: data["status"] as! String)
          ids = data["ids"] as! [String]
     }
     
     var data : [String : Any] {
          var data = [String : Any]()
          data["status"] = status.rawValue
          data["ids"] = ids
          return data
     }
}

struct WAWScheduleContainer {
     var propertyId : String
     var schedules : [WAWSchedule]
     
     var today : WAWSchedule? {
          get {
               let today = Date().dateKey()
               return schedules.first(where: { (schedule) -> Bool in
                    return schedule.dateString == today
               })
          }
     }
}

struct WAWSchedule : FirebaseDocument {
     var timeSlots : [WAWTimeSlot]
     var dateString : String
     
     static let df : DateFormatter = {
          let df = DateFormatter()
          df.dateFormat = "yyyyMMdd"
          return df
     }()
     
     init(date : String, data : [String : Any]) {
          dateString = date
          timeSlots = [WAWTimeSlot]()
          if let slots = data["slots"] as? [[String : Any]] {
               slots.forEach { (item) in
                    timeSlots.append(WAWTimeSlot(data: item))
               }
          }
     }
     
     var data : [String : Any] {
          var data = [String : Any]()
          data["slots"] = timeSlots.map { (slot) -> [String : Any] in
                    return slot.data
          }
          return data
     }
     
     func loadDate() -> Date {
          return WAWSchedule.df.date(from: dateString)!
     }
}

struct WAWAlert : Codable {
     
     
     var id : String
     var title : String
     var message : String
     var propertyId : String?
     var publishAt : TimeInterval
     var unpublishAt : TimeInterval
     
     static let OneYearInSeconds : TimeInterval = 31556926
     
     init?(id: String, data : [String : Any]) {
          guard let title = data["title"] as? String,
          let message = data["message"] as? String else {
               return nil
          }
          
          self.title = title
          self.id = id
          self.message = message
          if let id = data["propertyId"] as? String, !id.isEmpty {
               self.propertyId = id
          }
          if let unpublishAtString = data["unpublishAt"] as? String,
              let date = DateUtil.dateTimeTimeFormatter.date(from: unpublishAtString) {
               self.unpublishAt = date.timeIntervalSince1970
          } else {
               self.unpublishAt = Date(timeIntervalSinceNow: WAWAlert.OneYearInSeconds).timeIntervalSince1970
          }
          
          if let publishAtString = data["publishAt"] as? String,
               let date = DateUtil.dateTimeTimeFormatter.date(from: publishAtString) {
               self.publishAt = date.timeIntervalSince1970
          } else {
               self.publishAt = 0
          }
          
     }
}


struct WAWProperty: Codable {
     
     struct Capacity : Codable {
          var conferenceRooms : Int
          var dropIns : Int
          var phoneRooms : Int
          var seats : Int
          
          init(data : [String : Any]) {
               conferenceRooms = data["conferenceRooms"] as! Int
               dropIns = data["dropIns"] as! Int
               phoneRooms = data["phoneRooms"] as! Int
               seats = data["seats"] as! Int
          }
     }
     
     struct Contact : Codable {
          var name : String
          var imageUrl : String
          var email : String
          var tel : String
          var title : String
          
          init(data : [String : Any]) {
               name = data["name"] as! String
               imageUrl = data["imageUrl"] as! String
               email = data["email"] as! String
               tel = data["tel"] as! String
               title = data["title"] as! String
          }
     }
     
     struct Wifi : Codable {
          var info : String
          var username : String?
          var password : String?
          
          init(data : [String : Any]) {
               info = data["info"] as! String
               username = data["username"] as? String
               password = data["password"] as? String
          }
     }

     struct OpeningHours : Codable {
          
          var start : String
          var end : String
          
          init(data : [String : Any]) {
               start = data["start"] as! String
               end = data["end"] as! String
          }
     }
     
     struct Facilities : Codable {
          var coffee : String
          var conference : String
          var printers : String
          var wifi : Wifi
          
          init(data : [String : Any]) {
               coffee = data["coffee"] as! String
               conference = data["conference"] as! String
               printers = data["printers"] as! String
               wifi = Wifi(data: data["wifi"] as! [String : Any])
          }
     }
     
     var name : String
     var id : String
     var info : String
     var iconUrl : String
     var imageUrl : String
     var address : String
     var capacity : Capacity
     var contact : Contact
     var facilities : Facilities
     var lat : Double
     var lon : Double
     var goodToKnow : String
     var openingHours = [OpeningHours]()
     var visible = true
     
     init?(id : String, data : [String : Any]) {
          guard data["openingHours"] != nil else {
               return nil
          }
          self.id = id
          name = data["name"] as! String
          info = data["desc"] as! String
          address = data["address"] as! String
          iconUrl = data["iconUrl"] as! String
          imageUrl = data["imageUrl"] as! String
          goodToKnow = data["goodToKnow"] as! String
          capacity = Capacity(data: data["capacity"] as! [String : Any])
          contact = Contact(data: data["contact"] as! [String : Any])
          facilities = Facilities(data: data["facilities"] as! [String : Any])
          visible = data["visible"] as? Bool ?? true
          
          if let hours = data["openingHours"] as? [[String : Any]] {
               for item in hours {
                    openingHours.append(OpeningHours(data: item))
               }
          }
          
          let coords = data["coordinates"] as! [String : Any]
          lat = coords["lat"] as! Double
          lon = coords["lon"] as! Double
          
          
     }
     
}

extension WAWProperty {
     
     func availableSeats(dateKey : String) -> Int {
          var seats = capacity.seats
          let today = Date().dateKey()
          if today != dateKey {
               seats -= capacity.dropIns
          }
          return seats
     }
     
}

extension WAWBookingReference : Comparable {
     static func < (lhs: WAWBookingReference, rhs: WAWBookingReference) -> Bool {
          return "\(lhs.date)\(lhs.slotIdx)".compare("\(rhs.date)\(rhs.slotIdx)") == .orderedAscending
     }
     
     
}

