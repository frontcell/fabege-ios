//
//  FavoriteCell.swift
//  fabege
//
//  Created by Patrik Billgert on 23/03/15.
//  Copyright (c) 2015 Frontcell AB. All rights reserved.
//

import UIKit

class FavoriteCell: UICollectionViewCell {
    
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
}
