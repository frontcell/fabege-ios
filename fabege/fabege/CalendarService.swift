//
//  CalendarService.swift
//  fabege
//
//  Created by Oscar Kockum on 2019-01-21.
//  Copyright © 2019 Frontcell AB. All rights reserved.
//

import Foundation
import EventKit

protocol CalendarItem {
     var id : String { get }
}

class CalendarService {
     
     struct Event : CalendarItem {
          var title : String
          var startDate : Date
          var endDate : Date
          var description : String
          var latitude : Double
          var longitude : Double
          var id : String
     }
     
     class func removeCalendarItem(items : [CalendarItem]) {
          DispatchQueue.global(qos: .background).async { () -> Void in
               let eventStore = EKEventStore()
               eventStore.requestAccess(to: .event, completion: { (granted, error) in
                    guard error == nil, granted else {
                         return
                    }
                    for item in items {
                         if let eventIdentifier = UserDefaults.standard.object(forKey: item.id) as? String {
                              if let event = eventStore.event(withIdentifier: eventIdentifier) {
                                   try? eventStore.remove(event, span: EKSpan.thisEvent)
                              }
                         }
                         UserDefaults.standard.removeObject(forKey: item.id)
                    }
               })
          }
          
     }
     
     class func addEvents(events : [Event], completion: ((Bool) -> Void)? = nil) {
          DispatchQueue.global(qos: .background).async { () -> Void in
               let eventStore = EKEventStore()
               eventStore.requestAccess(to: .event, completion: { (granted, error) in
                    guard error == nil, granted else {
                         DispatchQueue.main.async {
                              completion?(true)
                         }
                         return
                    }
                    for aEvent in events {
                         let event = EKEvent(eventStore: eventStore)
                         event.title = aEvent.title
                         event.startDate = aEvent.startDate
                         event.endDate = aEvent.endDate
                          //event.notes = aEvent.description.htmlStripped()
                         let structuredLocation = EKStructuredLocation(title: aEvent.title)
                         structuredLocation.geoLocation = CLLocation(latitude: aEvent.latitude, longitude: aEvent.longitude)
                         event.structuredLocation = structuredLocation
                         event.calendar = eventStore.defaultCalendarForNewEvents
                         do {
                              try eventStore.save(event, span: .thisEvent)
                              UserDefaults.standard.set(event.eventIdentifier, forKey: aEvent.id)
                         } catch {
                              DispatchQueue.main.async {
                                   completion?(false)
                              }
                              return
                         }
                    }
                    DispatchQueue.main.async {
                         completion?(true)
                    }
               })
          }
     }
     
}
