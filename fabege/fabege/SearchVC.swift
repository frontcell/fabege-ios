//
//  SearchVC.swift
//  fabege
//
//  Created by Patrik Billgert on 18/03/15.
//  Copyright (c) 2015 Frontcell AB. All rights reserved.
//

import UIKit
import CoreLocation

protocol SearchVCDelegate : class {
     func didSelectProperty(_ property : Property)
     func didCloseView()
}

class SearchVC: UIViewController, UISearchBarDelegate, UITableViewDataSource, UITableViewDelegate, SegmentCellDelegate {
     
     @IBOutlet weak var propertiesTableView: UITableView!
     @IBOutlet weak var doneButton: UIButton!
     @IBOutlet weak var infoLabel: UILabel!
     @IBOutlet weak var doneButtonHeightConstraint: NSLayoutConstraint!
     @IBOutlet weak var headerViewHeightConstraint: NSLayoutConstraint!
     @IBOutlet weak var infoLabelTopConstraint: NSLayoutConstraint!
     @IBOutlet weak var infoLabelBottomConstraint: NSLayoutConstraint!
     @IBOutlet weak var tableView: UITableView!
     @IBOutlet weak var doneButtonTopConstraint: NSLayoutConstraint!
    
     var searchActive = false
     var firstLaunch = false
     var distanceSort  = true
     var changeProperty = false
     weak var delegate : SearchVCDelegate?
     var favoriteProperties: [Property] = []
     var properties: [Property] = []
     var filteredProperties:[Property] = []
     var searchBar: UISearchBar!
     var selectedProperty : Property?
     var disableExitUntilSelected = false
     
     // MARK: - Lifecycle
     
     override func viewDidLoad() {
          super.viewDidLoad()
        
          navigationItem.rightBarButtonItem?.tintColor = UIColor.white
        
        navigationItem.rightBarButtonItem?.setTitleTextAttributes([.font:UIFont(name: "HelveticaNeue", size: 14)!, NSAttributedString.Key.foregroundColor: UIColor.white],for: .normal)
        navigationItem.rightBarButtonItem?.setTitleTextAttributes([.font:UIFont(name: "HelveticaNeue", size: 14)!, NSAttributedString.Key.foregroundColor: UIColor.lightGray],for: .disabled)
          
          var widthReduction: CGFloat = CGFloat(77)
          if UIScreen.main.bounds.size.height > 667 {
               widthReduction = 87
          }
          
          searchBar = UISearchBar(frame: CGRect(x: 0, y: 0, width: view.frame.width - widthReduction, height: 44))
          searchBar.tintColor = UIColor.white
          searchBar.barTintColor = UIColor.white
          searchBar.placeholder = "Sök adress"
          searchBar.delegate = self
          searchBar.barStyle = .black
        
          var searchField : UITextField? = nil
          if #available(iOS 13.0, *) {
            searchField = searchBar.searchTextField
          } else {

          }
          searchField?.tintColor = .white

          let textFieldInsideSearchBar = searchBar.value(forKey: "searchField") as? UITextField
          textFieldInsideSearchBar?.textColor = UIColor.lightGray
        
          let searchbarItem = UIBarButtonItem()
          searchbarItem.customView = searchBar
          navigationItem.leftBarButtonItem = searchbarItem

          
          doneButton.isEnabled = false
          let sortType: AnyObject? = UserDefaults.standard.object(forKey: "sortType") as AnyObject?
          if sortType != nil {
               distanceSort = sortType as! Bool
          }
        
          navigationItem.rightBarButtonItem?.isEnabled = !disableExitUntilSelected || selectedProperty != nil
        
          if changeProperty {
               infoLabel.text = ""
               headerViewHeightConstraint.constant = 0
               infoLabelTopConstraint.constant = 0;
               infoLabelBottomConstraint.constant = 0;
               doneButtonHeightConstraint.constant = 0
               doneButton.isHidden = true
          } else {
               infoLabel.text = "Välj dina fastigheter i listan nedan som du bl.a. vill få senaste status om eller registrera ett ärende."
               favoriteProperties = model.getFavorites()
            var height : CGFloat
            if Device.IS_IPHONE_X {
                height = 68
            } else {
                height = 44
            }
            doneButtonHeightConstraint.constant = height
          }
        
          properties = model.getProperties(distanceSort)
          
          let authstate = CLLocationManager.authorizationStatus()
          if authstate == .notDetermined || authstate == .denied {
               NotificationCenter.default.addObserver(self, selector: #selector(SearchVC.locationUpdated), name: NSNotification.Name(rawValue: NotificationLocationUpdates), object: nil)
          }
          
          let appDelegate = UIApplication.shared.delegate as! AppDelegate
          appDelegate.setupLocationManager()
          
          if firstLaunch {
               UIApplication.shared.setStatusBarHidden(false, with: .fade)
               navigationItem.rightBarButtonItem = nil
               searchBar.frame = CGRect(x: 0, y: 0, width: view.frame.width - 32, height: 44)
          }
          
          // TODO: Track the user action that is important for you.
     }
     
     override func viewWillDisappear(_ animated: Bool) {
          super.viewWillDisappear(animated)
          
          if searchBar != nil && searchBar.isFirstResponder {
               searchBar.resignFirstResponder()
          }
     }
     
     override var preferredStatusBarStyle: UIStatusBarStyle {
          return .default
     }
     
     override func viewWillAppear(_ animated: Bool) {
          navigationController?.setNavigationBarHidden(false, animated: true)
     }
     
     override func didReceiveMemoryWarning() {
          super.didReceiveMemoryWarning()
          // Dispose of any resources that can be recreated.
     }
     
     
     
     // MARK: UITableViewDataSource
     
     func numberOfSections(in tableView: UITableView) -> Int {
          if searchActive {
               return 1
          }
          
          if (favoriteProperties.count > 0) {
               return 2
          } else {
               return 1
          }
     }
     
     func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
          if searchActive {
               return filteredProperties.count
          }
          
          if (favoriteProperties.count > 0 && section == 0) {
               return favoriteProperties.count
          } else {
               return properties.count
          }
     }
     
     func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
          var identifier = String()
          
          if changeProperty == true {
               identifier = "PROPERTY_CHECK_CELL"
          } else {
               identifier = "PROPERTY_CELL"
          }
          
          let cell = tableView.dequeueReusableCell(withIdentifier: identifier, for: indexPath) as! PropertyCell
          
          var selected = Bool()
          if (favoriteProperties.count > 0 && (indexPath as NSIndexPath).section == 0 && !searchActive) {
               let property = favoriteProperties[(indexPath as NSIndexPath).row]
               cell.titleLabel.text = property.address
               cell.distanceLabel.text = ""
               selected = changeProperty ? property == selectedProperty : true
          } else {
               var property: Property!
               if searchActive {
                    property = filteredProperties[(indexPath as NSIndexPath).row]
               } else {
                    property = properties[(indexPath as NSIndexPath).row]
               }
               
               cell.titleLabel.text = property.address
               let appDelegate = UIApplication.shared.delegate as! AppDelegate
               if (appDelegate.lastLocation != nil) {
                    let distanceMeters = appDelegate.lastLocation.distance(
                         from: CLLocation(latitude: property.coordinate.latitude, longitude: property.coordinate.longitude))
                    
                    if distanceMeters >= 1000 {
                         cell.distanceLabel.text = String(format: "%.f km", distanceMeters / 1000.0)
                    } else {
                         cell.distanceLabel.text = String(format: "%.f m", distanceMeters)
                    }
                    
               } else {
                    cell.distanceLabel.text = ""
               }
               
               selected = changeProperty ? property == selectedProperty : favoriteProperties.contains(property)
          }
          
          var selectedImage = UIImage(named: "checked")
          var defaultImage = UIImage(named: "unchecked")
          if changeProperty == true {
               selectedImage = UIImage(named: "checkmark")!
               defaultImage = UIImage(named: "")
          }
          
          if selected == true {
               cell.checkImageView.image = selectedImage
          } else {
               cell.checkImageView.image = defaultImage
          }
          
          return cell
     }
     
     
     
     // MARK: UITableViewDelegate
     
     func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
          if searchActive {
               return 30
          }
          
          if favoriteProperties.count > 0 && section == 0 {
               return 30
          } else {
               return 44
          }
     }
     
     func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
          if (favoriteProperties.count > 0 && section == 0) || searchActive {
               let cell = tableView.dequeueReusableCell(withIdentifier: "SELECTED_HEADER_CELL") as! SelectedHeaderCell
               if searchActive {
                    if filteredProperties.count > 0 {
                         cell.titleLabel.text = NSString(format: "Resultat (%i)", filteredProperties.count) as String
                    } else {
                         cell.titleLabel.text = "Inga resultat"
                    }
               } else {
                    cell.titleLabel.text = "Favoriter"
               }
               return cell
          } else {
               let cell = tableView.dequeueReusableCell(withIdentifier: "SEGMENT_HEADER_CELL") as! SegmentHeaderCell
               cell.delegate = self
               // quick ugly fix
               cell.setSortMode(distanceSort)
               return cell
          }
     }
     
     func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
          if changeProperty {
               if favoriteProperties.count > 0 && (indexPath as NSIndexPath).section == 0 && !searchActive {
                    selectedProperty = favoriteProperties[(indexPath as NSIndexPath).row]
               } else {
                    if searchActive {
                         selectedProperty = filteredProperties[(indexPath as NSIndexPath).row]
                    } else {
                         selectedProperty = properties[(indexPath as NSIndexPath).row]
                    }
               }
               
               
          } else {
               if favoriteProperties.count > 0 && (indexPath as NSIndexPath).section == 0 && !searchActive {
                    let deletedProperty = favoriteProperties[(indexPath as NSIndexPath).row]
                    if let idx = favoriteProperties.index(of: deletedProperty) {
                         favoriteProperties.remove(at: idx)
                    }
               } else {
                    // bläh, improve later, not nice
                    var selectedProperty: Property!
                    if searchActive {
                         selectedProperty = filteredProperties[(indexPath as NSIndexPath).row]
                    } else {
                         selectedProperty = properties[(indexPath as NSIndexPath).row]
                    }
                    
                    if let idx = favoriteProperties.index(of: selectedProperty) {
                         favoriteProperties.remove(at: idx)
                    } else {
                         selectedProperty.date = Date()
                         favoriteProperties.append(selectedProperty)
                         //Answers.logCustomEvent(withName: "Add Property", customAttributes: ["PropertyName" : selectedProperty.name,
                         //                                                                    "PropertyAdress" : selectedProperty.address])
                         FabegeAnalytics.trackEvent("Add_Property", parameters:  ["PropertyName" : selectedProperty.name,
                                                              "PropertyAdress" : selectedProperty.address])
                         
                         
                    }
                    
               }
               
               doneButton.isEnabled = favoriteProperties.count > 0
          }
           navigationItem.rightBarButtonItem?.isEnabled = !disableExitUntilSelected || selectedProperty != nil
          
          tableView.reloadData()
     }
     
     
     
     // MARK: UISearchbarDelegate
     
     func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
          searchActive = false;
          tableView.reloadData()
     }
     
     func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
          searchBar.resignFirstResponder()
     }
     
     func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
          filteredProperties = properties.filter({ (property) -> Bool in
               let tmp: NSString = property.address as NSString
               let range = tmp.range(of: searchText, options: NSString.CompareOptions.caseInsensitive)
               return range.location != NSNotFound
          })
          
          if filteredProperties.count > 0 {
               searchActive = true
          } else if searchText.isEmpty {
               searchActive = false
          }
          
          tableView.reloadData()
     }
     
     
     
     // MARK: - IBActions
     
     @IBAction func close(_ sender: AnyObject) {
          if changeProperty {
               if selectedProperty != nil {
                    delegate?.didSelectProperty(selectedProperty!)
               }
          }
          
          dismiss(animated: true, completion: nil)
     }
     
     @IBAction func done(_ sender: AnyObject) {
          if changeProperty {
               if selectedProperty != nil {
                    delegate?.didSelectProperty(selectedProperty!)
               }
          } else {
               model.setFavorites(favoriteProperties)
               UserDefaults.standard.set(model.getFavorites().count - 1, forKey: "propertyIndex")
               UserDefaults.standard.synchronize()

          }

          dismiss(animated: true) {
               self.delegate?.didCloseView()
               
          }
          
          
          
          

     }
     
     @IBAction func dismissKeyboard(_ sender: AnyObject) {
          if searchBar.isFirstResponder {
               searchBar.resignFirstResponder()
          }
     }
     
     
     
     func sortProperties(_ distance : Bool) {
          properties = model.getProperties(distance)
          distanceSort = distance
          tableView.reloadData()
          
          UserDefaults.standard.set(distance, forKey: "sortType")
          UserDefaults.standard.synchronize()
     }
     
     
     
     // MARK: - Notifications
     
     @objc func locationUpdated() {
          if distanceSort == true {
               sortProperties(true)
               tableView.reloadData()
          }
          
          NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: NotificationLocationUpdates), object: nil)
     }
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertToOptionalNSAttributedStringKeyDictionary(_ input: [String: Any]?) -> [NSAttributedString.Key: Any]? {
     guard let input = input else { return nil }
     return Dictionary(uniqueKeysWithValues: input.map { key, value in (NSAttributedString.Key(rawValue: key), value)})
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromNSAttributedStringKey(_ input: NSAttributedString.Key) -> String {
     return input.rawValue
}
