//
//  Webservice.swift
//  fabege
//
//  Created by Oscar Kockum on 2015-04-07.
//  Copyright (c) 2015 Frontcell AB. All rights reserved.
//Användarnamn: WS_Fabege_FC
// Lösenord: d6FSSkU65H6rbA

import UIKit
import Alamofire
import SWXMLHash

let WebserviceInstance = Webservice()

class Webservice: NSObject {
    
    fileprivate var sessionKey : String?
    fileprivate var sessionKeyTime : Date?
    fileprivate let Username = "WS_Fabege_FC"
    fileprivate let Password = "d6FSSkU65H6rbA"
    fileprivate static let ApiUploadKey = "MjEyMkYwN0MtQUUzRi00MkFDLUE3OTctQkFENzgwMUU3QjFB"
    fileprivate let SessionValidInSeconds = 15 * 60 as TimeInterval
    var utcDateTime: DateFormatter = DateFormatter()
    
    
    fileprivate let DetailsInputXML = "<?xml version=\"1.0\" encoding=\"utf-8\" ?><GetDetailInputParameters xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\"><FastighetId>XXX</FastighetId>\r\n</GetDetailInputParameters>"
    
    fileprivate let TaskInputXML = "<?xml version=\"1.0\" encoding=\"utf-8\" ?><Task xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\"><Description>%@</Description> <SendTaskMail>1</SendTaskMail><Profession><Id>157</Id></Profession><ReportedBy><Name>%@</Name><Email>%@</Email><Phone>%@</Phone></ReportedBy><Structure><DestinationId>%@</DestinationId></Structure><Placement>%@</Placement></Task>"
    
    fileprivate let UploadImageInputXML = "<?xml version=\"1.0\" encoding=\"utf-8\"?><DocumentFile xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\"><ObjectID>%@</ObjectID><ObjectType>Task</ObjectType><Filename>image.jpg</Filename></DocumentFile>"
    
    override init() {
        utcDateTime.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
    }
    
    func isSessionValid() -> Bool {
        if sessionKey == nil || sessionKeyTime == nil {
            return false
        }
        let now = Date()
        let diff = now.timeIntervalSince(sessionKeyTime!)
        return diff < SessionValidInSeconds
    }
    
    func invalidateSession() {
        sessionKey = nil
        sessionKeyTime = nil
    }
    
    func createTask(_ sourceId : Int,
                    company : String,
                    contact : String,
                    email : String,
                    phone : String,
                    description : String,
                    image : UIImage?,
                    completion: (() -> Void)?,
                    failure: ((_ error: NSError?) -> Void)?) {
        if !isSessionValid() {
            login({ (sessionKey) -> Void in
                self.sessionKey = sessionKey
                self.authCreateTask(sourceId, company: company, contact: contact, email: email, phone: phone, description: description, image: image, completion: completion, failure: failure)
            }, failure: { (error) -> Void in
                // Could not login --> failure. Not much to do, show alert.
            })
        } else {
            self.authCreateTask(sourceId, company: company, contact: contact, email: email, phone: phone, description: description, image: image, completion: completion, failure: failure)
        }
    }
    
    func uploadDeviceToken(_ deviceToken : String,
                           propertyIds : String) {
        
        let parameters = [
            "deviceId": deviceToken,
            "propertyIds": propertyIds]
        
        Alamofire.request("https://fabege.frontcell.se/push/register/ios", method: .post, parameters: parameters, encoding: URLEncoding.default)
            .responseString(completionHandler: { (response) in
                if let httpResponse = response.response {
                    print("register with resp. code = \(httpResponse.statusCode)")
                }
            })
        
    }
    
    
    func uploadImage(_ taskId : String,
                     image : UIImage,
                     completion: (() -> Void)?,
                     failure: ((_ error: NSError?) -> Void)?) {
        if !isSessionValid() {
            login({ (sessionKey) -> Void in
                self.sessionKey = sessionKey
                self.authUploadImage(taskId, image: image, completion: completion, failure: failure)
            }, failure: { (error) -> Void in
                // Could not login --> failure. Not much to do, show alert.
            })
        } else {
            self.authUploadImage(taskId, image: image, completion: completion, failure: failure)
        }
    }
    
    fileprivate func authUploadImage(_ taskId : String,
                                     image : UIImage,
                                     completion: (() -> Void)?,
                                     failure: ((_ error: NSError?) -> Void)?) {
        
        let headers = ["DeDU-Authorization" : Webservice.ApiUploadKey]
        
        Alamofire.upload(multipartFormData: { (data) in
            if let imgData = image.jpegData(compressionQuality: 0.8),
                let taskIdData = taskId.data(using: .utf8) {
                data.append(imgData, withName: "image.jpg", fileName: "image.jpg", mimeType: "image/jpeg")
                data.append(taskIdData, withName: "OrderNumber")
            }
            }, usingThreshold: SessionManager.multipartFormDataEncodingMemoryThreshold, to: URL(string: "https://api.dedu.se/UploadFiles/")!, method: .post, headers: headers) { (result) in
             switch result {
                case .failure(let error):
                    failure?(error as NSError)
                case .success(let upload, _, _):
                    upload.responseJSON { (response) in
                        if response.result.isSuccess {
                            completion?()
                        } else {
                            failure?(NSError(domain: "Unkown error", code: 0, userInfo: nil))
                        }
                    }
            }
        }
        
    }
    
    fileprivate func authCreateTask(_ sourceId : Int,
                                    company : String,
                                    contact : String,
                                    email : String,
                                    phone : String,
                                    description : String,
                                    image : UIImage?,
                                    completion: (() -> Void)?,
                                    failure: ((_ error: NSError?) -> Void)?) {
        
        
        let xmlFile = String(format: TaskInputXML, description, contact, email, phone, String(sourceId), company)
        
        let URL = Foundation.URL(string: "https://www.dedu.se/DeDUService/TemplatedXML?TemplateName=DeDUDBWS_CreateTask&SessionKey=\(sessionKey!)")
        var mutableURLRequest = URLRequest(url: URL!)
        mutableURLRequest.httpMethod = "POST"
        mutableURLRequest.httpBody = xmlFile.data(using: String.Encoding.utf8, allowLossyConversion: false)
        Alamofire.request(mutableURLRequest)
            .responseString(encoding: String.Encoding.utf8,
                            completionHandler: { (response) -> Void in
                                
                                if let httpResponse = response.response {
                                    if httpResponse.allHeaderFields["ErrorDescription"] != nil {
                                        let message = httpResponse.allHeaderFields["ErrorDescription"] as! String
                                        failure?(NSError(domain: message, code: 0, userInfo: nil))
                                    } else if httpResponse.allHeaderFields["id"] != nil {
                                        let id = httpResponse.allHeaderFields["id"] as! String
                                        print("Success : \(id)")
                                        // 1. If we have an image, upload first.
                                        // 2. Else call completion
                                        
                                        if let uImage = image {
                                            self.authUploadImage(id, image: uImage, completion: completion, failure: failure)
                                        } else {
                                            completion?()
                                        }
                                        
                                    }
                                    
                                    
                                } else {
                                    self.invalidateSession()
                                    failure?(nil)
                                }
                                
            })
        
        
    }
    
    
    func loadStructures(
        _ completion: ((_ response: [Property]) -> Void)?,
        failure: ((_ error: NSError?) -> Void)?) {
        if !isSessionValid() {
            login({ (sessionKey) -> Void in
                self.sessionKey = sessionKey
                self.authenticatedLoadStructures(completion, failure: failure)
            }, failure: { (error) -> Void in
                // Could not login --> failure. Not much to do, show alert.
            })
        } else {
            authenticatedLoadStructures(completion, failure: failure)
        }
    }
    
    func getDetails(_ propertyIds : [Int],
                    completion: ((_ response: [Int : [Task]]) -> Void)?,
                    failure: ((_ error: NSError?) -> Void)?) {
        if !isSessionValid() {
            login({ (sessionKey) -> Void in
                self.sessionKey = sessionKey
                self.authenticatedLoadDetails(propertyIds, completion: completion, failure: failure)
            }, failure: { (error) -> Void in
                // Could not login --> failure. Not much to do, show alert.
            })
        } else {
            authenticatedLoadDetails(propertyIds, completion: completion, failure: failure)
        }
    }
    
    fileprivate func authenticatedLoadDetails(_ propertyIds : [Int],
                                              completion: ((_ response: [Int : [Task]]) -> Void)?,
                                              failure: ((_ error: NSError?) -> Void)?) {
        
        var pTags = ""
        for propertyId in propertyIds {
            pTags += "<int>\(propertyId)</int>"
        }
        
        let xmlFile = DetailsInputXML.replacingOccurrences(of: "XXX", with: pTags, options: NSString.CompareOptions.literal, range: nil)
        let parameters = [
            "TemplateName": "Fabege_GetDetail",
            "xmlFile": xmlFile,
            "SessionKey": "\(sessionKey!)"]
        Alamofire.request("https://www.dedu.se/DeDUService/TemplatedXML", parameters: parameters)
            .responseString(encoding: String.Encoding.utf8,
                            completionHandler: { (response) -> Void in
                                if response.result.isSuccess {
                                    // Redo.... lot of unsafe stuff...
                                    var dict = [Int : [Task]]()
                                    let xml = SWXMLHash.parse(response.result.value!)
                                    
                                    for elem in xml["Fastigheter"]["Fastighet"].all {
                                        let id = Int(elem.element!.attribute(by: "ID")!.text)
                                        var tasks = [Task]()
                                        for elem in elem["DriftStorningLista"]["DriftStorning"].all {
                                            let task = Task(element: elem)
                                            tasks.insert(task, at: 0)
                                        }
                                        for elem in elem["AktuelltIHusetLista"]["AktuelltIHuset"].all {
                                            let task = Task(element: elem)
                                            tasks.insert(task, at: 0)
                                        }
                                        dict[id!] = tasks
                                    }
                                    
                                    
                                    completion?(dict)
                                    
                                } else {
                                    self.invalidateSession()
                                    failure?(nil)
                                    
                                }
            })
    }
    
    
    fileprivate func authenticatedLoadStructures(
        _ completion: ((_ response: [Property]) -> Void)?,
        failure: ((_ error: NSError?) -> Void)?) {
        
        let parameters = [
            "TemplateName": "Fabege_GetStructures",
            "xmlFile": "<?xml version=\"1.0\" encoding=\"utf-8\"?><GetStructureInputParameters xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\"><OnlyMarkedForInternet>1</OnlyMarkedForInternet></GetStructureInputParameters>",
            "SessionKey": "\(sessionKey!)"]
        
        Alamofire.request("https://www.dedu.se/DeDUService/TemplatedXML", parameters: parameters)
            .responseString(encoding: String.Encoding.utf8,
                            completionHandler: { (response) -> Void in
                                if response.result.isSuccess  {
                                    var properties = [Property]()
                                    let xml = SWXMLHash.parse(response.result.value!)
                                    for elem in xml["ArrayOfStructure"]["Structure"].all {
                                        let property = Property(element: elem)
                                        properties.append(property)
                                    }
                                    completion?(properties)
                                    
                                } else {
                                    self.invalidateSession()
                                    failure?(nil)
                                    
                                }
            })
    }
    
    
    
    fileprivate func login(
        _ completion: ((_ sessionKey: String) -> Void)?,
        failure: ((_ error: NSError?) -> Void)?) {
        
        let hash = generateHash()
        
        let parameters = [
            "Username": "\(Username)",
            "TimeStamp": "\(hash.timeStamp)",
            "Hash": "\(hash.hash)"]
        
        Alamofire.request("https://www.dedu.se/DeDUService/Login", parameters: parameters)
            .responseString(encoding: String.Encoding.utf8,
                            completionHandler: { (response) -> Void in
                                if response.result.isSuccess  {
                                    let xml = SWXMLHash.parse(response.result.value!)
                                    if let sessionKey = xml["SessionKey"]["Value"].element?.text {
                                        completion?(sessionKey)
                                    } else {
                                        failure?(nil)
                                    }
                                } else {
                                    failure?(nil)
                                    
                                }
            })
    }
    
    fileprivate func generateHash() -> (hash: String, timeStamp: String) {
        let dateFormatter = DateFormatter()
        let enUSPosixLocale = Locale(identifier: "en_US_POSIX")
        dateFormatter.locale = enUSPosixLocale
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZZZZZ"
        let timeStamp = dateFormatter.string(from: Date())
        let stringToSign = Username + timeStamp
        var hash = stringToSign.hmac(HMACAlgorithm.sha1, key: Password)
        //print("timestamp : \(timeStamp)")
        //print("hash before mod : \(hash)")
        hash = hash.replacingOccurrences(of: "+", with: "-", options: NSString.CompareOptions.literal, range: nil)
        hash = hash.replacingOccurrences(of: "=", with: "_", options: NSString.CompareOptions.literal, range: nil)
        hash = hash.replacingOccurrences(of: "/", with: "~", options: NSString.CompareOptions.literal, range: nil)
        //print("hash after mod : \(hash)")
        
        return (hash, timeStamp)
    }
    
}
