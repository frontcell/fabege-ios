//
//  AlertViewUIHandler.swift
//  fabege
//
//  Created by Oscar Kockum on 2019-06-11.
//  Copyright © 2019 Frontcell AB. All rights reserved.
//

import UIKit

class AlertViewUIHandler : NSObject, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout  {
     
     private var alertCollectionView: UICollectionView
     private var alertIcon: UIImageView
     private var alertCollectionHeight: NSLayoutConstraint
     private var propertyId : String?
     private var alerts = [WAWAlert]()
     
     private static let DismissedAlertIdKey = "AlertViewUIHandler.dismissedAlertIds"
     
     enum AlertVisibility {
          case hidden
          case expanded
          case minimized
     }
     
     
     class func dismissedAlertIds() -> [String] {
          return UserDefaults.standard.array(forKey: DismissedAlertIdKey) as? [String] ?? []
     }
     
     class func markAlertsAsDismissed(alerts: [WAWAlert]) {
          var ids = dismissedAlertIds()
          alerts.forEach { (alert) in
               if (!ids.contains(alert.id)) {
                    ids.append(alert.id)
               }
          }
          UserDefaults.standard.set(ids, forKey: DismissedAlertIdKey)
     }
     
     class func cleartDismissedAlertIds() {
          UserDefaults.standard.removeObject(forKey: DismissedAlertIdKey)
     }
     
     
     
     private var visibility = AlertVisibility.hidden {
          didSet {
               switch visibility {
               case .hidden:
                    alertIcon.isHidden = true
                    alertCollectionHeight.constant = 0
               case .expanded:
                    alertIcon.isHidden = false
                    alertCollectionHeight.constant = 293
                    (alertCollectionView.collectionViewLayout as! UPCarouselFlowLayout).itemSize = CGSize(width: alertCollectionView.frame.width - 36 * 2, height: 293)
               case .minimized:
                    alertIcon.isHidden = false
                    alertCollectionHeight.constant = 60
                    (alertCollectionView.collectionViewLayout as! UPCarouselFlowLayout).itemSize = CGSize(width: alertCollectionView.frame.width - 36 * 2, height: 60)
               }
               alertCollectionView.reloadData()
          }
     }
     
     var subscription : WAWService.Subscription?
     
     deinit {
          subscription?.detach()
     }

     
     init(collectionView : UICollectionView, alertIcon: UIImageView, propertyId : String? = nil) {
          self.alertCollectionView = collectionView
          self.alertIcon = alertIcon
          self.propertyId = propertyId
          alertCollectionHeight = collectionView.heightAnchor.constraint(equalToConstant: 0)
          alertCollectionHeight.isActive = true
          super.init()
          
          alertCollectionView.register(UINib(nibName: "SmallAlertCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "SmallAlertCollectionViewCell")
          alertCollectionView.register(UINib(nibName: "LargeAlertCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "LargeAlertCollectionViewCell")
          
          collectionView.delegate = self
          collectionView.dataSource = self
          let layout = UPCarouselFlowLayout()
          layout.scrollDirection = .horizontal
          layout.sideItemAlpha = 1
          layout.sideItemScale = 1
          collectionView.collectionViewLayout = layout
          
          visibility = .hidden
          
          subscription = WAWService().subscribeToAlerts { (alerts) in
               if var alerts = alerts {
                    alerts = alerts.filter { $0.propertyId == propertyId }
                    self.alerts = alerts
                    
                    let dismissedAlerts = AlertViewUIHandler.dismissedAlertIds()
                    let newAlerts = alerts.filter({ (alert) -> Bool in
                         return !dismissedAlerts.contains(alert.id)
                    })
                    
                    if newAlerts.count > 0 {
                         self.visibility = .expanded
                    } else if alerts.count > 0 {
                         self.visibility = .minimized
                    } else {
                         self.visibility = .hidden
                    }
               } else {
                    self.visibility = .hidden
                    self.alerts.removeAll()
               }
          }
     }
     
     
     func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
          return alerts.count
     }
     
     func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
          if visibility == .expanded {
               let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "LargeAlertCollectionViewCell", for: indexPath) as! LargeAlertCollectionViewCell
               cell.onExitClicked = { [unowned self] in
                    AlertViewUIHandler.markAlertsAsDismissed(alerts: self.alerts)
                    self.visibility = .minimized
               }
               cell.setAlert(alert: alerts[indexPath.row])
               return cell
          } else {
               let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SmallAlertCollectionViewCell", for: indexPath) as! SmallAlertCollectionViewCell
               cell.setAlert(alert: alerts[indexPath.row])
               return cell
          }
          
     }
     
     func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
          if visibility == .minimized {
               visibility = .expanded
          }
     }
     
}
