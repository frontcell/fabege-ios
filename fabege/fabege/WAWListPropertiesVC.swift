//
//  WAWListPropertiesVC.swift
//  fabege
//
//  Created by Oscar Kockum on 2018-12-18.
//  Copyright © 2018 Frontcell AB. All rights reserved.
//

import UIKit
import Kingfisher
import FirebaseMessaging
import Firebase

class WAWListPropertiesVC: UIViewController, UITableViewDataSource {
     
     @IBOutlet weak var tableView: UITableView!
     @IBOutlet weak var keyButton: UIButton!
     @IBOutlet weak var keyButtonInfoContainer: UIStackView!
     
     var properties = [WAWProperty]()
     var service = WAWService()
     var user : WAWUser?
     var alerts : [WAWAlert]?
     var activeKeysMap = [String : Int]()
     var propertyIdsInSubscription = [String]()
     
     var subscriptions = [WAWService.Subscription]()
     
     deinit {
          subscriptions.forEach { (subscription) in
               subscription.detach()
          }
     }
     
     private func setupData() {
          let subscription = service.subscribeToProperties { [unowned self] (properties) in
               if let properties = properties {
                    self.reloadPropertiesView(properties: properties)
               }
          }
          subscriptions.append(subscription)
          
          let alertSubscription = service.subscribeToAlerts { [unowned self] (alerts) in
               self.alerts = alerts
               self.tableView.reloadData()
          }
          subscriptions.append(alertSubscription)
          
          let userSubscription = service.subscribeToUserChanges { [unowned self] (user) in
               self.user = user
               
               if user.deviceToken == nil || Messaging.messaging().fcmToken != user.deviceToken {
                    self.refreshFirebaseToken()
               }
               
               self.updateKeyAvailability()
               self.tableView.reloadData()
          }
          if let userSubscription = userSubscription {
               subscriptions.append(userSubscription)
          }
     }
     
     private func refreshFirebaseToken() {
          InstanceID.instanceID().instanceID { [unowned self] (result, error) in
               if let error = error {
                    print("Error fetching remote instance ID: \(error)")
               } else if let result = result {
                    print("Remote instance ID token: \(result.token)")
                    self.service.updateDeviceToken(deviceToken: result.token)
               }
          }
     }
     
     
     fileprivate func updateKeyAvailability() {
          if let user = user, user.hasValidAccessForProperties(properties: properties) {
               keyButton.outerShadow(show: true, animated: true)
               keyButton.isEnabled = true
               keyButtonInfoContainer.isHidden = false
          } else {
               keyButton.outerShadow(show: false, animated: true)
               keyButton.isEnabled = false
               keyButtonInfoContainer.isHidden = true
          }
     }
     
     private func reloadKeysCount(property : WAWProperty, schedules : [WAWSchedule]) {
          var activeKeys = 0
          for schedule in schedules {
               for (idx, slot) in schedule.timeSlots.enumerated() {
                    if slot.ids.contains(service.userId), idx < property.openingHours.count, schedule.isOngoingOrUpcoming(hours: property.openingHours[idx]) {
                         activeKeys += 1
                    }
               }
          }
          let currentKeyCount = activeKeysMap[property.id] ?? 0
          if currentKeyCount != activeKeys {
               activeKeysMap[property.id] = activeKeys
               tableView.reloadData()
          }
     }
     
     private func reloadPropertiesView(properties : [WAWProperty]) {
          
          for property in properties {
               if !propertyIdsInSubscription.contains(property.id) {
                    propertyIdsInSubscription.append(property.id)
                    let subscription = service.subscribeToScheduleChanges(property: property) { [weak self] (schedules) in
                         if let schedules = schedules {
                              self?.reloadKeysCount(property: property, schedules: schedules)
                         }
                    }
                    subscriptions.append(subscription)
                    
               }
          }
          self.properties = properties
          self.tableView.reloadData()
          updateKeyAvailability()
     }
     
     override func viewDidLoad() {
          tableView.tableFooterView = UIView()
          setupData()
          super.viewDidLoad()
     }
     
     override func viewDidAppear(_ animated: Bool) {
          FabegeAnalytics.trackPageView(name: "WAW_list_properties")
     }
     
     override func viewWillAppear(_ animated: Bool) {
          navigationController?.setNavigationBarHidden(true, animated: true)
          setNeedsStatusBarAppearanceUpdate()
     }
     
     override var preferredStatusBarStyle: UIStatusBarStyle {
          return .default
     }
     
     @IBAction func actionBack(_ sender: Any) {
          navigationController?.popViewController(animated: true)
     }
     
     @IBAction func actionKey(_ sender: Any) {
          FabegeAnalytics.trackEvent("WAW_Open_door", parameters:nil)
          service.launchKeyApp(origin: "")
     }
     
     func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
          let cell = tableView.dequeueReusableCell(withIdentifier: "WAWPropertyCell", for: indexPath) as! WAWPropertyCell
          let property = properties[indexPath.row]
          cell.property = property
          let activeKeys = activeKeysMap[property.id] ?? 0
          if activeKeys  > 0 {
               cell.upcomingBookingsView.isHidden = false
               cell.upcomingBookingsCount.text = "\(activeKeys)"
          } else {
               cell.upcomingBookingsView.isHidden = true
          }
          cell.topView.isHidden = indexPath.row > 0
          
          let alertForProperty = alerts?.first(where: { (alert) -> Bool in
               return alert.propertyId == property.id
          })
          cell.alertIcon.isHidden = alertForProperty == nil
          
          return cell
     }
     
     func tableView(_ tableView: UITableView, didEndDisplaying cell:
          UITableViewCell, forRowAt indexPath: IndexPath) {
          if let cell = cell as? WAWPropertyCell {
               cell.propertyIcon.kf.cancelDownloadTask()
          }
     }
     
     func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
          return properties.count
     }
     
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
          if let vc = segue.destination as? WAWScheduleVC, let idx = tableView.indexPathForSelectedRow {
               vc.property = properties[idx.row]
          }
     }
     
     
}

