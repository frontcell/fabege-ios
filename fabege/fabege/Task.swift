//
//  Matter.swift
//  fabege
//
//  Created by Patrik Billgert on 02/04/15.
//  Copyright (c) 2015 Frontcell AB. All rights reserved.
//

import UIKit
import SWXMLHash


class Task: NSObject {
    
    var title: String!
    var information: String!
    var date: String?
    
    init(element: XMLIndexer) {
        if let text = element["Rubrik"].element?.text {
            title = text
        } else {
            title = ""
        }

        if let info = element["memoBeskrivning"].element?.text {
            information = info
        } else {
            information = ""
        }
        
        if let dateText = element["PubliceraTill"].element?.text {
            var formattedDate = dateText.components(separatedBy: " ")
            date = formattedDate[0]
        }
    }
}
