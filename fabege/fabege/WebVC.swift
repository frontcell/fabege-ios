//
//  InfoVC.swift
//  fabege
//
//  Created by Patrik Billgert on 18/03/15.
//  Copyright (c) 2015 Frontcell AB. All rights reserved.
//

import UIKit
import WebKit


class WebVC: UIViewController, WKNavigationDelegate {
    
    @IBOutlet weak var backButton: UIBarButtonItem!
    @IBOutlet weak var nextButton: UIBarButtonItem!
    var webView: WKWebView?
    
    var infoMode = false
    var showActivityIndicator = false
    var selectedProperty: Property!
    var navTitle: String?
    var urlSuffix: String!
    var url : URL?
    var adjustContentWidth = false
    
    deinit {
        webView?.removeObserver(self, forKeyPath: "canGoBack")
        webView?.removeObserver(self, forKeyPath: "canGoForward")
        webView?.navigationDelegate = nil
    }
    
    
    
    // MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        var webviewPreloaded = false
        if webView == nil {
            let wkUController = WKUserContentController()
            if adjustContentWidth {
                let jscript = "var meta = document.createElement('meta'); meta.setAttribute('name', 'viewport'); meta.setAttribute('content', 'width=device-width'); document.getElementsByTagName('head')[0].appendChild(meta);"
                let userScript = WKUserScript(source: jscript, injectionTime: .atDocumentEnd, forMainFrameOnly: true)
                wkUController.addUserScript(userScript)
            }
            let wkWebConfig = WKWebViewConfiguration()
            wkWebConfig.userContentController = wkUController
            webView = WKWebView(frame: CGRect(x: 0, y: 0, width: view.frame.size.width, height: view.frame.size.height - 44), configuration: wkWebConfig)
            webView?.contentMode = .scaleToFill
        } else {
            webviewPreloaded = true
            webView?.frame = CGRect(x: 0, y: 0, width: view.frame.size.width, height: view.frame.size.height)
            webView?.scrollView.setContentOffset(CGPoint(x: 0, y: 0), animated: false)
            
            
        }
        view.insertSubview(webView!, at: 0)
        webView?.addObserver(self, forKeyPath: "canGoBack", options:.new , context: nil)
        webView?.addObserver(self, forKeyPath: "canGoForward", options:.new , context: nil)
        backButton.isEnabled = false
        nextButton.isEnabled = false
        
        if url == nil {
            if infoMode {
                navigationItem.title = "Din fastighet"
                url = URLUtils.urlForProperty(selectedProperty)
            } else {
                navigationItem.title = navTitle
                url = URLUtils.urlForProperty(selectedProperty, suffix: urlSuffix)
            }
        } else {
            navigationItem.title = navTitle
        }
        
        if !webviewPreloaded {
            let request = URLRequest(url: url!, cachePolicy: .returnCacheDataElseLoad, timeoutInterval: 10)
            _ = webView?.load(request)
        }
        
        webView?.navigationDelegate = self
        webView?.boundInside(view)
        
        // TODO: Track the user action that is important for you.
        
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewWillAppear(_ animated: Bool) {
        setNeedsStatusBarAppearanceUpdate()
        navigationController?.setNavigationBarHidden(false, animated: true)
    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if keyPath == "canGoBack" {
            backButton.isEnabled = webView!.canGoBack
        } else if keyPath == "canGoForward" {
            nextButton.isEnabled = webView!.canGoForward
        }
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    var spinner : UIActivityIndicatorView?
    
    // MARK: WKNavigationDelegate
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
        if showActivityIndicator {
            spinner?.removeFromSuperview()
        }

    }
    
    
    
    
    func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
        if showActivityIndicator {
            spinner?.removeFromSuperview()
            spinner = UIActivityIndicatorView(style: .whiteLarge)
            spinner?.translatesAutoresizingMaskIntoConstraints = false
            spinner?.color = UIColor.waw.purpleColor
            spinner?.startAnimating()
            webView.addSubview(spinner!)
            spinner?.centerYAnchor.constraint(equalTo: webView.centerYAnchor).isActive = true
            spinner?.centerXAnchor.constraint(equalTo: webView.centerXAnchor).isActive = true
        }
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }
    
    
    @IBAction func goBack(_ sender: AnyObject) {
        if webView!.canGoBack {
            webView!.goBack()
        }
    }
    
    
    @IBAction func goForward(_ sender: AnyObject) {
        if webView!.canGoForward {
            webView!.goForward()
        }
    }
    
    // MARK: - Functions
    
    
}

extension UIView{
    
    func boundInside(_ view: UIView){
        
        self.translatesAutoresizingMaskIntoConstraints = false
        view.addConstraint(NSLayoutConstraint(item: self, attribute: .width, relatedBy: .equal, toItem: view, attribute: .width, multiplier: 1, constant: 0))
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:[view]-44-|", options: NSLayoutConstraint.FormatOptions(rawValue: 0), metrics: nil, views: ["view": self]));
        view.addConstraint(NSLayoutConstraint(item: self, attribute: .top, relatedBy: .equal, toItem: view, attribute: .top, multiplier: 1, constant: 0))
        
        
        
    }
}
