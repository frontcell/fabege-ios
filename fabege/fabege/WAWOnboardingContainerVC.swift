//
//  WAWOnboardingContainerVC.swift
//  fabege
//
//  Created by Oscar Kockum on 2019-01-14.
//  Copyright © 2019 Frontcell AB. All rights reserved.
//

import UIKit

class WAWOnboardingContainerVC : UIViewController, UIPageViewControllerDataSource, UIPageViewControllerDelegate {
   
     
     var pageVC : UIPageViewController? = nil
     @IBOutlet weak var nextButton: UIButton!
     @IBOutlet weak var pageControl: UIPageControl!
     @IBOutlet weak var nextButtonBottom: NSLayoutConstraint!
     
     static let EndPositionNextButton : CGFloat = 29
     static let StartPositionNextButton : CGFloat = -60
     
     
     var onboardingData = WAWOnboardingPageVC.OnboardingData.all
     
     override func viewDidLoad() {
          pageControl.numberOfPages = onboardingData.count
          nextButtonBottom.constant = WAWOnboardingContainerVC.StartPositionNextButton
     }
     
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
          if segue.identifier == "PageVC" {
               pageVC = segue.destination as? UIPageViewController
               pageVC?.dataSource = self
               pageVC?.delegate = self
               pageVC?.setViewControllers([loadViewController(data: onboardingData.first!)], direction: .forward, animated: false, completion: nil)
          }
     }
     
     private func loadViewController(data : WAWOnboardingPageVC.OnboardingData) -> WAWOnboardingPageVC {
          return WAWOnboardingPageVC.setup(with:data)
     }
     
     @IBAction func actionClose(_ sender: Any) {
          navigationController?.popViewController(animated: true)
     }
     @IBAction func actionNext(_ sender: Any) {
          let service = WAWService()
          service.setOnboardingShown()
          
          var info = WAWInfoController.Info()
          info.actionButton = "tutorial_last_action".localize()
          info.header = "tutorial_last_header".localize()
          info.info = "tutorial_last_info".localize()
          info.onActionClicked = {
               if let navVC = self.navigationController {
                    navVC.popToRootViewController(animated: false)
                    WAWRouter(navigationController: navVC).initialize()
               }
               
          }
          let vc = WAWInfoController.setup(with: info)
          navigationController?.pushViewController(vc, animated: true)
     }
     
     func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
          let data = (viewController as! WAWOnboardingPageVC).data!
          guard let idx = onboardingData.index(where: { $0.mainText == data.mainText }),
          idx > 0 else {
               return nil
          }
          return loadViewController(data: onboardingData[idx - 1])
     }
     
     func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
          let data = (viewController as! WAWOnboardingPageVC).data!
          guard let idx = onboardingData.index(where: { $0.mainText == data.mainText }),
               idx < (onboardingData.count - 1) else {
                    return nil
          }
          return loadViewController(data: onboardingData[idx + 1])
          
     }
     
     private var currentIdx = 0
     
     func pageViewController(_ pageViewController: UIPageViewController, willTransitionTo pendingViewControllers: [UIViewController]) {
          
          if let data = (pendingViewControllers.first as! WAWOnboardingPageVC).data,
               let idx = onboardingData.index(where: { $0.mainText == data.mainText }) {
               currentIdx = idx
          }
     }
     
     func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
          pageControl.currentPage = currentIdx
          
          // quick fix...
          if currentIdx == 1 {
               let appDelegate = UIApplication.shared.delegate as! AppDelegate
               appDelegate.registerForPUSH()
          }

          let showShowButton = self.currentIdx == self.onboardingData.count - 1
          nextButton.alpha = showShowButton ? 0 : 1
          let endAlpha: CGFloat = showShowButton ? 1 : 0
          let pageControlAlpha: CGFloat = showShowButton ? 0 : 1
          let endPosition = showShowButton ? WAWOnboardingContainerVC.EndPositionNextButton : WAWOnboardingContainerVC.StartPositionNextButton
          UIView.animate(withDuration: 0.2) {
               self.nextButton.alpha = endAlpha
               self.pageControl.alpha = pageControlAlpha
               self.nextButtonBottom.constant = endPosition
               self.view.layoutIfNeeded()
          }

     }
}

class WAWOnboardingPageVC : UIViewController {
     
     struct OnboardingData {
          var image : UIImage
          var mainText : String
          var secondaryText : String?

          
          static let all : [OnboardingData] = {
               return [page2, page1, page3, page4]
          }()
          
          static let page1 = OnboardingData(image:#imageLiteral(resourceName: "onboarding_1") , mainText: "onboarding_1".localize(), secondaryText: nil)
          static let page2 = OnboardingData(image:#imageLiteral(resourceName: "onboarding_2") , mainText: "onboarding_2".localize(), secondaryText: "onboarding_2_secondary".localize())
          static let page3 = OnboardingData(image:#imageLiteral(resourceName: "onboarding_3") , mainText: "onboarding_3".localize(), secondaryText: nil)
          static let page4 = OnboardingData(image:#imageLiteral(resourceName: "onboarding_4") , mainText: "onboarding_4".localize(), secondaryText: nil)
     }
     
     @IBOutlet weak var icon: UIImageView!
     @IBOutlet weak var mainTextLabel: UILabel!
     @IBOutlet weak var secondaryTextLabel: UILabel!
     
     var data : OnboardingData!
     
     override func viewDidLoad() {
          icon.image = data.image
          mainTextLabel.text = data.mainText
          if let text = data.secondaryText {
               secondaryTextLabel.isHidden = false
               secondaryTextLabel.text = text
          } else {
               secondaryTextLabel.isHidden = true
          }
     }
     
     class func setup(with data: OnboardingData) -> WAWOnboardingPageVC {
          let vc = UIStoryboard(name: "WAW", bundle: nil).instantiateViewController(withIdentifier: "WAWOnboardingPageVC") as! WAWOnboardingPageVC
          
          vc.data = data
          return vc
     }
     
}

