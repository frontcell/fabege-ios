//
//  ViewController.swift
//  fabege
//
//  Created by Patrik Billgert on 18/03/15.
//  Copyright (c) 2015 Frontcell AB. All rights reserved.
//

import UIKit

class SplashVC: UIViewController, UIAlertViewDelegate, SearchVCDelegate {
     
     
     
     @IBOutlet weak var statusLabel: UILabel!
     
     // MARK: - Lifecycle
     
     override func viewDidLoad() {
          super.viewDidLoad()
          
          self.navigationController?.setNavigationBarHidden(true, animated: false)
          UIApplication.shared.setStatusBarHidden(true, with: .none)
          
         
          
          statusLabel.text = "Hämtar fastigheter"
          
          loadStructures()
     }
     
     override func didReceiveMemoryWarning() {
          super.didReceiveMemoryWarning()
          // Dispose of any resources that can be recreated.
     }
     
     
     
     // MARK: - Navigation
     
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
          if segue.identifier == "PresentSearch" {
               let navigationVC = segue.destination as! UINavigationController;
               let searchVC = navigationVC.visibleViewController as! SearchVC;
               searchVC.delegate = self
               searchVC.firstLaunch = true
          } else if segue.identifier == "ShowHome" {
               
          }
     }
     
     func didSelectProperty(_ property: Property) {
          
     }
     
     func didCloseView() {
          let vc = self.storyboard?.instantiateViewController(withIdentifier: "HomeVC")
          self.navigationController?.setViewControllers([vc!], animated: true)
     }
     
     
     
     // MARK: - Requests
     
     func loadStructures() {
          WebserviceInstance.loadStructures({ (response) -> Void in
               model.setProperties(response)
               if (model.hasFavorites()) {
                    //self.performSegue(withIdentifier: "ShowHome", sender: nil)
                    let vc = self.storyboard?.instantiateViewController(withIdentifier: "HomeVC")
                    self.navigationController?.setViewControllers([vc!], animated: true)
               } else {
                    self.performSegue(withIdentifier: "PresentSearch", sender: nil)
               }
          }, failure: { (error) -> Void in
               let alertView = UIAlertView(title: "Något gick fel", message: "Försök igen", delegate: self, cancelButtonTitle: "OK")
               alertView.show()
          })
     }
     
     
     
     // MARK: - UIAlertViewDelegate
     
     func alertView(_ alertView: UIAlertView, clickedButtonAt buttonIndex: Int) {
          if buttonIndex == 0 {
               loadStructures()
          }
     }
}

