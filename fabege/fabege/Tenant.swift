//
//  Tenant.swift
//  fabege
//
//  Created by Patrik Billgert on 02/04/15.
//  Copyright (c) 2015 Frontcell AB. All rights reserved.
//

import UIKit

class Tenant: NSObject {
   
    var company: String!
    var email: String!
    var name: String!
    var phone: String!
    var property: Property!
}
